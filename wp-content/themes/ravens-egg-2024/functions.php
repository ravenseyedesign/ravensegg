<?php
/**
 * Raven's Egg functions
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */

Namespace RavensEgg2024;

/************* INCLUDE NEEDED FILES ***************/

// Various theme support functions
include_once 'inc/theme-support.php';
include_once 'inc/cleanup.php';
include_once 'inc/enqueue.php';
include_once 'inc/comments-callback.php';
include_once 'inc/plugin-mods.php';
include_once 'inc/customizer.php';
//include_once 'inc/block-templates.php';
//include_once 'inc/woocommerce-mods.php';

// Admin customization
include_once 'inc/admin.php';
include_once 'inc/login.php';
include_once 'inc/dashboard-widget.php';