<?php
/**
 * Password-protected page template
 *
 * Template name: PW Protected Page
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */

get_header(); ?>

<?php get_template_part( 'template-parts/header/start' ); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    <article <?php post_class(); ?>>

        <h1 id="page-title"><?php the_title(); ?></h1>

		<?php RavensEgg2024\insert_lightbox_featured_image(); ?>

        <div class="entry-content">

			<?php // Use with https://wordpress.org/plugins/logout-password-protected-posts/
			if ( has_action( 'posts_logout_link' ) ) {
				echo '<p class="pw-logout">';
				do_action( 'posts_logout_link', 'Log out of this page' );
				echo '</p>';
			} ?>

			<?php the_content(); ?>

        </div><!-- .entry-content -->

		<?php edit_post_link( __( 'Edit', 'ravens-egg-2024' ), '<p class="edit-link">', '</p>', null, 'button' ); ?>

    </article>

<?php endwhile; ?>

<?php else : ?>

	<?php get_template_part( 'template-parts/post/content-none' ); ?>


<?php endif; ?>

<?php

get_template_part( 'template-parts/page/page-navigation' );

if ( function_exists( 'wp_link_pages' ) ) {
	wp_link_pages( 'before=<p class="pagelink">Page: ' );
}

?>

<?php get_template_part( 'template-parts/footer/end' ); ?>