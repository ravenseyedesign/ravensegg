<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<?php get_template_part( 'template-parts/image/svg-sprite-icons' ); ?>

<div id="wrapper">

    <div class="skip-link">
        <a href="#primary"
           title="<?php esc_attr_e( 'Skip to content', 'ravens-egg-2024' ); ?>"><?php _e( 'Skip to content', 'ravens-egg-2024' );
			?></a>
    </div>

    <!-- Masthead -->
    <header class="container" id="branding">

        <?php if ( get_header_image() ) {
            printf( '<div id="site-header"><img src="%s" width="%s" height="%s" alt="%s"></div>', get_header_image(), absint( get_custom_header()->width ), absint(
                get_custom_header()->height ), esc_attr( get_bloginfo( 'name', 'display' ) ) );
        } ?>

        <div class="content">
            <div id="site-title">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>"
                   title="to home page">
                    <span><?php bloginfo( 'name' ); ?></span>
					<?php get_template_part( 'template-parts/image/svg-logo' ); ?>
                </a>
            </div>
			<?php if ( get_bloginfo( 'description' ) != null ) { ?>
                <div id="site-description"><?php echo html_entity_decode( get_bloginfo( 'description' ) );
					?></div>
			<?php } ?>
        </div><!-- .content -->
    </header><!-- #branding -->

    <!-- Main Navigation -->
    <nav class="container" id="access">

        <button type="button" id="open-menu">
            <svg class="menu" width="32" height="32">
                <use xlink:href="#icon-menu"></use>
            </svg>
            Menu
        </button>

        <button type="button" id="close-menu">
            <svg class="close" width="32" height="32">
                <use xlink:href="#icon-close"></use>
            </svg>
        </button>

		<?php $params = array(
			'theme_location'  => 'primary',
			'menu'            => 'Main Navigation',
			'container'       => '',
			'container_class' => '',
			'container_id'    => '',
			'menu_class'      => 'primary-menu',
			'menu_id'         => '',
			'echo'            => true,
			'fallback_cb'     => 'wp_page_menu',
			'before'          => '',
			'after'           => '',
			'link_before'     => '',
			'link_after'      => '',
			'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
			'depth'           => 0,
//			'walker'          =>
		);

		wp_nav_menu( $params ); ?>

		<?php get_search_form() ?>

    </nav><!-- #access -->