<?php
/**
 * Index/archive content for cpt section
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */
?>

<div class="card">

	<?php $terms = get_the_terms( $post->ID, 'custom_taxonomy' );
	if ( $terms ) {
		$term = array_pop( $terms );
		echo '<div class="custom-taxonomy">';
		echo '<a href="' . get_term_link( $term->slug, 'custom_taxonomy' ) . '">' . $term->name . '</a>';
		echo '</div>';
	} ?>

	<?php RavensEgg2024\insert_featured_image_card_thumbnail(); ?>

    <header class="post-header">
        <h2 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    </header>

    <div class="entry-content">
		<?php the_excerpt(); ?>

		<?php if ( has_excerpt() ) { ?>
            <p class="more-link"><p class="more-link"><a class="button ghost arrow"
                                    href="<?php the_permalink(); ?>"><?php _e( 'Read more',
						'ravens-egg-2024' ); ?></a></p>
		<?php } ?>
    </div>

</div>