<?php
/**
 * Intro content for CPT archives
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */

// Insert intro content from intro CPT

$archive_title = post_type_archive_title( '', false );

$args = array(
	'post_type' => 'intro',
	'name'      => $archive_title . '-intro',
);

$introQuery = new WP_Query( $args );

if ( $introQuery->have_posts() ) : ?>

    <div class="entry-content">

		<?php while ( $introQuery->have_posts() ) : $introQuery->the_post(); ?>

			<?php the_content(); ?>

		<?php endwhile; ?>

    </div>

<?php endif; ?>
<?php wp_reset_postdata(); ?>