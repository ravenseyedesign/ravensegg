<?php
/**
 * 404 page content
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */
?>

<article class="page 404 redirected">

    <h1 id="page-title"><?php _e( 'I&rsquo;m Invisible', 'ravens-egg-2024' ); ?></h1>

    <div class="entry-content">

        <h2><?php _e( 'This single view is redirected', 'ravens-egg-2024' );
			?></h2>

        <p><em><?php _e( 'This screen is only visible to logged-in website administrators, not to anyone else.',
		        'ravens-egg-2024' ); ?></em></p>

            <div class="inner">
				<?php the_content(); ?>
            </div>

    </div>

	<?php edit_post_link( __( 'Edit', 'ravens-egg-2024' ), '<p class="edit-link">', '</p>', null, 'button' ); ?>

</article>