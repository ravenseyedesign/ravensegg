<?php
/**
 * Single cpt content
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */
?>

<article <?php post_class(); ?>>

    <div class="entry-content">

        <h1 class="cpt-title"><?php the_title(); ?></h1>

		<?php the_content(); ?>

    </div><!-- .entry-content -->

	<?php edit_post_link( __( 'Edit', 'ravens-egg-2024' ), '<p class="edit-link">', '</p>', null, 'button' ); ?>

</article>