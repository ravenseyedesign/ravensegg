<?php
/**
 * Wide page start wrapper
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */
?>

<div class="container wide" id="primary">

        <main>