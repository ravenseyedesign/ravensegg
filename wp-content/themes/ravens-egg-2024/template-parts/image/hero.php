<?php
/**
 * Hero image
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */
?>

<?php
$position = get_field( 'hero_image_caption_position' );
$color    = get_field( 'hero_image_caption_color' );

$header_image = get_header_image();
$caption_link = get_field( 'hero_image_caption_link' );
if ( ! empty( $header_image ) ) : ?>
    <figure id="hero">
        <img src="<?php header_image(); ?>"
             width="<?php echo get_custom_header()->width; ?>"
             height="<?php echo get_custom_header()->height; ?>"
             alt="<?php bloginfo( 'description' ); ?>"/>
        <figcaption class="<?php echo $position . ' ' . $color; ?>">
			<?php if ( $caption_link ) { ?><a href="<?php echo $caption_link; ?>"><?php } ?>
				<?php bloginfo( 'description' ); ?>
				<?php if ( $caption_link ) { ?></a><?php } ?>
        </figcaption>
    </figure>

<?php endif; ?>