<?php
/**
 * Prevent lazy loading on page template with this included.
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */
?>

<?php

/**
 * Skip lazy loading on this page template.
 *
 * @param $apply_lazyload
 *
 * @return bool
 */
function skip_a3_lazy_load_for_this_page( $apply_lazyload ) {

	$apply_lazyload = false;

	return $apply_lazyload;
}

add_filter( 'a3_lazy_load_run_filter', 'skip_a3_lazy_load_for_this_page', 11 );

?>