<?php
/**
 * Page end wrapper
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024 * @since Ravens_Egg_2024 1.0
 */
?>

    </main>

<?php get_sidebar(); ?>

    </div><!-- .content -->

    </div><!-- #primary.sidebar -->

<?php get_footer(); ?>