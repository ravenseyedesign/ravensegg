<?php
/**
 * Wide Page end wrapper
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */
?>


    </main>

    </div><!-- #primary.wide -->

<?php get_footer(); ?>