<?php
/**
 * Index/archive content
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */
?>

<article <?php post_class(); ?>>

    <header class="post-header">

        <h2 class="post-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>

        <div class="entry-meta">

            <span class="date">
                <svg width="24" height="24">
                    <use xlink:href="#icon-date"></use>
                </svg>
                <time datetime="<?php the_time( 'Y-m-d' ); ?>" class="timestamp updated">
                    <?php the_time( 'M j, Y' ); ?>
                </time>
            </span>

            <span class="author vcard">
				<span class="fn">
					<svg width="24" height="24">
						<use xlink:href="#icon-user"></use>
					</svg>
					<?php _e( 'By ', 'ravens-egg-2024' ); ?>
					<?php the_author(); ?>
				</span>
			</span>

			<?php if(has_category()) { ?>
            <span class="categories">
				<svg width="24" height="24">
					<use xlink:href="#icon-category"></use>
				</svg>
				<?php _e( 'Posted in', 'ravens-egg-2024' ); ?>
				<?php the_category( ', ' ); ?>
			</span>
			<?php } ?>

			<?php $tag_list = get_the_tag_list( '', ', ', '' );
			if ( $tag_list != '' ) { ?>
                <span class="tags">
					<svg width="24" height="24">
						<use xlink:href="#icon-tag"></use>
					</svg>
					<?php _e( 'Tagged with', 'ravens-egg-2024' ); ?>
					<?php echo $tag_list; ?>
				</span>
			<?php } ?>

			<?php if ( get_comments_number() > 0 ) : ?>
                <span class="comments">
					<svg width="24" height="24">
						<use xlink:href="#icon-comments"></use>
					</svg>
					<?php comments_popup_link( __( ' 0 <span>Comments</span>', 'ravens-egg-2024' ), __( ' 1 <span>Comment</span>', 'ravens-egg-2024' ), __( ' % <span>Comments</span>', 'ravens-egg-2024' ), '', '' ); ?>
				</span>
			<?php endif; ?>

        </div><!-- .entry-meta -->

    </header><!-- .post-header -->

    <div class="entry-content">

		<?php // Add featured image that links to single post
		if ( has_post_thumbnail() ) : ?>
            <figure>
                <a href="<?php the_permalink(); ?>">
					<?php the_post_thumbnail( 'medium', array( 'class' => 'alignright scale-with-grid' ) ); ?>
                </a>
            </figure>

		<?php endif; ?>

		<?php the_excerpt(); ?>

		<?php if ( has_excerpt() ) { ?>
            <p><a class="more-link" href="<?php echo get_permalink(); ?>"><?php _e('Read more', 'ravens-egg-2024'); ?></a></p>
		<?php } ?>

    </div><!-- .entry-content -->

</article>
