<?php
/**
 * Posts displayed as cards
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */

namespace RavensEgg2024;

// Get Author Data
$author = get_the_author();
$author_description = get_the_author_meta( 'description' );
$author_website = esc_url( get_the_author_meta( 'user_url' ) );
$author_avatar = get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'RavensEgg2024\author_bio_avatar_size',
	100
) );
$author_url = esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) );

// Only display if author has a description
if ( $author_description ) : ?>

    <h4 id="author-bio"><?php printf( __( 'About the Author', 'ravens-egg-2024' ), $author ); ?></h4>
    <div class="author-info">
		<?php if ( $author_avatar ) { ?>
            <div class="author-avatar">
                <!--<a href="--><?php //echo $author_url; ?><!--" rel="author">-->
				<?php echo $author_avatar; ?>
                <!--</a>-->
            </div><!-- .author-avatar -->
		<?php } ?>
        <div class="author-description">
            <h5 class="author-name"><?php echo $author; ?></h5>
            <p><?php echo $author_description; ?></p>
			<?php if ( $author_website ) : ?>
                <p id="author-urls">
                    <a class="author-website" href="<?php echo $author_website; ?>" target="_blank"
                       title="<?php _e( 'View author website', 'ravens-egg-2024' ); ?>">
						<?php _e( 'View author website', 'ravens-egg-2024' ); ?></a>
                </p>
			<?php endif; ?>
        </div><!-- .author-description -->
    </div><!-- .author-info -->

<?php endif; ?>