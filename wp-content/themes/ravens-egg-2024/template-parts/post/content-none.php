<?php
/**
 * 404 page content
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */
?>

<article class="error-page">

    <h1 id="page-title"><?php _e( 'Oops!', 'ravens-egg-2024' ); ?></h1>

    <h2><?php _e( 'There&rsquo;s nothing here!', 'ravens-egg-2024' ); ?></h2>

    <p><?php _e( 'We&rsquo;re sorry, but there is nothing here! The page you&rsquo;re looking for either doesn&rsquo;t exist, or the URL you followed or typed to get to it is incorrect in some way. Please use the navigation or search box on this page to try again.', 'ravens-egg-2024' ); ?></p>

</article>