<?php
/**
 * Index/archive content
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */
?>

<div class="card">
    <article <?php post_class(); ?>>

	<?php RavensEgg2024\insert_featured_image_card_thumbnail(); ?>

        <header class="post-header">

            <h2 class="post-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>

            <div class="entry-meta">

            <span class="date">
                <svg width="24" height="24">
                    <use xlink:href="#icon-date"></use>
                </svg>
                <time datetime="<?php the_time( 'Y-m-d' ); ?>" class="timestamp updated">
                    <?php the_time( 'M j, Y' ); ?>
                </time>
            </span>

            <span class="author vcard">
				<span class="fn">
					<svg width="24" height="24">
						<use xlink:href="#icon-user"></use>
					</svg>
					<?php the_author(); ?>
				</span>
			</span>
				
			<?php if(has_category()) { ?>
                <span class="categories">
				<svg width="24" height="24">
					<use xlink:href="#icon-category"></use>
				</svg>
					<?php the_category( ', ' ); ?>
				</span>
			<?php } ?>

				<?php $tag_list = get_the_tag_list( '', ', ', '' );
				if ( $tag_list != '' ) { ?>
                    <span class="tags">
					<svg width="24" height="24">
						<use xlink:href="#icon-tag"></use>
					</svg>
						<?php echo $tag_list; ?>
				</span>
				<?php } ?>

				<?php if ( get_comments_number() > 0 ) : ?>
                    <span class="comments">
					<svg width="24" height="24">
						<use xlink:href="#icon-comments"></use>
					</svg>
						<?php comments_popup_link( __( ' 0 <span>Comments</span>', 'ravens-egg-2024' ), __( ' 1 <span>Comment</span>', 'ravens-egg-2024' ), __( ' % <span>Comments</span>', 'ravens-egg-2024' ), '', '' ); ?>
				</span>
				<?php endif; ?>

            </div><!-- .entry-meta -->

        </header><!-- .post-header -->

        <div class="entry-content">

			<?php the_excerpt(); ?>

			<?php if ( has_excerpt() ) { ?>
                <p class="more-link"><a class="button ghost arrow"
                                        href="<?php the_permalink(); ?>"><?php _e( 'Read more',
							'ravens-egg-2024' ); ?></a></p>
			<?php } ?>

        </div><!-- .entry-content -->

    </article>
</div>