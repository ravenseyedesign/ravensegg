<?php
/**
 * Post navigation, uses this plugin:
 * https://wordpress.org/plugins/ambrosite-nextprevious-post-link-plus/
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */
?>

<?php if ( function_exists( 'previous_post_link_plus' ) || function_exists( 'next_post_link_plus' ) ) { ?>

    <div class="container" id="post-navigation">
        <div class="screen-reader-text"><?php _e( 'Post navigation', 'ravens-egg-2024' ); ?></div>

        <p class="nav-prev">&nbsp;

			<?php if ( function_exists( 'previous_post_link_plus' ) ) :
				previous_post_link_plus( array(
					'order_by'       => 'post_date',
//				'order_by'       => 'menu_order',
					'format'         => ' %link',
					'link'           => '%title',
					'tooltip'        => 'Previous',
					'ex_cats'        => '',
					'ex_cats_method' => 'diff',
					'in_same_cat'    => true,
					'in_cats'        => '',
					'before'         => '<svg class="arrow-left" width="32" height="32"><use xlink:href="#arrow-left"></use></svg> ',
					'after'          => '',
				) );

			else :

				previous_posts_link( '&laquo; %link' );

			endif; ?>

        </p><!-- .nav-prev -->

        <p class="nav-next">&nbsp;

			<?php if ( function_exists( 'next_post_link_plus' ) ) :

				next_post_link_plus( array(
					'order_by'       => 'post_date',
//				'order_by'       => 'menu_order',
					'format'         => '%link ',
					'link'           => '%title',
					'tooltip'        => 'Next',
					'ex_cats'        => '',
					'ex_cats_method' => 'diff',
					'in_same_cat'    => true,
					'in_cats'        => '',
					'before'         => '',
					'after'          => ' <svg class="arrow-right" width="32" height="32"><use xlink:href="#arrow-right"></use></svg>',
				) );

			else :

				next_posts_link( '&raquo; %link' );

			endif; ?>

        </p><!-- .nav-next -->

    </div><!-- #post-navigation -->

<?php }

// All params available (plugin documentation has been abandoned):

//if ( function_exists( 'next_post_link_plus' ) ) {
//	next_post_link_plus( array(
//		'order_by'       => 'post_date',
//		'order_2nd'      => 'post_date',
//		'meta_key'       => '',
//		'post_type'      => '',
//		'loop'           => false,
//		'end_post'       => false,
//		'thumb'          => false,
//		'max_length'     => 0,
//		'format'         => '%link &raquo;',
//		'link'           => '%title',
//		'date_format'    => '',
//		'tooltip'        => '%title',
//		'in_same_cat'    => false,
//		'in_same_tax'    => false,
//		'in_same_format' => false,
//		'in_same_author' => false,
//		'in_same_meta'   => false,
//		'ex_cats'        => '',
//		'ex_cats_method' => 'weak',
//		'in_cats'        => '',
//		'ex_posts'       => '',
//		'in_posts'       => '',
//		'before'         => '',
//		'after'          => '',
//		'num_results'    => 1,
//		'return'         => ''
//	) );
//
//}
//
//if ( function_exists( 'previous_post_link_plus' ) ) {
//	previous_post_link_plus( array(
//		'order_by'       => 'post_date',
//		'order_2nd'      => 'post_date',
//		'meta_key'       => '',
//		'post_type'      => '',
//		'loop'           => false,
//		'end_post'       => false,
//		'thumb'          => false,
//		'max_length'     => 0,
//		'format'         => '%link &raquo;',
//		'link'           => '%title',
//		'date_format'    => '',
//		'tooltip'        => '%title',
//		'in_same_cat'    => false,
//		'in_same_tax'    => false,
//		'in_same_format' => false,
//		'in_same_author' => false,
//		'in_same_meta'   => false,
//		'ex_cats'        => '',
//		'ex_cats_method' => 'weak',
//		'in_cats'        => '',
//		'ex_posts'       => '',
//		'in_posts'       => '',
//		'before'         => '',
//		'after'          => '',
//		'num_results'    => 1,
//		'return'         => ''
//	) );
//
//}