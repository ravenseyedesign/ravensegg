<?php
/**
 * Single post content
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */
?>

<article <?php post_class(); ?>>

    <div class="post-header">
        <h1 id="post-title"><?php the_title(); ?></h1>

        <p class="entry-meta">

            <span class="date">
                <svg width="24" height="24">
                    <use xlink:href="#icon-date"></use>
                </svg>
                <time datetime="<?php the_time( 'Y-m-d' ); ?>" class="timestamp updated">
                    <?php the_time( 'M j, Y' ); ?>
                </time>
            </span>

            <span class="author vcard">
				<span class="fn">
					<svg width="24" height="24">
						<use xlink:href="#icon-user"></use>
					</svg>
					<?php _e( "By ", "ravens-egg-2024" );
					$author_description = get_the_author_meta( 'description' );
					if ( $author_description ) {
						printf( ' <a href="#author-bio">%s</a>', get_the_author() );
					} else {
						the_author();
					} ?>
				</span>
			</span>

			<?php if ( has_category() ) { ?>
                <span class="categories">
				<svg width="24" height="24">
					<use xlink:href="#icon-category"></use>
				</svg>
				<?php _e( 'Posted in', 'ravens-egg-2024' ); ?>
					<?php the_category( ', ' ); ?>
			</span>
			<?php } ?>

			<?php $tag_list = get_the_tag_list( '', ', ', '' );
			if ( $tag_list != '' ) : ?>
                <span class="tags">
					<svg width="24" height="24">
						<use xlink:href="#icon-tag"></use>
					</svg>
					<?php _e( ' Tagged with', 'ravens-egg-2024' ); ?>
					<?php echo $tag_list; ?>
				</span>
			<?php endif; ?>

			<?php if ( get_comments_number() > 0 ) : ?>
                <span class="comments">
					<svg width="24" height="24">
						<use xlink:href="#icon-comments"></use>
					</svg>
					<?php comments_popup_link( __( ' 0 <span>Comments</span>', 'ravens-egg-2024' ), __( ' 1 <span>Comment</span>', 'ravens-egg-2024' ), __( ' % <span>Comments</span>', 'ravens-egg-2024' ), '', '' ); ?>
				</span>
			<?php endif; ?>

        </p><!-- .entry-meta -->

    </div><!-- .post-header -->

    <div class="entry-content">

		<?php if ( ! has_tag( 'Videos' ) ) {

			RavensEgg2024\insert_lightbox_featured_image();

		} ?>

		<?php the_content(); ?>

    </div><!-- .entry-content -->

	<?php if ( is_singular( 'post' ) ) {
		get_template_part( 'template-parts/post/author-bio' );
	} ?>

	<?php edit_post_link( __( 'Edit', 'ravens-egg-2024' ), '<p class="edit-link">', '</p>', null, 'button' ); ?>

</article>
