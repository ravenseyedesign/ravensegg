<?php
/**
 * Faux table content
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */
?>

<div class="table specific-class">
    <div class="row header">
        <div class="cell header-one">Header One</div>
        <div class="cell header-two">Header Two</div>
        <div class="cell header-three">Header Three</div>
    </div><!-- .row.header -->
    <div class="row">
        <div class="cell header-one" data-header="Header One">Content</div>
        <div class="cell header-two" data-header="Header Two">Content 2</div>
        <div class="cell header-three" data-header="Header Three">Content 3</div>
    </div><!-- .row -->
    <div class="row">
        <div class="cell header-one" data-header="Header One">Content 4</div>
        <div class="cell header-two" data-header="Header Two">Content 5</div>
        <div class="cell header-three" data-header="Header Three">Content 6</div>
    </div><!-- .row -->
    <div class="row">
        <div class="cell header-one" data-header="Header One">Content 7</div>
        <div class="cell header-two" data-header="Header Two">Content 8</div>
        <div class="cell header-three" data-header="Header Three">Content 9</div>
    </div><!-- .row -->
</div><!-- .table -->