<?php
/**
 * Page content
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */
?>

<article <?php post_class(); ?>>

	<?php RavensEgg2024\insert_lightbox_featured_image(); ?>

    <div class="entry-content">

		<?php the_content(); ?>

    </div><!-- .entry-content -->

	<?php edit_post_link( __( 'Edit', 'ravens-egg-2024' ), '<p class="edit-link">', '</p>', null, 'button' ); ?>

</article>