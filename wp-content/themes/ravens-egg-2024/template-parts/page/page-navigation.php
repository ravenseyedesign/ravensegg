<?php
/**
 * Page navigation, uses this plugin:
 * https://wordpress.org/plugins/ambrosite-nextprevious-page-link-plus/
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */
?>

<?php if ( function_exists( 'previous_page_link_plus' ) || function_exists( 'next_page_link_plus' ) ) { ?>

    <div class="container" id="page-navigation">
        <div class="screen-reader-text"><?php _e( 'Page navigation', 'ravens-egg-2024' ); ?></div>

        <p class="nav-prev">

			<?php if ( function_exists( 'previous_page_link_plus' ) ) :
				previous_page_link_plus( array(

					'order_by'    => 'menu_order',
					'order_2nd'   => 'post_title',
//				'meta_key'       => '',
//				'loop'           => false,
//				'end_page'       => false,
//				'thumb'          => false,
//				'max_length'     => 0,
					'format'      => ' %link',
					'link'        => '%title',
					'tooltip'     => 'Previous',
//				'in_same_parent' => false,
//				'in_same_author' => false,
//				'in_same_meta'   => false,
//				'ex_pages'       => '',
//				'in_pages'       => '',
					'before'      => '<svg class="arrow-left" width="32" height="32"><use xlink:href="#arrow-left"></use></svg> ',
					'after'       => '',
					'num_results' => 1,
//				'return'         => '',
				) );

			endif; ?>

        </p><!-- .nav-prev -->

        <p class="nav-next">&nbsp;

			<?php if ( function_exists( 'next_page_link_plus' ) ) :

				next_page_link_plus( array(

					'order_by'    => 'menu_order',
					'order_2nd'   => 'post_title',
//				'meta_key'       => '',
//				'loop'           => false,
//				'end_page'       => false,
//				'thumb'          => false,
//				'max_length'     => 0,
					'format'      => '%link ',
					'link'        => '%title',
					'tooltip'     => 'Next',
//				'in_same_parent' => false,
//				'in_same_author' => false,
//				'in_same_meta'   => false,
//				'ex_pages'       => '',
//				'in_pages'       => '',
					'before'      => '',
					'after'       => ' <svg class="arrow-right" width="32" height="32"><use xlink:href="#arrow-right"></use></svg>',
					'num_results' => 1,
//				'return'         => '',
				) );

			endif; ?>

        </p><!-- .nav-next -->

    </div><!-- #page-navigation -->

<?php }

// All params available (plugin documentation has been abandoned):

//if ( function_exists( 'next_page_link_plus' ) ) {
//	next_page_link_plus( array(
//		'format'         => '%link &raquo;',
//		'link'           => 'Next',
//		'in_same_parent' => true,
//		'ex_pages'       => '',
//		'in_pages'       => '',
//		'before'         => '<div class="nav-right">',
//		'after'          => '</div>',
//		'num_results'    => 1,
//		'return'         => ''
//	) );
//}
//
//
//if ( function_exists( 'previous_page_link_plus' ) ) {
//	previous_page_link_plus( array(
//		'format'         => '&laquo; %link',
//		'link'           => 'Previous',
//		'in_same_parent' => true,
//		'ex_pages'       => '',
//		'in_pages'       => '',
//		'before'         => '<div class="nav-left">',
//		'after'          => '</div>',
//		'num_results'    => 1,
//		'return'         => ''
//	) );
//}