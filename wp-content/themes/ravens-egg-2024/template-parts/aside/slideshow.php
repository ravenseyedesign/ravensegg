<?php
/**
 * Glide slideshow
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */
?>

<div id="slideshow">

	<?php

	$images = get_field( 'slider_images' );

	if ( $images ): ?>
    <div class="glide">
        <div class="glide__track" data-glide-el="track">
            <ul class="glide__slides">
				<?php foreach ( $images as $image ):

					$imgSrc  = $image['sizes']['slideshow'];
					$imgAlt  = $image['alt'] ?: 'slide-image';
					$caption = $image['caption'];

					printf( '<li class="glide__slide"><figure><img src="%s" alt="%s" />', $imgSrc, $imgAlt );
					if ( $caption ) {
						printf( '<figcaption>%s</figcaption>', $caption );
					}
					echo '</figure></li>';

				endforeach; ?>

            </ul><!-- .glide__slides -->

            <div class="glide__arrows" data-glide-el="controls">
                <button class="glide__arrow glide__arrow--left" data-glide-dir="<">prev</button>
                <button class="glide__arrow glide__arrow--right" data-glide-dir=">">next</button>
            </div>

			<?php endif; ?>

        </div><!-- .glide__track -->
    </div><!-- .glide -->
</div><!-- #slideshow -->