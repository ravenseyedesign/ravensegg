<?php
/**
 * Social profile links set by Theme Customizer
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */
?>

<?php if ( get_theme_mod( 'display_social_links' ) == 1 ) : ?>

	<?php get_template_part( 'template-parts/image/svg-sprite-icons-social' ); ?>

    <aside id="social-links" class="widget">

        <!--        <h3 class="widget-title">Follow</h3>-->

        <ul>

			<?php if ( is_category() ) :

				$category = get_category( get_query_var( 'cat' ) );
				$category_name = $category->name;
				$category_feed_link = get_category_feed_link( $category->cat_ID );

				if ( ! empty( $category ) ) : ?>

                    <li><a class="rss" title="RSS Feed for <?php echo $category_name; ?>"
                           href="<?php echo $category_feed_link; ?>" rel="nofollow">
                            <svg class="rss" width="32" height="32">
                                <use xlink:href="#icon-rss"></use>
                            </svg>
                            <span><?php echo $category_name; ?>RSS</span></a></li>

				<?php endif;
			else : ?>

                <li><a class="rss" title="RSS Feed for entire site" href="<?php bloginfo( 'rss2_url' ); ?>"
                       rel="nofollow">
                        <svg class="rss" width="32" height="32">
                            <use xlink:href="#icon-rss"></use>
                        </svg>
                        <span>RSS</span></a></li>

			<?php endif; ?>

			<?php if ( get_theme_mod( 'facebook_field' ) != '' ) : ?>
                <li><a class="facebook" title="Facebook" href="<?php echo esc_url( get_theme_mod( 'facebook_field', ''
					) );
					?>"
                       target="_blank">
                        <svg class="facebook" width="32" height="32">
                            <use xlink:href="#icon-facebook"></use>
                        </svg>
                        <span>Facebook</span></a></li>
			<?php endif; ?>

			<?php if ( get_theme_mod( 'instagram_field' ) != '' ) : ?>
                <li><a class="instagram" title="Instagram" href="<?php echo esc_url( get_theme_mod( 'instagram_field',
						'' ) );
					?>"
                       target="_blank">
                        <svg class="instagram" width="32" height="32">
                            <use xlink:href="#icon-instagram"></use>
                        </svg>
                        <span>Instagram</span></a></li>
			<?php endif; ?>

			<?php if ( get_theme_mod( 'linkedin_field' ) != '' ) : ?>
                <li><a class="linkedin" title="LinkedIn" href="<?php echo esc_url( get_theme_mod( 'linkedin_field', ''
					) );
					?>"
                       target="_blank">
                        <svg class="linkedin" width="32" height="32">
                            <use xlink:href="#icon-linkedin"></use>
                        </svg>
                        <span>LinkedIn</span></a></li>
			<?php endif; ?>

			<?php if ( get_theme_mod( 'pinterest_field' ) != '' ) : ?>
                <li><a class="pinterest" title="Pinterest" href="<?php echo esc_url( get_theme_mod( 'pinterest_field',
						'' ) );
					?>"
                       target="_blank">
                        <svg class="pinterest" width="32" height="32">
                            <use xlink:href="#icon-pinterest"></use>
                        </svg>
                        <span>Pinterest</span></a></li>
			<?php endif; ?>

			<?php if ( get_theme_mod( 'skype_field' ) != '' ) : ?>
                <li><a class="skype" title="Skype" href="<?php echo esc_url( get_theme_mod( 'skype_field', '' ) ); ?>"
                       target="_blank">
                        <svg class="skype" width="32" height="32">
                            <use xlink:href="#icon-skype"></use>
                        </svg>
                        <span>Skype</span></a></li>
			<?php endif; ?>

			<?php if ( get_theme_mod( 'x_field' ) != '' ) : ?>
                <li><a class="x" title="X" href="<?php echo esc_url( get_theme_mod( 'x_field', '' ) );
					?>"
                       target="_blank">
                        <svg class="x" width="32" height="32">
                            <use xlink:href="#icon-x"></use>
                        </svg>
                        <span>X</span></a></li>
			<?php endif; ?>

			<?php if ( get_theme_mod( 'vimeo_field' ) != '' ) : ?>
                <li><a class="vimeo" title="Vimeo" href="<?php echo esc_url( get_theme_mod( 'vimeo_field', '' ) ); ?>"
                       target="_blank">
                        <svg class="vimeo" width="32" height="32">
                            <use xlink:href="#icon-vimeo"></use>
                        </svg>
                        <span>Vimeo</span></a></li>
			<?php endif; ?>

			<?php if ( get_theme_mod( 'youtube_field' ) != '' ) : ?>
                <li><a class="youtube" title="YouTube" href="<?php echo esc_url( get_theme_mod( 'youtube_field', '' ) );
					?>"
                       target="_blank">
                        <svg class="youtube" width="32" height="32">
                            <use xlink:href="#icon-youtube"></use>
                        </svg>
                        <span>YouTube</span></a></li>
			<?php endif; ?>

             <?php if ( get_theme_mod( 'tiktok_field' ) != '' ) : ?>
                <li><a class="tiktok" title="TikTok" href="<?php echo esc_url( get_theme_mod( 'tiktok_field', '' ) );
                    ?>"
                       target="_blank">
                        <svg class="tiktok" width="32" height="32">
                            <use xlink:href="#icon-tiktok"></use>
                        </svg>
                        <span>TikTok</span></a></li>
            <?php endif; ?>

        </ul>

    </aside>

<?php endif; ?>