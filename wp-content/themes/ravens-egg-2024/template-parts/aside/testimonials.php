<?php
/**
 * Custom Testimonials widget
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */
?>

<aside id="testimonials" class="widget">
    <h4 class="widgettitle">Testimonials</h4>

		<?php $args = array(
			'post_type'      => 'testimonial',
			'posts_per_page' => - 1,

		) ?>

		<?php
		$testimonials = get_posts( $args );

		if ( empty( $testimonials ) ) {

			printf( '<p>%s</p>', __( 'There were no testimonials.', 'ravens-egg-2024' ) );

		} else {

			$testimonial = $testimonials[ array_rand( $testimonials ) ];

			echo $testimonial->post_content;

		}

		?>

		<?php wp_reset_postdata(); ?>

    <a class="button" href="<?php echo esc_url( home_url( '/' ) ) ?>testimonials/">View More</a>
</aside>