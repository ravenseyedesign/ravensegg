<?php
/**
 * Search results page for WooCommerce content
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */
?>

<div class="store-card">
    <article <?php post_class(); ?>>

		<?php // Add featured image that links to single post
		if ( has_post_thumbnail() ) : ?>
            <figure>
                <a class="linked-image" href="<?php the_permalink(); ?>">
					<?php the_post_thumbnail( 'medium' ); ?>
                </a>
            </figure>

		<?php endif; ?>

        <header class="post-header">

            <h2 class="post-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>

        </header>

    </article>
</div>