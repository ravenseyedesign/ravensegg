<?php
/**
 * Custom recent posts widget
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */
?>

<?php $args =
	array(
		'tag'            => 'featured',
		'posts_per_page' => 3,
		'cat'            => 'blog'
	);
?>

<?php $recent_posts_loop = new WP_Query( $args ); ?>

<?php if ( $recent_posts_loop->have_posts() ) { ?>

    <aside id="recent-posts" class="widget">
        <h3>Recent Posts</h3>
        <ul>

			<?php while ( $recent_posts_loop->have_posts() ) : $recent_posts_loop->the_post(); ?>
                <li<?php if ( is_singular() && $post->ID == $wp_query->post->ID ) {
					echo ' class="current-menu-item"';
				} ?>>
                    <a href="<?php the_permalink(); ?>">

						<?php the_title(); ?>

						<?php $date    = get_the_date( 'Y-m-d' );
						$dateFormatted = get_the_date( 'M j, Y' );

						echo( '<time datetime="' . $date . '">' . $dateFormatted .
						      '</time>' ); ?>

                    </a>
                </li>

			<?php endwhile; ?>

        </ul>
    </aside>

<?php } ?>

<?php wp_reset_postdata(); ?>