<?php
/**
 * Tag index/archive catch-all
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */

get_header(); ?>

<?php get_template_part( 'template-parts/header/start-wide' ); ?>

    <h1 id="section-title"><?php single_tag_title() ?></h1>

    <div class="cards tags">

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'template-parts/post/content-archive-cards' ); ?>

<?php endwhile; ?>

    </div><!-- .cards.tags -->

	<?php if ( function_exists( 'wp_pagenavi' ) ) {
		echo '<div class="pagenavi-container">';
		wp_pagenavi();
		echo '</div>';
	} ?>

<?php else : ?>

	<?php get_template_part( 'template-parts/post/content-none' ); ?>

<?php endif; ?>

<?php get_template_part( 'template-parts/footer/end-wide' ); ?>