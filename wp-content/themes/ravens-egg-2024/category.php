<?php
/**
 * Catch-all category index
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */

get_header(); ?>

<?php get_template_part( 'template-parts/header/start-wide' ); ?>

    <h1 id="section-title"><?php single_cat_title() ?></h1>

    <div class="cards blog">

<?php global $cat;
$categoryVariable = $cat; // assign the variable as current category ?>

<?php $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1; // Set up pagination ?>

<?php $args = array(
	'cat'   => $categoryVariable,
	'paged' => $paged,
); ?>

<?php query_posts( $args ); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'template-parts/post/content-archive-cards' ); ?>

<?php endwhile; ?>

    </div><!-- .cards.blog -->

	<?php if ( function_exists( 'wp_pagenavi' ) ) {
		echo '<div class="pagenavi-container">';
		wp_pagenavi();
		echo '</div>';
	} ?>

<?php else : ?>

	<?php get_template_part( 'template-parts/post/content-none' ); ?>

<?php endif; ?>

<?php get_template_part( 'template-parts/footer/end-wide' ); ?>