<?php
/**
 * 404 page template
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */


get_header(); ?>

<?php get_template_part( 'template-parts/header/start' ); ?>

<?php get_template_part( 'template-parts/post/content-none' ); ?>

<?php get_template_part( 'template-parts/footer/end' ); ?>
