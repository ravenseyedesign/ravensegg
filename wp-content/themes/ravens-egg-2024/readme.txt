=== Raven's Egg starter theme ===
Contributors: Raven's Eye Design
Requires at least: 5.9
Tested up to: 5.9
Requires PHP: 7.4
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Copyright ==

Raven's Egg WordPress Theme, Copyright 2022 Chad Bush
Raven's Egg is distributed under the terms of the GNU GPL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see https://www.gnu.org/licenses/gpl-3.0.html.

== Author ==

* by Chad Bush at Raven's Eye Design, https://ravenseyedesign.com/

== Description ==

This theme is not intended to be used as is. It's a starter theme created, maintained and used for custom website development by Raven's Eye Design.

== Important Notes ==

=  Raven's Egg is hand-built using open source components from all over the place. It's intended for use as a starting point when creating a new custom theme from scratch. Special thanks to the Bones theme, the Skeleton grid and Bootstrap for inspiration and code snippets.

== Changelog ==

= 0.1 =
* This is the first version of this theme, released 01/2010.

= 0.5 =
* Updated mega-menu option. To deploy mega-menus, do the following:
	- Uncomment imports in /src/scss/screen.scss for /src/scss/inc/base/_megamenu.scss and
	/src/scss/inc/responsive/_972up-megamenu.scss
	- Uncomment mega-menu variables in /src/scss/settings/variables.scss
	- Uncomment megamenu.js import in /src/webpack.common.js
	- Uncomment registration and enqueue for megamenu.js in /inc/enqueue.php
	- Add classes and structure as needed to menu items:
		- .mega-menu-item.three-wide (top level, number variable)
		- .mega-column (level 2, named 'Column 1', 'Column 2', etc., hidden)
		- .mega-subhead (level 3)
		- Live links (level 4)

