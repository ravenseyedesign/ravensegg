<?php
/**
 * Single cpt custom post type
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */

get_header(); ?>

<?php get_template_part( 'template-parts/header/start' ); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php $terms = get_the_terms( $post->ID, 'custom_taxonomy' );
	if ( $terms ) {
		$term = array_pop( $terms );
		echo '<h2 class="page-heading">' . $term->name . '</h2>';
	} ?>

	<?php get_template_part( 'template-parts/cpt/content-single-cpt' ); ?>

<?php endwhile; ?>

<?php get_template_part( 'template-parts/post/single-post-navigation' ); ?>

<?php get_template_part( 'template-parts/footer/end' ); ?>