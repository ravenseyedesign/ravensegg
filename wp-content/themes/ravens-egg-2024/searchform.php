<?php
/**
 * Search form
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */
?>

<div id="search">
    <svg class="search" width="24" height="24">
        <use xlink:href="#icon-search"></use>
    </svg>
    <form method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
        <label>
            <span class="screen-reader-text"><?php echo _x( 'Search for:', 'label', 'ravens-egg-2024' ) ?></span>
            <input type="search" class="search-field"
                   placeholder="<?php echo esc_attr_x( 'Search...', 'placeholder', 'ravens-egg-2024' ) ?>"
                   value="<?php echo get_search_query() ?>" name="s"
                   title="<?php echo esc_attr_x( 'Search for:', 'label', 'ravens-egg-2024' ) ?>"/>
        </label>
        <input type="submit" class="search-submit"
               value="<?php echo esc_attr_x( 'Search', 'submit button', 'ravens-egg-2024' ) ?>"/>
    </form>
</div>