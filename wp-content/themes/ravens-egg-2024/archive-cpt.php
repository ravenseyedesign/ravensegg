<?php
/**
 * Work index
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */

get_header(); ?>

<?php get_template_part( 'template-parts/header/start-wide' ); ?>

<?php if ( have_posts() ) : ?>

    <div class="inner intro">
        <div class="section-head"><h1 id="section-title"><?php post_type_archive_title(); ?></h1></div>

		<?php get_template_part( 'template-parts/cpt/content-intro-archive-cpt' ); ?>

    </div>

    <div class="cards">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'template-parts/cpt/content-archive-cpt' ); ?>

		<?php endwhile; ?>

    </div><!-- .cards -->

	<?php if ( function_exists( 'wp_pagenavi' ) ) {
		echo '<div class="pagenavi-container">';
		wp_pagenavi();
		echo '</div>';
	} ?>

<?php else : ?>

    <div class="inner">

		<?php get_template_part( 'template-parts/post/content-none' ); ?>

    </div>

<?php endif; ?>

<?php wp_reset_query(); ?>

<?php get_template_part( 'template-parts/footer/end-wide' ); ?>