<?php
/**
 * Search results page
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */

// Get custom output for vendor CPT search results
// if ( 'cptname' == get_post_type() ) {
// get_template_part( 'search-vendor' );
// } else {

get_header(); ?>

<?php get_template_part( 'template-parts/header/start' ); ?>

<?php if ( have_posts() ) : ?>

    <div class="inner">

        <h1 id="page-title"><?php _e( 'Search Results', 'ravens-egg-2024' ); ?></h1>

        <h2><?php _e( 'Your search found the following items:', 'ravens-egg-2024' ); ?></h2>

		<?php while ( have_posts() ) : the_post(); ?>

            <article class="post">

                <h3 class="post-title"><a
                            href="<?php the_permalink() ?>"><?php if ( function_exists( 'relevanssi_the_title' ) ) {
							relevanssi_the_title();
						} else {
							the_title();
						} ?></a></h3>


				<?php if ( function_exists( 'relevanssi_the_excerpt' ) ) {
					relevanssi_the_excerpt();
				} else {
					the_excerpt();
				} ?>

            </article>

		<?php endwhile; ?>

		<?php if ( function_exists( 'wp_pagenavi' ) ) {
			echo '<div class="pagenavi-container">';
			wp_pagenavi();
			echo '</div>';
		} ?>

    </div>

<?php else : ?>

    <article <?php post_class(); ?>>
        <h1 id="page-title"><?php _e( 'Nothing Found', 'ravens-egg-2024' ); ?></h1>

        <h2><?php _e( 'Your search did not find anything.', 'ravens-egg-2024' ); ?></h2>

        <p>
			<?php _e( 'The terms you searched for were not found on this site. That doesn&rsquo;t mean the terms don&rsquo;t exist here. It might just mean that the search terms didn&rsquo;t match up exactly with what the website expected. Please use the navigation or search box to try again.', 'ravens-egg-2024' ); ?>
        </p>

    </article>

<?php endif; ?>

<?php get_template_part( 'template-parts/footer/end' ); ?>