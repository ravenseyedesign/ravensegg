<?php
/**
 * Custom dashboard widget
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */

namespace RavensEgg2024;

/**
 * Create a basic instructions area on the Dashboard
 *
 * @return void
 */
function instructions_dashboard_widget() {

	$homeURL        = get_home_url();
	$homeID         = url_to_postid( $homeURL );
	$homeEditScreen = admin_url( 'post.php?post=' . $homeID . '&action=edit' );

	printf( '<h2>Please use the links on the left to add, edit and manage website content.</h2>
    <p><a class="button" href="%s">Edit Home Page</a></p>', $homeEditScreen );

}

/**
 * Add instructions widget.
 *
 * @return void
 */
function custom_dashboard_widgets() {
	wp_add_dashboard_widget( 'instructions_dashboard_widget', 'Website Instructions', 'RavensEgg2024\instructions_dashboard_widget' );
}

add_action( 'wp_dashboard_setup', 'RavensEgg2024\custom_dashboard_widgets' );