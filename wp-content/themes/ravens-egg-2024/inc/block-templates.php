<?php
/**
 * Gutenberg support
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */

namespace RavensEgg2024;

/**
 * Adds block template to pages.
 */
function register_block_template() {
	$post_type_object           = get_post_type_object( 'page' );
	$post_type_object->template = array(
		array(
			'core/heading',
			array(
				'level'       => 2,
				'placeholder' => 'Add heading text.',
				'className'   => 'custom-class',
			)
		),
		array(
			'core/paragraph',
			array(
				'placeholder' => 'Add paragraph text.',
				'className'   => 'custom-class',
			)
		),
		array(
			'core/block',
			array(
				'ref' => 100,// Set to reusable block
			),
		),
	);
}

add_action( 'init', 'RavensEgg2024\register_block_template' );


/**
 * Adds template to posttype (rename with actual CPT).
 *
 * @param $args
 * @param $post_type
 *
 * @return mixed
 */
function register_posttype_block_templates( $args, $post_type ) {

	if ( 'posttype' === $post_type ) {

		$args['template'] = [

			array(
				'ravenseyeblocks/cpt-chapter',
			),

			array(
				'core/block',
				array(
					'ref' => 100,// Set to reusable block
				),
			),
			array(
				'core/heading',
				array(
					'level'       => 2,
					'placeholder' => 'Add heading text.',
					'className'   => 'custom-class',
				)
			),
			array(
				'core/paragraph',
				array(
					'placeholder' => 'Add paragraph text.',
					'className'   => 'custom-class',
				)
			),

			array(
				'core/buttons',
				array(
					'className' => 'is-content-justification-right contact-link',
				),
				array(
					array(
						'core/button',
						array(
							'text'      => 'Contact',
							'url'       => '',
							'className' => 'is-style-outline',
						),
					),
				),
			),

		];
	}

	return $args;

}

add_filter( 'register_post_type_args', 'RavensEgg2024\register_posttype_block_templates', 20, 2 );