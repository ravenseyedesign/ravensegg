<?php
/**
 * Woocommerce mods
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */

namespace RavensEgg2024;

// ******************** WooCommerce Overall ************************ //

/**
 * Adds WooCommerce theme support.
 *
 * @package WooCommerce
 */
add_theme_support( 'woocommerce' );

remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );

/**
 * Adjusts WooCommerce pages content wrapper to match theme. Outer and inner wrappers work with re-positioned
 * woocommerce sidebar. Priorities control the placement of elements.
 *
 * @package WooCommerce
 */
function woocommerce_outer_wrapper_start() {
	echo '<div class="container" id="primary">
   <main>';
}

function woocommerce_main_wrapper_start() {
	echo '<article><div class="entry-content">';
}

function woocommerce_main_wrapper_end() {
	echo '</div></article>';
}

function woocommerce_outer_wrapper_end() {
	echo '</main></div>';
}

add_action( 'woocommerce_before_main_content', 'RavensEgg2024\woocommerce_outer_wrapper_start', 5 );
add_action( 'woocommerce_before_main_content', 'RavensEgg2024\woocommerce_main_wrapper_start', 10 );
add_action( 'woocommerce_after_main_content', 'RavensEgg2024\woocommerce_main_wrapper_end', 5 );
//add_action( 'woocommerce_after_main_content', 'woocommerce_get_sidebar', 10 );
add_action( 'woocommerce_after_main_content', 'RavensEgg2024\woocommerce_outer_wrapper_end', 15 );

/**
 * Change the breadcrumb separator
 */
//function change_breadcrumb_delimiter( $defaults ) {
//	$defaults['delimiter'] = ' &#10095; ';
//
//	return $defaults;
//}

//add_filter( 'woocommerce_breadcrumb_defaults', 'RavensEgg2024\change_breadcrumb_delimiter' );


// ******************** Product Index ************************ //

/**
 * Changes number of products per row.
 */
//if ( ! function_exists( 'loop_columns' ) ) {
//	function loop_columns() {
//		return 3; // 3 products per row
//	}
//}

// add_filter( 'loop_shop_columns', 'RavensEgg2024\loop_columns' );

/**
 * Change number of products that are displayed per page (shop page)
 *
 * @param $cols
 *
 * @return int
 */
//function new_loop_shop_per_page( $cols ) {
//	// $cols contains the current number of products per page based on the value stored on Options -> Reading
//	// Return the number of products you wanna show per page.
//	$cols = 12;
//
//	return $cols;
//}

// add_filter( 'loop_shop_per_page', 'RavensEgg2024\new_loop_shop_per_page', 20 );

/**
 * Change number of upsells output
 */
add_filter( 'woocommerce_upsell_display_args', 'RavensEgg2024\wc_change_number_upsell_products', 20 );

function wc_change_number_upsell_products( $args ) {

//  $args['posts_per_page'] = 1;
	$args['columns'] = 3; //change number of upsells here

	return $args;
}


// ******************** Single Product ************************ //

/**
 * Adds support for Woocommerce 3.0 gallery features.
 */
function woo_gallery_setup() {
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
}

add_action( 'after_setup_theme', 'RavensEgg2024\woo_gallery_setup' );

/**
 * Removes lightbox and link from single product image thumbnail.
 *
 * @param $html
 *
 * @return string
 */
// function wc_remove_link_on_thumbnails( $html ) {
// 	return strip_tags( $html,'<img>' );
// }

// add_filter('woocommerce_single_product_image_thumbnail_html','RavensEgg2024\wc_remove_link_on_thumbnails' );

/**
 * Removes single product excerpt from its default position and puts it back in further down.
 */
//remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
//add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 60 );

/**
 * Removes related products.
 *
 * @package WooCommerce
 */
//remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

/**
 * Moves product single title to top
 */
// remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
// add_action( 'woocommerce_before_single_product_summary', 'woocommerce_template_single_title', 1 );


/**
 * Removes "Description" heading from main product details.
 */
// add_filter( 'woocommerce_product_description_heading', '__return_null' );

/**
 * Remove some sorting types.
 *
 * @param $options
 *
 * @return mixed
 */
function remove_sorting_options_woocommerce_shop( $options ) {
	unset( $options['rating'] );
	unset( $options['popularity'] );
	unset( $options['date'] );

	return $options;
}

add_filter( 'woocommerce_catalog_orderby', 'RavensEgg2024\remove_sorting_options_woocommerce_shop' );

// Note: you can unset another default sorting option... here's the list:
// 'menu_order' => __( 'Default sorting', 'ravens-egg-2024' ),
// 'popularity' => __( 'Sort by popularity', 'ravens-egg-2024' ),
// 'rating'     => __( 'Sort by average rating', 'ravens-egg-2024' ),
// 'date'       => __( 'Sort by newness', 'ravens-egg-2024' ),
// 'price'      => __( 'Sort by price: low to high', 'ravens-egg-2024' ),
// 'price-desc' => __( 'Sort by price: high to low', 'ravens-egg-2024' ),


/**
 * Create account checkbox checked by default.
 */
add_filter( 'woocommerce_create_account_default_checked', '__return_true' );


/**
 * Removes breadcrumbs.
 *
 * @package WooCommerce
 */
//remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );

/**
 * Disables buggy select2 code in WC checkout.
 */
//function dequeue_stylesandscripts_select2() {
//	if ( class_exists( 'woocommerce' ) ) {
//		wp_dequeue_style( 'selectWoo' );
//		wp_deregister_style( 'selectWoo' );
//
//		wp_dequeue_script( 'selectWoo' );
//		wp_deregister_script( 'selectWoo' );
//	}
//}
//
//add_action( 'wp_enqueue_scripts', 'RavensEgg2024\dequeue_stylesandscripts_select2', 100 );

/**
 * Disables All WooCommerce  Styles and Scripts Except Shop Pages.
 */
function dequeue_woocommerce_styles_scripts() {
	if ( function_exists( 'is_woocommerce' ) ) {
		if ( ! is_woocommerce() && ! is_cart() && ! is_checkout() ) {
# Styles
			wp_dequeue_style( 'woocommerce-general' );
			wp_dequeue_style( 'woocommerce-layout' );
			wp_dequeue_style( 'woocommerce-smallscreen' );
			wp_dequeue_style( 'woocommerce_frontend_styles' );
			wp_dequeue_style( 'woocommerce_fancybox_styles' );
			wp_dequeue_style( 'woocommerce_chosen_styles' );
			wp_dequeue_style( 'woocommerce_prettyPhoto_css' );
# Scripts
			wp_dequeue_script( 'wc_price_slider' );
			wp_dequeue_script( 'wc-single-product' );
			wp_dequeue_script( 'wc-add-to-cart' );
			wp_dequeue_script( 'wc-cart-fragments' );
			wp_dequeue_script( 'wc-checkout' );
			wp_dequeue_script( 'wc-add-to-cart-variation' );
			wp_dequeue_script( 'wc-single-product' );
			wp_dequeue_script( 'wc-cart' );
			wp_dequeue_script( 'wc-chosen' );
			wp_dequeue_script( 'woocommerce' );
			wp_dequeue_script( 'prettyPhoto' );
			wp_dequeue_script( 'prettyPhoto-init' );
			wp_dequeue_script( 'jquery-blockui' );
			wp_dequeue_script( 'jquery-placeholder' );
			wp_dequeue_script( 'fancybox' );
			wp_dequeue_script( 'jqueryui' );
		}
	}
}

add_action( 'wp_enqueue_scripts', 'RavensEgg2024\dequeue_woocommerce_styles_scripts', 99 );

/**
 * Changes add to cart button on index to "View details".
 *
 * @param $button
 * @param $product
 *
 * @return string
 */
//function replace_add_to_cart_button( $button, $product  ) {
//	$button_text = __('View details', 'ravens-egg-2024');
//	$button = '<a class="button" href="' . $product->get_permalink() . '">' . $button_text . '</a>';
//
//	return $button;
//}

//add_filter( 'woocommerce_loop_add_to_cart_link', 'RavensEgg2024\replace_add_to_cart_button', 10, 2 );

//function display_sold_out_loop_woocommerce() {
//	global $product;
//
//	if ( !$product->is_in_stock() ) {
//		echo '<span class="sold-out">' . __( 'Sold Out', 'ravens-egg-2024' ) . '</span>';
//	}
//}

//add_action( 'woocommerce_before_shop_loop_item_title', 'RavensEgg2024\display_sold_out_loop_woocommerce' );

/**
 * Renames coupon field on the cart page.
 *
 * @param $translated_text
 * @param $text
 * @param $text_domain
 *
 * @return string
 */
//function woocommerce_rename_coupon_field_on_cart( $translated_text, $text, $text_domain ) {
//	// bail if not modifying frontend woocommerce text
//	if ( is_admin() || 'woocommerce' !== $text_domain ) {
//		return $translated_text;
//	}
//	if ( 'Apply Coupon' === $text ) {
//		$translated_text = 'New Coupon Name';
//	}
//
//	return $translated_text;
//}

//add_filter( 'gettext', 'RavensEgg2024\woocommerce_rename_coupon_field_on_cart', 10, 3 );

/**
 * Renames the "Have a Coupon?" message on the checkout page.
 *
 * @return string
 */
//function woocommerce_rename_coupon_message_on_checkout() {
//	return 'Have a Promo Code?' . ' <a href="#" class="showcoupon">' . __( 'New Coupon Name', 'ravens-egg-2024' ) .
//	       '</a>';
//}

//add_filter( 'woocommerce_checkout_coupon_message', 'RavensEgg2024\woocommerce_rename_coupon_message_on_checkout' );

/**
 * Renames coupon field on checkout page.
 *
 * @param $translated_text
 * @param $text
 * @param $text_domain
 *
 * @return string
 */
//function woocommerce_rename_coupon_field_on_checkout( $translated_text, $text, $text_domain ) {
//	if ( is_admin() || 'woocommerce' !== $text_domain ) {
//		return $translated_text;
//	}
//	if ( 'Coupon code' === $text ) {
//		$translated_text = 'New Coupon Name';
//
//	} elseif ( 'Apply Coupon' === $text ) {
//		$translated_text = 'Apply New Coupon Name';
//	}
//
//	return $translated_text;
//}

//add_filter( 'gettext', 'RavensEgg2024\woocommerce_rename_coupon_field_on_checkout', 10, 3 );

/**
 * Tests if is subcategory archive.
 *
 * @param null $cat_id
 *
 * @return bool
 */
//function is_subcategory( $cat_id = null ) {
//	if ( is_tax( 'product_cat' ) ) {
//
//		if ( empty( $cat_id ) ) {
//			$cat_id = get_queried_object_id();
//		}
//
//		$cat = get_term( get_queried_object_id(), 'product_cat' );
//		if ( empty( $cat->parent ) ) {
//			return false;
//		} else {
//			return true;
//		}
//	}
//
//	return false;
//}


/**
 * Detaches product detail description from tabs.
 */
//remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

/**
 * Remove Additional Information tab
 *
 * @param $tabs
 *
 * @return mixed
 */
//function remove_product_tabs( $tabs ) {
//	unset( $tabs['additional_information'] );
//
//	return $tabs;
//}

//add_filter( 'woocommerce_product_tabs', 'RavensEgg2024\remove_product_tabs', 98 );

/**
 * Change number of related products output
 */
//function woo_related_products_limit() {
//	global $product;
//
//	$args['posts_per_page'] = 3;
//
//	return $args;
//}

/**
 * Change number of related products and their columns.
 *
 * @param $args
 *
 * @return mixed
 */
//function related_products_args( $args ) {
//	$args['posts_per_page'] = 3; // 3 related products
//	$args['columns']        = 3; // arranged in 3 columns
//
//	return $args;
//}
//
//add_filter( 'woocommerce_output_related_products_args', 'RavensEgg2024\related_products_args' );

/**
 * Remove SKU display.
 */
//add_filter( 'wc_product_sku_enabled', '__return_false' );

/**
 * Remove product meta (categories)
 */
// remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

/**
 * Disables product reviews.
 *
 * @package WooCommerce
 */
//function woo_remove_reviews_tab( $tabs ) {
//	unset( $tabs['reviews'] );
//
//	return $tabs;
//}
//
//add_filter( 'woocommerce_product_tabs', 'RavensEgg2024\woo_remove_reviews_tab', 98 );

/**
 * Adds spans to WooCommerce page titles.
 *
 * @package WooCommerce
 */
//function woo_title( $page_title ) {
//	return '<span>' . $page_title . '</span>';
//}
//add_filter( 'woocommerce_page_title', 'RavensEgg2024\woo_title', 15 );

/**
 * Uses custom product placeholder image.
 *
 * @package WooCommerce
 */
//function custom_fix_thumbnail() {
//	add_filter( 'woocommerce_placeholder_img_src', 'custom_woocommerce_placeholder_img_src' );
//	function custom_woocommerce_placeholder_img_src( $src ) {
//		$upload_dir = wp_upload_dir();
//		$uploads    = untrailingslashit( $upload_dir['baseurl'] );
//		$src        = $uploads . '/product-default.jpg';
//
//		return $src;
//	}
//}
//add_action( 'init', 'RavensEgg2024\custom_fix_thumbnail' );

/**
 * Removes product images.
 *
 * @package WooCommerce
 */
//remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );
//remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );


/**
 * Removes CSV formatting from embedded Gravity Forms in products.
 *
 * @package WooCommerce
 */
//function configure_woocommerce_gforms_strip_meta_html( $strip_html ) {
//	$strip_html = false;
//
//	return $strip_html;
//}
//add_filter( 'RavensEgg2024\woocommerce_gforms_strip_meta_html',
// 'configure_woocommerce_gforms_strip_meta_html' );

/**
 * Makes default WP search only get products.
 *
 * @param $query
 */
//function search_woocommerce_only( $query ) {
//	if ( ! is_admin() && is_search() && $query->is_main_query() ) {
//		$query->set( 'post_type', 'product' );
//	}
//}

// add_action( 'pre_get_posts', 'RavensEgg2024\search_woocommerce_only' );

/**
 * Translates all matching strings in WooCommerce.
 *
 * @param $translated
 * @param $untranslated
 * @param $domain
 *
 * @return mixed|string
 */
// function translate_woocommerce_strings( $translated, $untranslated, $domain ) {

//     if ( ! is_admin() && 'woocommerce' === $domain ) {

//         switch ( $translated ) {

//             case 'Available on backorder':

//                 $translated = 'Pre-Order Now';
//                 break;

//         }

//     }

//     return $translated;

// }

// add_filter( 'gettext', 'RavensEgg2024\translate_woocommerce_strings', 999, 3 );


/** Removes '(can be backordered)' text from in stock items that can be backordered.
 *
 * @param $availability
 * @param $product
 *
 * @return array|mixed|string|string[]
 */
// function filter_product_availability_text( $availability, $product ) {

//     if ( $product->backorders_require_notification() ) {
//         $availability = str_replace( '(can be backordered)', '', $availability );
//     }

//     return $availability;
// }

// add_filter( 'woocommerce_get_availability_text', 'RavensEgg2024\filter_product_availability_text', 10, 2 );

/**
 * Change the default state and country on the checkout page
 */
//function change_default_checkout_country() {
//	return ''; // country code - leave blank to remove
//}
//
//function change_default_checkout_state() {
//	return ''; // state code - leave blank to remove
//}

//add_filter( 'default_checkout_billing_country', 'RavensEgg2024\change_default_checkout_country' );
//add_filter( 'default_checkout_billing_state', 'RavensEgg2024\change_default_checkout_state' );



remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
//add_filter( 'woocommerce_is_purchasable', '__return_false' );

// Remove the additional information title
add_filter( 'woocommerce_product_additional_information_heading', '__return_null' );

//add back to store button after cart
function add_back_to_shop() {
    printf( '<a href="%s">Return to Shop</a>', get_permalink( wc_get_page_id( 'shop' ) ) );
}

//add_action( 'woocommerce_single_product_summary', 'RavensEgg2024\add_back_to_shop', 35 );


//add_filter( 'woocommerce_catalog_orderby', 'RavensEgg2024\remove_sorting_options_woocommerce_shop' );


remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );

function replace_markup() {

    if ( is_shop() || is_post_type_archive() ) {

        /**
         * Adjusts WooCommerce pages content wrapper to match theme. Outer and inner wrappers work with re-positioned
         * woocommerce sidebar. Priorities control the placement of elements.
         *
         * @package WooCommerce
         */
        function woocommerce_shop_outer_wrapper_start() {
            echo '<div class="container sidebar" id="primary"><div class="content"><main>';
        }

        function woocommerce_shop_main_wrapper_start() {
            echo '<article><div class="entry-content">';
        }

        function woocommerce_shop_main_wrapper_end() {
            echo '</div></article></main>';
        }

        function woocommerce_shop_outer_wrapper_end() {
            echo '</div></div>';
        }

        add_action( 'woocommerce_before_main_content', 'RavensEgg2024\woocommerce_shop_outer_wrapper_start', 5 );
        add_action( 'woocommerce_before_main_content', 'RavensEgg2024\woocommerce_shop_main_wrapper_start', 10 );
        add_action( 'woocommerce_after_main_content', 'RavensEgg2024\woocommerce_shop_main_wrapper_end', 10 );
        add_action( 'woocommerce_after_main_content', 'woocommerce_get_sidebar', 15 );
        add_action( 'woocommerce_after_main_content', 'RavensEgg2024\woocommerce_shop_outer_wrapper_end',
            20 );


    } else {

        /**
         * Adjusts WooCommerce pages content wrapper to match theme. Outer and inner wrappers work with re-positioned
         * woocommerce sidebar. Priorities control the placement of elements.
         *
         * @package WooCommerce
         */
        function woocommerce_outer_wrapper_start() {
            echo '<div class="container" id="primary">
   <main>';
        }

        function woocommerce_main_wrapper_start() {
            echo '<article><div class="entry-content">';
        }

        function woocommerce_main_wrapper_end() {
            echo '</div></article>';
        }

        function woocommerce_outer_wrapper_end() {
            echo '</main></div>';
        }

        add_action( 'woocommerce_before_main_content', 'RavensEgg2024\woocommerce_outer_wrapper_start', 5 );
        add_action( 'woocommerce_before_main_content', 'RavensEgg2024\woocommerce_main_wrapper_start', 10 );
        add_action( 'woocommerce_after_main_content', 'RavensEgg2024\woocommerce_main_wrapper_end', 5 );
        add_action( 'woocommerce_after_main_content', 'RavensEgg2024\woocommerce_outer_wrapper_end', 15 );

    }
}

//add_action( 'template_redirect', 'RavensEgg2024\replace_markup' );


/**
 * Removes specific shipping options from a cat of products.
 * 
 * https://stackoverflow.com/questions/50184151/hide-specific-shipping-options-based-on-products-or-categories-in-woocommerce
 *
 * @param $rates
 * @param $package
 * @return mixed
 */
// function specific_products_shipping_methods($rates, $package)
// {
//     // HERE set the product category in the array (ID, slug or name)
//     $terms = array('eclipse-glasses');
//     $taxonomy = 'product_cat';

//     // HERE set the shipping methods to be removed (like "flat_rate:5")
//     $method_instances_ids = array(
//         'flat_rate:30',
//         'flat_rate:31',
//         'ups:3:12',
//         'ups:3:03',
//         'ups:3:02',
//         'ups:3:01',
//         'ups:3:13',
//         'ups:3:14');

//     $found = false;

//     // Loop through cart items checking for defined product IDs
//     foreach ($package['contents'] as $cart_item) {
//         if (has_term($terms, $taxonomy, $cart_item['product_id'])) {
//             $found = true;
//             break;
//         }
//     }

//     if (!$found) return $rates; // If not found we exit

//     // Loop through your active shipping methods
//     foreach ($rates as $rate_id => $rate) {
//         // Remove all other shipping methods other than your defined shipping method
//         if (in_array($rate_id, $method_instances_ids)) {
//             unset($rates[$rate_id]);
//         }
//     }

//     return $rates;
// }

// add_filter('woocommerce_package_rates', 'RavensEgg2024\specific_products_shipping_methods', 10, 2);