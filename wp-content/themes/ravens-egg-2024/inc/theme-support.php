<?php
/**
 * Theme support
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */

namespace RavensEgg2024;

if (!function_exists('ravensegg_setup')) {
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     *
     * @return void
     * @since Ravens_Egg_2024 1.0
     *
     */
    function ravensegg_setup()
    {

        /**
         * Makes theme available for translation.
         * Translations can be filed in the /languages/ directory.
         */
        // load_theme_textdomain( 'ravens-egg-2024', get_template_directory() . '/languages' );

        // Adds default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /**
         * Lets WordPress manage the document title.
         * This theme does not use a hard-coded <title> tag in the document head,
         * WordPress will provide it for us.
         */
        add_theme_support('title-tag');

        /**
         * Enables support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');
        set_post_thumbnail_size(1920, 1240);

        register_nav_menus(
            array(
                'primary' => esc_html__('The Main Menu', 'ravens-egg-2024'), // main nav in header
                'secondary' => __('Secondary menu', 'ravens-egg-2024'),
                'top' => __('Top menu', 'ravens-egg-2024'),
                'footer' => __('Footer menu', 'ravens-egg-2024')
            )
        );

        /**
         * Switches default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support(
            'html5',
            array(
                'comment-form',
                'comment-list',
                'gallery',
                'caption',
                'style',
                'script',
                'navigation-widgets',
            )
        );

        // Adds support for responsive embedded content.
        add_theme_support('responsive-embeds');

        // Adds theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        // Adds support for full and wide align images.
        add_theme_support('align-wide');

        // Adds support for editor styles.
        add_theme_support('editor-styles');

        // Adds support for Block Styles. Displays default block styles from editor also on front end.
//		add_theme_support( 'wp-block-styles' );

        // Custom background color.
//		add_theme_support(
//			'custom-background',
//			array(
//				'default-color' => 'ffffff',
//			)
//		);

        /**
         * Adds support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
//		$logo_width  = 300;
//		$logo_height = 100;
//
//		add_theme_support(
//			'custom-logo',
//			array(
//				'height'               => $logo_height,
//				'width'                => $logo_width,
//				'flex-width'           => true,
//				'flex-height'          => true,
//				'unlink-homepage-logo' => true,
//			)
//		);

        /**
         * Adds core custom header support. Also must add code to display it.
         *
         * @link https://developer.wordpress.org/themes/functionality/custom-headers/
         *
         */

        // $args = array(
        // 	'flex-width'    => true,
        // 	'width'         => 1920,
        // 	'flex-height'   => true,
        // 	'height'        => 600,
        // 	'default-image' => get_template_directory_uri() . get_asset_path( 'images/masthead-default.webp' ),
        // );

        // add_theme_support( 'custom-header', $args );


        /**
         * Adds post-formats support.
         */
//		add_theme_support(
//			'post-formats',
//			array(
//				'link',
//				'aside',
//				'gallery',
//				'image',
//				'quote',
//				'status',
//				'video',
//				'audio',
//				'chat',
//			)
//		);

        // Editor color palette.
//		$teal          = '#00637a';
//		$aqua          = '#77d1e6';
//		$warmGrayLight = '#ebe7e0';
//		$choco         = '#615742';
//		$orange        = '#f26a13';
//
//		add_theme_support(
//			'editor-color-palette',
//			array(
//				array(
//					'name'  => esc_html__( 'Teal', 'ravens-egg-2024' ),
//					'slug'  => 'teal',
//					'color' => $teal,
//				),
//				array(
//					'name'  => esc_html__( 'Aqua', 'ravens-egg-2024' ),
//					'slug'  => 'aqua',
//					'color' => $aqua,
//				),
//				array(
//					'name'  => esc_html__( 'Warm Gray Light', 'ravens-egg-2024' ),
//					'slug'  => 'warm-gray-light',
//					'color' => $warmGrayLight,
//				),
//				array(
//					'name'  => esc_html__( 'Choco', 'ravens-egg-2024' ),
//					'slug'  => 'choco',
//					'color' => $choco,
//				),
//				array(
//					'name'  => esc_html__( 'Orange', 'ravens-egg-2024' ),
//					'slug'  => 'orange',
//					'color' => $orange,
//				),
//			)
//		);

        // Disables custom colors
//		add_theme_support( 'disable-custom-colors' );

        // Disables custom gradients
//		add_theme_support( 'disable-custom-gradients' );

        // To completely disable gradients, leave preset array empty.
//		add_theme_support( 'editor-gradient-presets', array() );

        // Adds gradient preset(s).
//		add_theme_support( 'editor-gradient-presets', array(
//			array(
//				'name'     => esc_attr__( 'Gray Wash', 'ravens-egg-2024' ),
//				'gradient' => 'linear-gradient(to right, rgba(128, 128, 128, 1), rgba(192, 192, 192, 1))',
//				'slug'     => 'gray-wash',
//			)
//		) );

        // Add custom editor font sizes.
//		add_theme_support( 'editor-font-sizes',
//			array(
//				array(
//					'name'      => esc_html__( 'Small', 'ravens-egg-2024' ),
//					'shortName' => esc_html_x( 'S', 'ravens-egg-2024' ),
//					'size'      => 14,
//					'slug'      => 'small'
//				),
//				array(
//					'name'      => esc_html__( 'Normal', 'ravens-egg-2024' ),
//					'shortName' => esc_html_x( 'N', 'ravens-egg-2024' ),
//					'size'      => 16,
//					'slug'      => 'normal'
//				),
//				array(
//					'name'      => esc_html__( 'Large', 'ravens-egg-2024' ),
//					'shortName' => esc_html_x( 'M', 'ravens-egg-2024' ),
//					'size'      => 20,
//					'slug'      => 'medium'
//				),
//				array(
//					'name'      => esc_html__( 'Larger', 'ravens-egg-2024' ),
//					'shortName' => esc_html_x( 'L', 'ravens-egg-2024' ),
//					'size'      => 23,
//					'slug'      => 'large'
//				),
//				array(
//					'name'      => esc_html__( 'Huge', 'ravens-egg-2024' ),
//					'shortName' => esc_html_x( 'XL', 'ravens-egg-2024' ),
//					'size'      => 26,
//					'slug'      => 'huge'
//				)
//			) );

        // Disables custom font sizes
//		add_theme_support( 'disable-custom-font-sizes' );

        // Add support for custom line height controls.
//		add_theme_support( 'custom-line-height' );

        // Add support for experimental link color control.
//		add_theme_support( 'experimental-link-color' );

        // Add support for experimental cover block spacing.
//		add_theme_support( 'custom-spacing' );

        // Adds custom style to button block.
        add_action('init', function () {
            register_block_style('core/buttons', [
                'name' => 'large',
                'label' => __('Large', 'ravens-egg-2024'),
            ]);
        });

    }
}

add_action('after_setup_theme', 'RavensEgg2024\ravensegg_setup');

// Add editor styles with cache busting hash.
function add_editor_styles()
{
    add_editor_style(get_asset_path('editor.css'));
}

add_action('init', 'RavensEgg2024\add_editor_styles');


/**
 * Sets active sidebars and widgetized areas.
 *
 * @return void
 */
function register_sidebars()
{

    $my_sidebars = array(
        array(
            'name' => 'Sidebar 1',
            'id' => 'sidebar1',
            'description' => 'The first (primary) sidebar',
        ),
        array(
            'name' => 'Sidebar 2',
            'id' => 'sidebar2',
            'description' => 'The second (secondary) sidebar',
        ),
        array(
            'name' => 'Subfooter Widget',
            'id' => 'subfooter',
            'description' => 'Subfooter widget, non-sidebar',
        ),
    );

    $defaults = array(
        'name' => 'Default Sidebar',
        'id' => 'sidebar-default',
        'description' => 'This is the default sidebar',
        'class' => '',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'
    );

    foreach ($my_sidebars as $sidebar) {
        $args = wp_parse_args($sidebar, $defaults);
        register_sidebar($args);
    }

}

add_action('widgets_init', 'RavensEgg2024\register_sidebars');


/**
 * Disables core update notification emails unless update failed.
 *
 * @param $send
 * @param $type
 * @param $core_update
 * @param $result
 *
 * @return false
 */
function auto_core_update_send_email($send, $type, $core_update, $result)
{
    if (!empty($type) && $type == 'success') {
        // don't send email
        return false;
    }

    // use default settings
    return $send;
}

add_filter('auto_core_update_send_email', 'RavensEgg2024\auto_core_update_send_email', 10, 4);

// Disables auto-update email notifications for plugins.
add_filter('auto_plugin_update_send_email', '__return_false');

// Disables auto-update email notifications for themes.
add_filter('auto_theme_update_send_email', '__return_false');

/**
 * Sets custom thumbnail sizes. Full size is limited by Imsanity plugin to 1920 x 1080.
 * Colorbox restrains lightboxed images to 90% of screen width and height
 */
add_image_size('card', 600, 600, true);
//add_image_size( 'xlarge', 1024, 768, false );
// add_image_size( 'slideshow', 1200, 675, true );
//add_image_size( 'ravensegg2024-thumb-300', 300, 100, true );

/**
 * Disables full screen editor by default.
 */
function disable_editor_fullscreen_by_default()
{
    $script = "window.onload = function() { const isFullscreenMode = wp.data.select( 'core/edit-post' ).isFeatureActive( 'fullscreenMode' ); if ( isFullscreenMode ) { wp.data.dispatch( 'core/edit-post' ).toggleFeature( 'fullscreenMode' ); } }";
    wp_add_inline_script('wp-blocks', $script);
}

add_action('enqueue_block_editor_assets', 'RavensEgg2024\disable_editor_fullscreen_by_default');

/**
 * Disables font editor introduced with WP 6.5.
 *
 * @param $editor_settings
 * @return mixed
 */
function disable_font_library_ui($editor_settings)
{
    $editor_settings['fontLibraryEnabled'] = false;
    return $editor_settings;
}

add_filter('block_editor_settings_all', 'RavensEgg2024\disable_font_library_ui');

/**
 * Disables core font endpoints.
 *
 * @param $arg
 * @param $post_type
 * @return mixed
 */
//function disable_fonts_rest_api_endpoints( $arg, $post_type ) {
//    if ( 'wp_font_family' === $post_type || 'wp_font_face' === $post_type ) {
//        $arg['show_in_rest'] = false;
//    }
//
//    return $arg;
//}
//
//add_filter( 'register_post_type_args', 'RavensEgg2024\disable_fonts_rest_api_endpoints', 10, 2 );

/**
 * Disables core font collections endpoints.
 *
 * @param $endpoints
 * @return mixed
 */
//function disable_font_collections_rest_api_endpoints($endpoints)
//{
//    foreach ($endpoints as $route => $endpoint) {
//        if (str_starts_with($route, '/wp/v2/font-collections')) {
//            unset($endpoints[$route]);
//        }
//    }
//
//    return $endpoints;
//}
//
//add_filter('rest_endpoints', 'RavensEgg2024\disable_font_collections_rest_api_endpoints');

/**
 * Display unlimited post types per page and sort by random.
 *
 * @param $query
 */
// function alter_posttype_query( $query ) {

// 	/* only proceed on the front end */
// 	if ( is_admin() ) {
// 		return;
// 	}

// 	/* only on the posttype post archive for the main query */
// 	if ( $query->is_post_type_archive( 'posttype' ) && $query->is_main_query() ) {

// 		$query->set( 'posts_per_page', - 1 );
// 		$query->set( 'orderby', 'post_title' );
// 		$query->set( 'order', 'ASC' );
// 		$query->set( 'orderby', 'rand' );
// Fetch only posts with taxonomy 'customtax' set to 'customtaxsetting'.
// $taxquery = array(
// 	array(
// 		'taxonomy' => 'customtax',
// 		'field'    => 'slug',
// 		'terms'    => array( 'customtaxsetting' ),
// 	)
// );
// $query->set( 'tax_query', $taxquery );

// 	}
// }

// add_action( 'pre_get_posts', 'RavensEgg2024\alter_posttype_query' );