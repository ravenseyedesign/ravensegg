<?php
/**
 * Theme Customizer
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */

namespace RavensEgg2024;

/**
 * Custom input sanitization
 *
 * @param $input
 *
 * @return string
 */
function sanitize_text($input)
{
    return wp_kses_post(force_balance_tags($input));
}

/**
 * Custom checkbox sanitization
 *
 * @param $input
 *
 * @return int|string
 */
function sanitize_checkbox($input)
{
    if ($input == 1) {
        return 1;
    } else {
        return '';
    }
}


/**
 * Registers options with the Theme Customizer
 *
 * @param object $wp_customize The WordPress Theme Customizer
 *
 * @package    ravens-egg-2024
 * @since      0.2.0
 * @version    1.0.0
 */
function register_theme_customizer($wp_customize)
{

    // Social links section
    $wp_customize->add_section(
        'social_links_section',
        array(
            'title' => __('Social Links', 'ravens-egg-2024'),
            'description' => __('Please enter the full URL of each social media profile you\'d like to link to from your site', 'ravens-egg-2024'),
            'priority' => 2
        )
    );

    // Facebook URL
    $wp_customize->add_setting(
        'facebook_field',
        array(
            'default' => __('', 'ravens-egg-2024'),
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
            'transport' => 'refresh',
            'sanitize_callback' => 'esc_url_raw',
        )
    );
    $wp_customize->add_control(
        'facebook_field',
        array(
            'label' => __('Facebook', 'ravens-egg-2024'),
            'section' => 'social_links_section',
            'type' => 'text',
            'priority' => 2
        )
    );

    // Instagram URL
    $wp_customize->add_setting(
        'instagram_field',
        array(
            'default' => __('', 'ravens-egg-2024'),
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
            'transport' => 'refresh',
            'sanitize_callback' => 'esc_url_raw',
        )
    );
    $wp_customize->add_control(
        'instagram_field',
        array(
            'label' => __('Instagram', 'ravens-egg-2024'),
            'section' => 'social_links_section',
            'type' => 'text', // Text field control
            'priority' => 8
        )
    );

    // LinkedIn URL
    $wp_customize->add_setting(
        'linkedin_field',
        array(
            'default' => __('', 'ravens-egg-2024'),
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
            'transport' => 'refresh',
            'sanitize_callback' => 'esc_url_raw',
        )
    );
    $wp_customize->add_control(
        'linkedin_field',
        array(
            'label' => __('LinkedIn', 'ravens-egg-2024'),
            'section' => 'social_links_section',
            'type' => 'text', // Text field control
            'priority' => 5
        )
    );

    // Pinterest URL
    $wp_customize->add_setting(
        'pinterest_field',
        array(
            'default' => __('', 'ravens-egg-2024'),
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
            'transport' => 'refresh',
            'sanitize_callback' => 'esc_url_raw',
        )
    );
    $wp_customize->add_control(
        'pinterest_field',
        array(
            'label' => __('Pinterest', 'ravens-egg-2024'),
            'section' => 'social_links_section',
            'type' => 'text', // Text field control
            'priority' => 9
        )
    );

    // Skype URL
    $wp_customize->add_setting(
        'skype_field',
        array(
            'default' => __('', 'ravens-egg-2024'),
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
            'transport' => 'refresh',
            'sanitize_callback' => 'esc_url_raw',
        )
    );
    $wp_customize->add_control(
        'skype_field',
        array(
            'label' => __('Skype', 'ravens-egg-2024'),
            'section' => 'social_links_section',
            'type' => 'text', // Text field control
            'priority' => 11
        )
    );

    // X URL
    $wp_customize->add_setting(
        'x_field',
        array(
            'default' => __('', 'ravens-egg-2024'),
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
            'transport' => 'refresh',
            'sanitize_callback' => 'esc_url_raw',
        )
    );
    $wp_customize->add_control(
        'x_field',
        array(
            'label' => __('X', 'ravens-egg-2024'),
            'section' => 'social_links_section',
            'type' => 'text',
            'priority' => 3
        )
    );

    // Vimeo URL
    $wp_customize->add_setting(
        'vimeo_field',
        array(
            'default' => __('', 'ravens-egg-2024'),
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
            'transport' => 'refresh',
            'sanitize_callback' => 'esc_url_raw',
        )
    );
    $wp_customize->add_control(
        'vimeo_field',
        array(
            'label' => __('Vimeo', 'ravens-egg-2024'),
            'section' => 'social_links_section',
            'type' => 'text', // Text field control
            'priority' => 7
        )
    );

    // YouTube URL
    $wp_customize->add_setting(
        'youtube_field',
        array(
            'default' => __('', 'ravens-egg-2024'),
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
            'transport' => 'refresh',
            'sanitize_callback' => 'esc_url_raw',
        )
    );
    $wp_customize->add_control(
        'youtube_field',
        array(
            'label' => __('YouTube', 'ravens-egg-2024'),
            'section' => 'social_links_section',
            'type' => 'text', // Text field control
            'priority' => 6
        )
    );

    // TikTok URL
    $wp_customize->add_setting(
        'tiktok_field',
        array(
            'default' => __('', 'ravens-egg-2024'),
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
            'transport' => 'refresh',
            'sanitize_callback' => 'esc_url_raw',
        )
    );
    $wp_customize->add_control(
        'tiktok_field',
        array(
            'label' => __('TikTok', 'ravens-egg-2024'),
            'section' => 'social_links_section',
            'type' => 'text', // Text field control
            'priority' => 12
        )
    );

    // Display social links? y/n
    $wp_customize->add_setting(
        'display_social_links',
        array(
            'default' => true,
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
            'transport' => 'refresh',
            'sanitize_callback' => 'RavensEgg2024\sanitize_checkbox',
        )
    );
    $wp_customize->add_control(
        'display_social_links',
        array(
            'label' => __('Display Social Links', 'ravens-egg-2024'),
            'section' => 'social_links_section',
            'type' => 'checkbox', // Checkbox field control
            'priority' => 1
        )
    );


    // Footer section
    $wp_customize->add_section(
        'footer_section',
        array(
            'title' => __('Footer Content', 'ravens-egg-2024'),
            'description' => __('Check the box to display this website\'s email on the left in the footer. Uncheck and fill in the blank to display something else.', 'ravens-egg-2024'),
            'priority' => 3
        )
    );

    // Display footer email? y/n
    $wp_customize->add_setting(
        'display_footer_email',
        array(
            'default' => true,
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
            'transport' => 'refresh',
            'sanitize_callback' => 'RavensEgg2024\sanitize_checkbox',
        )
    );
    $wp_customize->add_control(
        'display_footer_email',
        array(
            'label' => __('Display Footer Email', 'ravens-egg-2024'),
            'section' => 'footer_section',
            'type' => 'checkbox', // Checkbox field control
            'priority' => 1
        )
    );

    $wp_customize->add_setting(
        'footer_left',
        array(
            'default' => __('', 'ravens-egg-2024'),
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
            'transport' => 'refresh',
            'sanitize_callback' => 'RavensEgg2024\sanitize_text',
        )
    );
    $wp_customize->add_control(
        'footer_left',
        array(
            'label' => __('Left Footer Text', 'ravens-egg-2024'),
            'section' => 'footer_section',
            'type' => 'textarea',
            'priority' => 2
        )
    );

    // GTM section
    $wp_customize->add_section(
        'gtm_section',
        array(
            'title' => __('Google Tag Manager', 'ravens-egg-2024'),
            'description' => __('Check the box to insert Google Tag Manager (GTM) tags on the site.', 'ravens-egg-2024'),
            'priority' => 15
        )
    );

    // Display GTM tags? y/n
    $wp_customize->add_setting(
        'display_gtm_tags',
        array(
            'default' => true,
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
            'transport' => 'refresh',
            'sanitize_callback' => 'RavensEgg2024\sanitize_checkbox',
        )
    );
    $wp_customize->add_control(
        'display_gtm_tags',
        array(
            'label' => __('Display GTM Tags', 'ravens-egg-2024'),
            'section' => 'gtm_section',
            'type' => 'checkbox', // Checkbox field control
            'priority' => 1
        )
    );

    // Google Tag Manager head code snippet
    $wp_customize->add_setting(
        'gtm_container_id',
        array(
            'default' => __('', 'ravens-egg-2024'),
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
            'transport' => 'refresh',
            'sanitize_callback' => 'RavensEgg2024\sanitize_text',
        )
    );
    $wp_customize->add_control(
        'gtm_container_id',
        array(
            'label' => __('GTM Container ID (GTM-*******)', 'ravens-egg-2024'),
            'section' => 'gtm_section',
            'type' => 'text',
            'priority' => 2
        )
    );

    // Remove default sections
//	$wp_customize->remove_section( 'nav' );
    $wp_customize->remove_section('static_front_page');
    $wp_customize->remove_panel('themes');
    $wp_customize->remove_control('active_theme');
    $wp_customize->remove_control('header_textcolor');


} // end register_theme_customizer
add_action('customize_register', 'RavensEgg2024\register_theme_customizer');