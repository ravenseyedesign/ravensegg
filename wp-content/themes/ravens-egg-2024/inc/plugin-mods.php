<?php
/**
 * Plugin mods
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */

namespace RavensEgg2024;

// ******************** RCP ************************ //

/**
 * This will remove the username requirement on the registration form
 * and use the email address as the username.
 */
function rcp_user_registration_data( $user ) {
	rcp_errors()->remove( 'username_empty' );
	$user['login'] = $user['email'];

	return $user;
}

// add_filter( 'rcp_user_registration_data', 'RavensEgg2024\rcp_user_registration_data' );

/**
 * Change RCP text output.
 *
 * @param $translated_text
 *
 * @return string
 */
function replace_rcp_text( $translated_text ) {
	if ( $translated_text === 'ORIGINAL TEXT' ) {
		$translated_text = 'NEW TEXT';
	}

	return $translated_text;
}

// add_filter( 'gettext', 'RavensEgg2024\replace_rcp_text', 20 );

// ******************** Download Monitor ************************ //

/**
 * Sets S3 request expiration to 24 hours from 15 minutes.
 */
//add_filter( 'dlm_as3_request_valid_minutes', function ( $minutes, $download, $version, $path ) {
//	return 1440;
//}, 10, 4 );

// ******************** ACF ************************ //

/**
 * Adds ACF Options page.
 *
 * @package ACF
 */
//if ( function_exists( 'acf_add_options_page' ) ) {
//	acf_add_options_page( array(
//		'page_title' => 'Site Options',
//		'menu_title' => 'Site Options',
//		'menu_slug'  => 'site-options',
//		'capability' => 'edit_posts',
//		'redirect'   => false,
//		'icon_url'   => 'dashicons-admin-tools',
//		'position'   => 8
//	) );
//}

/**
 * Adds Google Maps API key to ACF.
 */
// function acf_google_map_init() {

// 	acf_update_setting('google_api_key', 'insert_key_here');
// }

// add_action('acf/init', 'ravensegg20241\acf_google_map_init');

/**
 * Sorts subfields by subfield with key shown below.
 *
 * @param $value
 * @param $post_id
 * @param $field
 *
 * @return mixed
 *
 * @package ACF
 */
//function acf_load_value( $value, $post_id, $field ) {
//	$order = array();
//	if ( empty( $value ) ) {
//		return $value;
//	}
//	foreach ( $value as $i => $row ) {
//		$order[ $i ] = $row['field_538a33275738e'];
//	}
//	array_multisort( $order, SORT_DESC, $value );
//
//	return $value;
//}
//
//add_filter( 'acf/load_value/name=scores', 'RavensEgg2024\acf_load_value', 10, 3 );

/**
 * Customizes TinyMCE in ACF.
 *
 * @param $toolbars
 *
 * @return mixed
 *
 * @package ACF
 */
//function modify_acf_toolbars( $toolbars ) {
//	// Uncomment to view format of $toolbars
//	//	echo '< pre >';
//	//	print_r( $toolbars );
//	//	echo '< /pre >';
//	//	die;
//
//	$toolbars['RED']    = array();
//	$toolbars['RED'][1] = array(
//		'formatselect',
//		'bold',
//		'italic',
//		'bullist',
//		'numlist',
//		'blockquote',
//		'link',
//		'unlink',
//	);
//	$toolbars['RED'][2] = array(
//		'strikethrough',
//		'hr',
//		'pastetext',
//		'removeformat',
//		'charmap',
//		'undo',
//		'redo',
//		'wp_help',
//		'styleselect',
//	);
//
//	return $toolbars;
//}
//
//add_filter( 'acf/fields/wysiwyg/toolbars', 'RavensEgg2024\modify_acf_toolbars' );

// ******************** Tribe The Events Calendar ************************ //

/**
 * Removes unwanted styles from The Events Calendar plugin.
 */
function remove_tec_styles() {
	$styles = array(
		'tribe-events-bootstrap-datepicker-css',
//		'tribe-events-calendar-style',
//		'tribe-events-custom-jquery-styles',
//		'tribe-events-calendar-style',
//		'tribe-events-calendar-pro-style'
	);
	wp_deregister_style( $styles );
}

add_action( 'wp_enqueue_scripts', 'RavensEgg2024\remove_tec_styles', 100 );

/**
 * Hides all past events.
 *
 * @param $wp_query
 *
 * @return mixed
 *
 * @package TheEventsCalendar
 */
//function filter_tribe_all_occurrences( $wp_query ) {
//
//	if ( ! is_admin() ) {
//
//		$new_meta = array();
//		$today    = new DateTime();
//
//		// Join with existing meta_query
//		if ( is_array( $wp_query->meta_query ) ) {
//			$new_meta = $wp_query->meta_query;
//		}
//
//		// Add new meta_query, select events ending from now forward
//		$new_meta[] = array(
//			'key'     => '_EventEndDate',
//			'type'    => 'DATETIME',
//			'compare' => '>=',
//			'value'   => $today->format( 'Y-m-d H:i:s' )
//		);
//
//		$wp_query->set( 'meta_query', $new_meta );
//	}
//
//	return $wp_query;
//}
//
//add_filter( 'tribe_events_pre_get_posts', 'RavensEgg2024\filter_tribe_all_occurrences', 100 );

/**
 * Excludes categories from month view.
 *
 * @param $month_query
 *
 * @return mixed
 *
 * @package TheEventsCalendar
 */
//function exclude_month_events_category( $month_query ) {
//
//	if ( $month_query->query_vars['eventDisplay'] == 'month' && $month_query->query_vars['post_type'] == TribeEvents::POSTTYPE && ! is_tax( TribeEvents::TAXONOMY ) && empty( $month_query->query_vars['suppress_filters'] ) ) {
//		$month_query->set( 'tax_query', array(
//				array(
//					'taxonomy' => TribeEvents::TAXONOMY,
//					'field'    => 'slug',
//					'terms'    => array( 'workshop-intro-international', 'workshop-intro-usa', ),
//					'operator' => 'NOT IN'
//				)
//			)
//		);
//	}
//
//	return $month_query;
//}
//
//add_action( 'pre_get_posts', 'RavensEgg2024\exclude_month_events_category' );

/**
 * Excludes category from list view.
 *
 * @param $list_query
 *
 * @return mixed
 *
 * @package TheEventsCalendar
 */
//function exclude_list_events_category( $list_query ) {
//	if ( $list_query->query_vars['eventDisplay'] == 'upcoming' || $list_query->query_vars['eventDisplay'] == 'past' && $list_query->query_vars['post_type'] == TribeEvents::POSTTYPE && ! is_tax( TribeEvents::TAXONOMY ) && empty( $list_query->query_vars['suppress_filters'] ) ) {
//		$list_query->set( 'tax_query', array(
//				array(
//					'taxonomy' => TribeEvents::TAXONOMY,
//					'field'    => 'slug',
//					'terms'    => array( 'calendar-only' ),
//					'operator' => 'NOT IN'
//				)
//			)
//		);
//	}
//
//	return $list_query;
//}
//
//add_action( 'pre_get_posts', 'RavensEgg2024\exclude_list_events_category' );

/**
 * String replacement for any text in The Events Calendar
 *
 * @param $translations
 * @param $text
 * @param $domain
 *
 * @return mixed
 *
 * @link    http://codex.wordpress.org/Translating_WordPress#Localization_Technology
 *
 * @package TheEventsCalendar
 *
 * Put your custom text here in a key => value pair
 * Example: 'Text you want to change' => 'This is what it will be changed to'
 * The text you want to change is the key, and it is case-sensitive
 * The text you want to change it to is the value
 * You can freely add or remove key => values, but make sure to separate them with a comma
 * This example changes the label "Event" to "Webinar"
 */
//function tribe_custom_theme_text( $translations, $text, $domain ) {
//
//
//	$custom_text = array(
//		'Event'  => 'Webinar',
//		'event'  => 'webinar',
//		'Events' => 'Webinars',
//		'events' => 'webinars',
//	);
//
//	// If this text domain starts with "tribe-" or "the-events-", and we have replacement text
//	if ( ( strpos( $domain, 'tribe-' ) === 0 || strpos( $domain, 'the-events-' ) === 0 ) && array_key_exists( $text, $custom_text ) ) {
//		$text = $custom_text[ $text ];
//	}
//
//	return $text;
//}
//
//add_filter( 'gettext', 'RavensEgg2024\tribe_custom_theme_text', 20, 3 );

// Customizes the medium responsive breakpoint for month view from 768px to 600px
// function customize_tribe_events_v2_view_month_medium_breakpoint( $breakpoints ) {
//     $breakpoints['medium'] = 600;
 
//     return $breakpoints;
// }
 
// add_filter( 'tribe_events_views_v2_view_month_breakpoints', 'RavensEgg2024\customize_tribe_events_v2_view_month_medium_breakpoint' );


// ******************** Gravity Forms ************************ //

/**
 * Change Gravity Forms Entries page size.
 *
 * @return int
 *
 * @package GravityForms
 */
function entry_page_size() {
	return 100;
}

add_filter( 'gform_entry_page_size', 'RavensEgg2024\entry_page_size' );


/**
 * Filters the next, previous and submit buttons.
 * Replaces the forms <input> buttons with <button> while maintaining attributes from original <input>.
 *
 * @param $button
 * @param $form
 * @return false|string|void
 * @throws DOMException
 */
function input_to_button($button, $form)
{
    $dom = new \DOMDocument();

    $dom->loadHTML($button);

    $inputs = $dom->getElementsByTagName('input');

    foreach ($inputs as $input) {
        if (str_contains($input->getAttribute('type'), 'submit')) {

            $new_button = $dom->createElement('button');

            $new_button->appendChild($dom->createTextNode($input->getAttribute('value')));

            $input->removeAttribute('value');
            foreach ($input->attributes as $attribute) {
                $new_button->setAttribute($attribute->name, $attribute->value);
            }

            $input->parentNode->replaceChild($new_button, $input);

            return $dom->saveHtml($new_button);
        }
    }
}

add_filter( 'gform_next_button', 'RavensEgg2024\input_to_button', 10, 2 );
add_filter( 'gform_previous_button', 'RavensEgg2024\input_to_button', 10, 2 );
add_filter( 'gform_submit_button', 'RavensEgg2024\input_to_button', 10, 2 );

// Disables all but essential gforms styles.
add_filter( 'gform_disable_form_theme_css', '__return_true' );

/**
 * Debug Pending Updates
 *
 * Crude debugging method that will spit out all pending plugin
 * and theme updates for admin level users when ?debug_updates is
 * added to a /wp-admin/ URL.
 *
 */
// add_action( 'init', 'RavensEgg2024\debug_pending_updates' );
//function debug_pending_updates() {
//
//	// Rough safety nets
//	if ( ! is_user_logged_in() || ! current_user_can( 'manage_options' ) ) {
//		return;
//	}
//	if ( ! isset( $_GET['debug_updates'] ) ) {
//		return;
//	}
//
//	$output = "";
//
//	// Check plugins
//	$plugin_updates = get_site_transient( 'update_plugins' );
//	if ( $plugin_updates && ! empty( $plugin_updates->response ) ) {
//		foreach ( $plugin_updates->response as $plugin => $details ) {
//			$output .= "<p><strong>Plugin</strong> <u>$plugin</u> is reporting an available update.</p>";
//		}
//	}
//
//	// Check themes
//	wp_update_themes();
//	$theme_updates = get_site_transient( 'update_themes' );
//	if ( $theme_updates && ! empty( $theme_updates->response ) ) {
//		foreach ( $theme_updates->response as $theme => $details ) {
//			$output .= "<p><strong>Theme</strong> <u>$theme</u> is reporting an available update.</p>";
//		}
//	}
//
//	if ( empty( $output ) ) {
//		$output = "No pending updates found in the database.";
//	}
//
//	wp_die( $output );
//}