<?php
/**
 * Scripts and enqueueing
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */

namespace RavensEgg2024;

/**
 * Enqueues all the assets upon theme setup.
 */
function init_load_assets() {

	// enqueue base scripts and styles
	add_action( 'wp_enqueue_scripts', 'RavensEgg2024\load_scripts_and_styles', 999 );

}

add_action( 'after_setup_theme', 'RavensEgg2024\init_load_assets', 15 );

/**
 * Loads asset file with hash from Webpack-generated JSON manifest.
 *
 * @param $asset
 *
 * @return string
 */
function get_asset_path( $asset ) {

	$map = get_template_directory() . '/dist/data/manifest.json';
	static $hash = null;

	if ( null === $hash ) {
		$hash = file_exists( $map ) ? json_decode( file_get_contents( $map ), true ) : [];
	}

	if ( array_key_exists( $asset, $hash ) ) {
		return '/dist/' . $hash[ $asset ];
	}

	echo json_encode( $asset, JSON_PRETTY_PRINT );

	return '/fallback/' . $asset;

}

/**
 * Loads jquery and reply script.
 */
function load_scripts_and_styles() {
	if ( ! is_admin() ) {

		// Registers main stylesheet. Mustard cut media query prevents loading on old browsers: https://github.com/Fall-Back/CSS-Mustard-Cut
		wp_register_style( 'ravensegg2024-stylesheet', get_template_directory_uri() . get_asset_path( 'screen.css' ),
			array(), null, 'screen' );

		// Registers print stylesheet.
		wp_register_style( 'ravensegg2024-print', get_template_directory_uri() . get_asset_path( 'print.css' ),
			array(),
			null, 'print' );

		// Adds comment reply script for threaded comments.
		if ( is_singular() and comments_open() and ( get_option( 'thread_comments' ) == 1 ) ) {
			wp_enqueue_script( 'comment-reply' );
		}

		// Loads fonts asynchronously via Google/Typekit webfontloader.js and adds event classes to html element.
		wp_register_script( 'ravensegg2024-webfontloader', get_template_directory_uri() . get_asset_path( 'fonts.js'
			), '', null, false );

		// Register custom scripts.
		wp_register_script( 'ravensegg2024-scripts', get_template_directory_uri() . get_asset_path( 'index.js' ),
			'', null, true );

		// Register mega-menu script.
		wp_register_script( 'ravensegg2024-megamenu', get_template_directory_uri() . get_asset_path( 'megamenu.js' ),
			'', null, true );

		// Make site url available to JS scripts - wp_localize
		// $ravensegg2024_site_vars = array(
		//     'site_url' => get_site_url(),
		//     'theme_directory' => get_template_directory_uri()
		//     );

		// wp_localize_script( 'ravensegg2024-js', 'ravensegg2024_script_vars', $ravensegg2024_site_vars );

		// load on home page or slideshow page template only
		// if ( is_front_page() || is_page_template('page-templates/slideshow.php') ) {
		//   wp_register_script( 'ravensegg2024-slider', get_template_directory_uri() .
		// get_asset_path( 'slider.js' ), '', '', true );
		// }


		// load on acf google map page only
		// if ( is_page( 'has-acf-google-map-field' ) ) {
		// 	wp_register_script( 'ravensegg2024-googlemaps-api', 'https://maps.googleapis
		//.com/maps/api/js?key=insert_key_here' );
		// 	wp_register_script( 'ravensegg2024-googlemaps-init', get_template_directory_uri() .
		// get_asset_path( 'googlemaps-init-min.js' ), array(
		// 		'jquery',
		// 		'ravensegg2024-googlemaps-api',
		// 	), '', true );
		// }

		// enqueue styles and scripts
		wp_enqueue_style( 'ravensegg2024-stylesheet' );
		wp_enqueue_style( 'ravensegg2024-print' );

		wp_enqueue_script( 'ravensegg2024-webfontloader' );
		wp_enqueue_script( 'ravensegg2024-scripts' );

		wp_enqueue_script( 'ravensegg2024-megamenu' );

		// load on home page or slideshow page template only
		// if ( is_front_page() || is_page_template('page-templates/slideshow.php') ) {
		//   wp_enqueue_script( 'ravensegg2024-slider' );
		// }

		// load on acf google map page only
//		if ( is_page( 'has-acf-google-map-field' ) ) {
//			wp_enqueue_script( 'ravensegg2024-googlemaps-api' );
//			wp_enqueue_script( 'ravensegg2024-googlemaps-init' );
//		}

	}
}

/**
 * Loads Google web fonts synchronously (blocking, not recommended).
 */
//function load_google_webfonts() {
//	$protocol   = is_ssl() ? 'https' : 'http';
//	$query_args = array(
//		'family' => 'Source+Sans+Pro:400,600,400italic,600italic%7cMerriweather:400,400italic',
//		'subset' => 'latin',
//	);
//
//	wp_enqueue_style( 'google-webfonts',
//		add_query_arg( $query_args, "$protocol://fonts.googleapis.com/css" ),
//		array(), null );
//}

//add_action( 'wp_enqueue_scripts', 'RavensEgg2024\load_google_webfonts' );