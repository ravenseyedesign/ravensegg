<?php
/**
 * Cleanup functions
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */

namespace RavensEgg2024;

/**
 * Removes the […] from teaser, adds More link.
 *
 * @param $more
 *
 * @return string
 */
function revise_excerpt_more($more)
{
    global $post;

    $link = get_permalink($post->ID);
    $text = __('Read more', 'ravens-egg-2024');

    // edit here as needed
    return sprintf('...  <p class="more-link"><a class="button ghost arrow" href="%s" title="Read 
	more">%s</a></p>', $link, $text);
}

add_filter('excerpt_more', 'RavensEgg2024\revise_excerpt_more');

/**
 * Sets custom length of excerpt.
 *
 * @param $length
 *
 * @return int
 */
function set_custom_excerpt_length($length)
{
    return 30;
}

add_filter('excerpt_length', 'RavensEgg2024\set_custom_excerpt_length', 999);

/**
 * Adds link to top.
 */
function add_link_to_top()
{
    $scrollMessage = __('Scroll to top', 'ravens-egg-2024');
    printf('<a id="scroll-to-top" href="#" title="%s"><span>%s</span></a>', $scrollMessage, $scrollMessage);
}

add_action('wp_footer', 'RavensEgg2024\add_link_to_top');

/**
 * Removes "Protected:" etc. from password-protected & private page titles.
 *
 * @param $title
 *
 * @return mixed|string|void
 */
function trim_pw_protected_title($title)
{
    $title = esc_attr($title);
    $find_these = array(
        '#Protected:#',
        '#Private:#'
    );
    $replace_with = array(
        '<span class="protected"></span>', // What to replace "Protected:" with
        '<span class="private"></span>' // What to replace "Private:" with
    );
    $title = preg_replace($find_these, $replace_with, $title);

    return $title;

}

add_filter('the_title', 'RavensEgg2024\trim_pw_protected_title');

/**
 * Gets theme author URI from styles.css and prints it.
 *
 * @return void
 */
function print_theme_author_link()
{
    $theme_data = wp_get_theme();
    echo $theme_data->get('AuthorURI');
}

/**
 * Get post thumbnail if one exists and display with link to full size.
 *
 * @param string $post_id
 */
function insert_lightbox_featured_image($post_id = '')
{
    if (!$post_id) {
        $post_id = get_the_ID();
    }

    $thumbnail_id = get_post_thumbnail_id($post_id);
    $thumbnail_obj = get_post($thumbnail_id);
    $thumbnail_caption = $thumbnail_obj->post_excerpt;
    $thumbnail_title = null;

    if ($thumbnail_caption !== '') {
        $thumbnail_title = 'title="' . $thumbnail_caption . '"';
    }

    if (has_post_thumbnail()) {
        $large_image_url = wp_get_attachment_image_src($thumbnail_id, 'full');
        printf('<figure class="featured-image aligncenter"><a class="lightbox" href="%s" %s>', $large_image_url[0],
            $thumbnail_title);

        the_post_thumbnail('large');

        echo '</a>';
        if ($thumbnail_caption !== '') {
            printf('<figcaption class="caption">%s</figcaption>', $thumbnail_caption);
        }
        echo '</figure>';

    }
}

/**
 * Insert thumbnail featured image with optional caption that links to single post.
 *
 * @param string $post_id
 */
function insert_featured_image_thumbnail_link_to_single($post_id = '')
{

    if (!$post_id) {
        $post_id = get_the_ID();
    }

    $thumbnail_id = get_post_thumbnail_id($post_id);
    $thumbnail_obj = get_post($thumbnail_id);
    $thumbnail_caption = $thumbnail_obj->post_excerpt;
    $thumbnail_title = null;

    if ($thumbnail_caption !== '') {
        $thumbnail_title = 'title="' . $thumbnail_caption . '"';
    }

    if (has_post_thumbnail($post_id)) {

        printf('<figure class="featured-image-item"><a class="linked-image" href="%s" %s >', get_the_permalink(),
            $thumbnail_title);

        the_post_thumbnail('medium');

        if ($thumbnail_caption != '') {
            printf('<figcaption class="caption">%s</figcaption>', $thumbnail_caption);
        }

        echo '</a>';
        echo '</figure>';

    }
}

/**
 * Insert thumbnail featured image card size with link to single post.
 *
 * @param $post_id
 *
 * @return void
 */
function insert_featured_image_card_thumbnail($post_id = '')
{

    if (!$post_id) {
        $post_id = get_the_ID();
    }

    $thumbnail_id = get_post_thumbnail_id($post_id);
    $thumbnail_obj = get_post($thumbnail_id);
    $thumbnail_title = 'title="' . $thumbnail_obj->post_title . '"';

    if (has_post_thumbnail($post_id)) {

        printf('<figure class="featured-image-item"><a class="linked-image" href="%s" %s >', get_the_permalink(),
            $thumbnail_title);

        the_post_thumbnail('card');

        echo '</a>';
        echo '</figure>';

    }
}

/**
 * Insert featured image without link to larger version.
 *
 * @param string $post_id
 */
function insert_featured_image($post_id = '')
{
    if (!$post_id) {
        $post_id = get_the_ID();
    }

    $thumbnail_id = get_post_thumbnail_id($post_id);
    $thumbnail_obj = get_post($thumbnail_id);
    $thumbnail_caption = $thumbnail_obj->post_excerpt;

    if (has_post_thumbnail()) {
        echo '<figure class="featured-image">';

        the_post_thumbnail('large');

        if ($thumbnail_caption != '') {
            printf('<figcaption class="caption">%s</figcaption>', $thumbnail_caption);
        }
        echo '</figure>';

    }
}

/**
 * If no featured image set, insert featured image from home page without link to larger image.
 * If no featured image on home page, insert default from theme fallback directory (optional).
 *
 * @param string $post_id
 */
function insert_home_featured_image($post_id = '')
{

    if (!$post_id) {
        $post_id = get_the_ID();
    }

    $home_id = get_option('page_on_front');

    if (has_post_thumbnail($post_id)) {

        $thumbnail_id = get_post_thumbnail_id($post_id);
        $thumbnail_obj = get_post($thumbnail_id);
        $thumbnail_caption = $thumbnail_obj->post_excerpt;

        echo '<figure class="featured-image">';

        the_post_thumbnail('large');

        if ($thumbnail_caption != '') {
            printf('<figcaption class="caption">%s</figcaption>', $thumbnail_caption);
        }
        echo '</figure>';

    } elseif (has_post_thumbnail($home_id)) {

        $home_thumbnail_id = get_post_thumbnail_id($home_id);
        $home_thumbnail_obj = get_post($home_thumbnail_id);
        $home_thumbnail_img = wp_get_attachment_image($home_thumbnail_id, 'large');
        $home_thumbnail_caption = $home_thumbnail_obj->post_excerpt;


        echo '<figure class="featured-image">';

        echo $home_thumbnail_img;

        if ($home_thumbnail_caption != '') {
            printf('<figcaption class="caption">%s</figcaption>', $home_thumbnail_caption);
        }
        echo '</figure>';

    } else {

        $default_image_path = get_stylesheet_directory() . '/fallback/masthead-default.jpg';
        $default_image_url = get_stylesheet_directory_uri() . '/fallback/masthead-default.jpg';

        if (file_exists($default_image_path)) {
            printf('<figure class="featured-image"><img width="1920" height="1280" src="%s" alt="Featured Image"></figure>', $default_image_url);
        }

    }
}

// @TODO Be sure to update the size to match the masthead default for the current project!

/**
 * If no featured image set, insert featured image from home page as background.
 * If no featured image on home page, insert default image from theme fallback directory.
 *
 * @param string $post_id
 */
function insert_home_featured_image_background($post_id = '')
{

    if (!$post_id) {
        $post_id = get_the_ID();
    }

    $home_id = get_option('page_on_front');

    if (is_page() && has_post_thumbnail($post_id)) {

        $thumbnail_img_url = get_the_post_thumbnail_url($post_id, 'masthead');

        printf('<div class="masthead-image" style="background-image: url(\'%s\')">', $thumbnail_img_url);

    } elseif (has_post_thumbnail($home_id)) {

        $home_thumbnail_img_url = get_the_post_thumbnail_url($home_id, 'masthead');
        printf('<div class="masthead-image" style="background-image: url(\'%s\')">', $home_thumbnail_img_url);

    } else {

        $default_image_path = get_stylesheet_directory() . '/fallback/masthead-default.jpg';
        $default_image_url = get_stylesheet_directory_uri() . '/fallback/masthead-default.jpg';

        if (file_exists($default_image_path)) {
            printf('<div class="masthead-image" style="background-image: url(\'%s\')">', $default_image_url);
        }

    }
}

/**
 * If no featured image set, insert thumbnail featured image from home page that links to single post.
 *
 * @param string $post_id
 */
function insert_home_featured_image_thumbnail($post_id = '')
{

    if (!$post_id) {
        $post_id = get_the_ID();
    }

    $home_id = get_option('page_on_front');

    if (has_post_thumbnail($post_id)) {

        echo '<figure class="featured-image-item"><a class="linked-image" href="' . get_the_permalink() . '">';
        the_post_thumbnail('medium');
        echo '</a></figure>';

    } elseif (has_post_thumbnail($home_id)) {

        $home_thumbnail_id = get_post_thumbnail_id($home_id);
        $home_thumbnail_img = wp_get_attachment_image($home_thumbnail_id, array('medium'));

        printf('<figure class="featured-image-item"><a class="linked-image" href="%s">%s</a></figure>',
            get_the_permalink(), $home_thumbnail_img);

    }
}

/**
 * Changes admin login/logout text.
 *
 * @param $text
 *
 * @return mixed
 */
function log_in_out_text_change($text)
{
    $login_text_before = 'Log in';
    $login_text_after = 'Log in admin';

    $logout_text_before = 'Log out';
    $logout_text_after = 'Log out admin';

    $text = str_replace($login_text_before, $login_text_after, $text);
    $text = str_replace($logout_text_before, $logout_text_after, $text);

    return $text;
}

add_filter('loginout', 'RavensEgg2024\log_in_out_text_change');

/**
 * Adds login/out link to footer menu.
 *
 * @param $items
 * @param $args
 *
 * @return string
 */
function log_in_out_menu_link($items, $args)
{
    if ($args->theme_location == 'footer') {
        if (is_user_logged_in()) {
            $items .= '<li class="menu-item"><a rel="nofollow" href="' . wp_logout_url() . '">' . __('Log out admin', 'ravens-egg-2024'
                ) . '</a></li>';
        } else {
            $items .= '<li class="menu-item"><a rel="nofollow" href="' . wp_login_url(get_permalink()) . '">' . __('Log in admin', 'ravens-egg-2024') . '</a></li>';
        }
    }

    return $items;
}

add_filter('wp_nav_menu_items', 'RavensEgg2024\log_in_out_menu_link', 10, 2);

/**
 * Tests if we're on a blog page.
 *
 * @return bool
 */
function is_blog()
{
    return (is_archive() || is_author() || is_category() || is_single() || is_tag()) && 'post' == get_post_type();
}

/**
 * Adds various custom classes to body class.
 *
 * @param $classes
 *
 * @return mixed
 */
function add_custom_body_classes($classes)
{

    // Adds multisite ID body class
    // $id        = get_current_blog_id();
    // $classes[] = 'site-id-' . $id;

    // Adds blog body class
    if (is_blog()) {

        $classes[] = 'blog-section';
    }

    // Adds featured image body class
    if (is_singular() && has_post_thumbnail()) {
        //if ( is_singular() && has_post_thumbnail() && ! is_woocommerce() ) { // If WC installed
        $classes[] = 'has-featured-image';
    }

    return $classes;
}

add_filter('body_class', 'RavensEgg2024\add_custom_body_classes');

/**
 * Disable the confirmation notices when an administrator
 * changes their email address. Comment out for production.
 *
 * @see http://codex.wordpress.com/Function_Reference/update_option_new_admin_email
 */
function update_option_new_admin_email($old_value, $value)
{

    update_option('admin_email', $value);
}

remove_action('add_option_new_admin_email', 'update_option_new_admin_email');
remove_action('update_option_new_admin_email', 'update_option_new_admin_email');
add_action('add_option_new_admin_email', 'RavensEgg2024\update_option_new_admin_email', 10, 2);
add_action('update_option_new_admin_email', 'RavensEgg2024\update_option_new_admin_email', 10, 2);

// add_action( 'template_redirect', 'RavensEgg2024\redirect_single_testimonial' );

/**
 * Redirects single custom post type to archive for all except admins.
 */
//function redirect_single_testimonial() {
//	$queried_post_type = get_query_var( 'post_type' );
//	if ( ! current_user_can( 'administrator' ) && is_single() && 'testimonial' == $queried_post_type ) {
//		wp_redirect( home_url() . '/testimonials', 301 );
//		exit;
//	}
//}


/**
 * Parses blocks for render block functions.
 *
 * @param $content
 * @param $blockName
 * @param $callback
 */
function atBlockType($content, $blockName, $callback)
{
    $filter = function ($html, $block) use ($blockName, &$callback, &$filter) {
        if ($block['blockName'] === $blockName) {
            remove_filter('render_block', $filter, 10);
            $callback($html, $block);
            add_filter('render_block', $filter, 10, 2);
        }
    };

    add_filter('render_block', $filter, 10, 2);
    do_blocks($content);
    remove_filter('render_block', $filter, 10);
}

/**
 * Renders blocks of a type into an array.
 *
 * @param $content
 * @param $blockName
 *
 * @return array
 */
function renderBlocksOfType($content, $blockName)
{
    $blocks = [];

    atBlockType($content, $blockName, function ($html, $block) use (&$blocks) {
        $blocks[] = render_block($block);
    });

    return $blocks;
}

/**
 * Renders blocks of a type into a string.
 *
 * @param $content
 * @param $blockName
 *
 * @return string
 */
function renderBlocksOfTypeIntoString($content, $blockName)
{
    $output = '';

    atBlockType($content, $blockName, function ($html, $block) use (&$output) {
        $output .= render_block($block);
    });

    return $output;
}

/**
 * Renders inner blocks of a parent block type.
 *
 * @param $content
 * @param $parentBlockName
 *
 * @return array
 */
function getRenderedInnerBlocksOfType($content, $parentBlockName)
{
    $blocks = [];

    atBlockType($content, $parentBlockName, function ($html, $block) use (&$blocks) {
        foreach ($block['innerBlocks'] as $innerBlock) {
            $blocks[] = render_block($innerBlock);
        }
    });

    return $blocks;
}

/**
 * Sets comments to off for default post type.
 *
 * @param $data
 *
 * @return mixed
 */
function default_comments_off($data)
{
    if ($data['post_type'] === 'post' && $data['post_status'] === 'auto-draft') {
        $data['comment_status'] = 0;
    }

    return $data;
}

add_filter('wp_insert_post_data', 'RavensEgg2024\default_comments_off');

/**
 * Removes website field from comment form.
 *
 * @param $fields
 *
 * @return mixed
 */
function remove_website_field_comments($fields)
{
    if (isset($fields['url'])) {
        unset($fields['url']);
    }

    return $fields;
}

add_filter('comment_form_default_fields', 'RavensEgg2024\remove_website_field_comments');

/**
 * Hides the archive title but leaves it for screen readers for context.
 *
 * @param $title
 *
 * @return mixed|string
 */
function hide_the_archive_title($title)
{

    // Splits the title into parts so we can wrap them with spans.
    $title_parts = explode(': ', $title, 2);

    // Glue it back together again.
    if (!empty($title_parts[1])) {
        $title = wp_kses(
            $title_parts[1],
            array(
                'span' => array(
                    'class' => array(),
                ),
            )
        );
        $title = '<span class="screen-reader-text">' . esc_html($title_parts[0]) . ': </span>' . $title;
    }

    return $title;

}

add_filter('get_the_archive_title', 'RavensEgg2024\hide_the_archive_title');


/**
 * Inserts GTM head code with container ID from Customizer.
 *
 * @return null
 */
function gtm_head_code()
{

    if (get_theme_mod('display_gtm_tags') == 1) {

        if (!empty(get_theme_mod('gtm_container_id'))) {
            $containerID = get_theme_mod('gtm_container_id');
            printf('<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({\'gtm.start\':
new Date().getTime(),event:\'gtm.js\'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!=\'dataLayer\'?\'&l=\'+l:\'\';j.async=true;j.src=
\'https://www.googletagmanager.com/gtm.js?id=\'+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,\'script\',\'dataLayer\',\'%s\');</script>
<!-- End Google Tag Manager -->', $containerID);
        }
    }

    return null;
}

add_action('wp_head', 'RavensEgg2024\gtm_head_code');

/**
 * Inserts GTM body code with container ID from Customizer.
 *
 * @return null
 */
function gtm_body_code()
{

    if (get_theme_mod('display_gtm_tags') == 1) {

        if (!empty(get_theme_mod('gtm_container_id'))) {
            $containerID = get_theme_mod('gtm_container_id');
            printf('<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=%s"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->', $containerID);
        }
    }

    return null;
}

add_action('wp_body_open', 'RavensEgg2024\gtm_body_code');

/**
 * Wrap the label for File Block download button text in a span for styling.
 *
 * @param $block_content
 * @param $block
 *
 * @return false|mixed|string
 * @throws \DOMException
 */
function modify_file_block_download($block_content, $block)
{
    if ($block['blockName'] !== 'core/file') {
        return $block_content;
    }

    if (!$block_content) {
        return $block_content;
    }

    $dom = new \DOMDocument;

    $dom->loadHTML(mb_convert_encoding($block_content, 'HTML-ENTITIES', 'UTF-8'));

    $links = $dom->getElementsByTagName('a');

    foreach ($links as $link) {

        if (str_contains($link->getAttribute('class'), 'wp-block-file__button')) {

            $span = $dom->createElement('span');
            $children = [];

            foreach ($link->childNodes as $child) {
                $children[] = $child;
            }

            foreach ($children as $child) {
                $link->removeChild($child);
                $span->appendChild($child);
            }

            $link->appendChild($span);

        }
    }

    return $dom->saveHTML();

}

add_filter('render_block', 'RavensEgg2024\modify_file_block_download', 10, 2);


/**
 * Adds class to image links for lightbox.
 */
add_filter('render_block_core/image', function ($html) {
    $processor = new \WP_HTML_Tag_Processor($html);

    if ($processor->next_tag('a')) {
        $href = $processor->get_attribute('href');
        if (!preg_match('~\.(jpe?g|png|tiff?|avif|heic|jxl|gif|webp)$~', $href)) {
            return $html;
        }

        $processor->add_class('lightbox');
    }

    return $processor->get_updated_html();
}, 10, 1);


/**
 * Adds class to specific menu a element.
 *
 * @param $atts
 * @param $item
 * @param $args
 * @return mixed
 */
// function add_specific_menu_location_atts($atts, $item, $args)
// {
//     // check if the item is in the primary menu
//     if ($args->menu == 'top-menu') {
//         // add the desired attributes:
//         $atts['class'] = 'menu-link-class';
//     }
//     return $atts;
// }

// add_filter('nav_menu_link_attributes', 'RavensEgg2024\add_specific_menu_location_atts', 10, 3);


/**
 * Adds pretty search URLs.
 */
//function change_search_url_rewrite() {
//	if ( is_search() && ! empty( $_GET['s'] ) ) {
//		wp_redirect( home_url( "/search/" ) . urlencode( get_query_var( 's' ) ) );
//		exit();
//	}
//}
//
//add_action( 'template_redirect', 'RavensEgg2024\change_search_url_rewrite' );

/**
 * Conditional checks if post type outside or inside of loop.
 *
 * @param $type
 *
 * @return bool
 */
//function is_post_type( $type ) {
//	global $wp_query;
//	if ( $type == get_post_type( $wp_query->post->ID ) ) {
//		return true;
//	}
//
//	return false;
//}

/**
 * Call a shortcode function by tag name.
 *
 * @param string $tag The shortcode whose function to call.
 * @param array $atts The attributes to pass to the shortcode function. Optional.
 * @param array $content The shortcode's content. Default is null (none).
 *
 * @return string|bool False on failure, the result of the shortcode on success.
 * @author J.D. Grimes
 * @link   https://codesymphony.co/dont-do_shortcode/
 *
 */
//function do_shortcode_func( $tag, array $atts = array(), $content = null ) {
//
//	global $shortcode_tags;
//
//	if ( ! isset( $shortcode_tags[ $tag ] ) ) {
//		return false;
//	}
//
//	return call_user_func( $shortcode_tags[ $tag ], $atts, $content, $tag );
//}


/**
 * Gets parent category.
 *
 * @link http://www.sandboxdev.com/2009/01/15/get-top-parent-category-wordpress/#sthash.5i33Yoj4.dpuf
 *
 */
//if ( ! function_exists( 'post_is_in_parent_category' ) ) {
//	function post_is_in_parent_category( $cats, $_post = null ) {
//		foreach ( (array) $cats as $cat ) {
//			$descendants = get_term_children( (int) $cat, "category" );
//			if ( $descendants && in_category( $descendants, $_post ) ) {
//				return true;
//			}
//		}
//
//		return false;
//	}
//}

/**
 * Redirects single custom post type to archive.
 */
// function redirect_single_posttype() {
// 	$queried_post_type = get_query_var('post_type');
// 	if ( is_single() && 'posttype' ==  $queried_post_type ) {
// 		wp_redirect( home_url() . '/posttype', 301 );
// 		exit;
// 	}
// }

//add_action( 'template_redirect', 'RavensEyeDesign2020\redirect_single_posttype' );

/**
 * Tests if any of a post's assigned categories are descendants of target categories.
 *
 * @link http://codex.wordpress.org/Function_Reference/in_category#Testing_if_a_post_is_in_a_descendant_category
 */
//if ( ! function_exists( 'post_is_in_descendant_category' ) ) {
//	function post_is_in_descendant_category( $cats, $_post = null ) {
//		foreach ( (array) $cats as $cat ) {
//			// get_term_children() accepts integer ID only
//			$descendants = get_term_children( (int) $cat, 'category' );
//			if ( $descendants && in_category( $descendants, $_post ) ) {
//				return true;
//			}
//		}
//
//		return false;
//	}
//}

/**
 * Checks if post has embedded content.
 *
 * @param bool $post_id
 *
 * @return bool
 */
//function has_embed( $post_id = false ) {
//	if ( ! $post_id ) {
//		$post_id = get_the_ID();
//	} else {
//		$post_id = absint( $post_id );
//	}
//	if ( ! $post_id ) {
//		return false;
//	}
//
//	$post_meta = get_post_custom_keys( $post_id );
//
//	foreach ( $post_meta as $meta ) {
//		if ( '_oembed' != substr( trim( $meta ), 0, 7 ) ) {
//			continue;
//		}
//
//		return true;
//	}
//
//	return false;
//}

/**
 * Adds even/odd post classes.
 *
 * @param $classes
 *
 * @return array
 */
//function add_odd_even_post_class( $classes ) {
//	global $current_class;
//	$classes[]     = $current_class;
//	$current_class = ( $current_class == 'odd' ) ? 'even' : 'odd';
//
//	return $classes;
//}
//
//global $current_class;
//$current_class = 'odd';

//add_filter( 'post_class', 'RavensEgg2024\add_odd_even_post_class' );

/**
 * Adds a class with the post status (ie. draft, published, private) to each nav menu li so nav menu items can be hooked to via CSS based on post status.
 * Allows for hiding of any Draft or Private menu items automatically.
 * CSS will need to be added to make this work: (ie. li.draft).
 *
 * @param $classes
 * @param $item
 *
 * @return array
 */
//function nav_menu_add_post_status_class( $classes, $item ) {
//	$post_status = get_post_status( $item->object_id );
//	$classes[]   = $post_status;
//
//	return $classes;
//}

//add_filter( 'nav_menu_css_class', 'RavensEgg2024\nav_menu_add_post_status_class', 10, 2 );

/**
 * Force sub-categories to use the parent category template.
 *
 * @link http://werdswords.com/force-sub-categories-use-the-parent-category-template/
 *
 * @return string
 */
//function force_new_subcategory_hierarchy() {
//	$category  = get_queried_object();
//	$parent_id = $category->category_parent;
//	$templates = array();
//	if ( $parent_id == 0 ) {
//		// Use default values from get_category_template()
//		$templates[] = "category-{$category->slug}.php";
//		$templates[] = "category-{$category->term_id}.php";
//		$templates[] = 'category.php';
//	} else {
//		// Create replacement $templates array
//		$parent = get_category( $parent_id );
//		// Current first
//		$templates[] = "category-{$category->slug}.php";
//		$templates[] = "category-{$category->term_id}.php";
//		// Parent second
//		$templates[] = "category-{$parent->slug}.php";
//		$templates[] = "category-{$parent->term_id}.php";
//		$templates[] = 'category.php';
//	}
//
//	return locate_template( $templates );
//}

//add_filter( 'category_template', 'RavensEgg2024\force_new_subcategory_hierarchy' );

/**
 * Check if post category is a child category of a parent category.
 *
 * @return bool
 */
//function post_is_subcategory() {
//	$cat      = get_query_var( 'cat' );
//	$category = get_category( $cat );
//	$category->parent;
//
//	return ( $category->parent == '0' ) ? false : true;
//}

/**
 * More tags jump to top of single page instead of to specific ID.
 *
 * @param $link
 *
 * @return mixed
 */
//function remove_more_jump_link( $link ) {
//	$offset = strpos( $link, '#more-' );
//	if ( $offset ) {
//		$end = strpos( $link, '"', $offset );
//	}
//	if ( $end ) {
//		$link = substr_replace( $link, '', $offset, $end - $offset );
//	}
//
//	return $link;
//}

//add_filter( 'the_content_more_link', 'RavensEgg2024\remove_more_jump_link' );

/**
 * Force permalink slug to update when title updates. Generally a bad idea.
 *
 * @param $data
 * @param $postarr
 *
 * @return mixed
 */
//function force_slug_update_to_title( $data, $postarr ) {
//	if ( ! in_array( $data['post_status'], array( 'draft', 'pending', 'auto-draft' ) ) ) {
//		$data['post_name'] = wp_unique_post_slug( sanitize_title( $data['post_title'] ), $postarr['ID'], $data['post_status'], $data['post_type'], $data['post_parent'] );
//	}
//
//	return $data;
//}

//add_filter( 'wp_insert_post_data', 'RavensEgg2024\force_slug_update_to_title', 99, 2 );

/**
 * Remove unwanted capabilities. Run once, then comment out.
 */
//function clean_unwanted_caps() {
//	$delete_caps = array(
//		'NextGEN Manage gallery',
//		'NextGEN Manage others gallery',
//		'NextGEN Upload images',
//		'NextGEN Use TinyMCE',
//	);
//	global $wp_roles;
//	foreach ( $delete_caps as $cap ) {
//		foreach ( array_keys( $wp_roles->roles ) as $role ) {
//			$wp_roles->remove_cap( $role, $cap );
//		}
//	}
//}

//add_action( 'admin_init', 'RavensEgg2024\clean_unwanted_caps' );

/**
 * Get Youtube video ID from URL
 *
 * @param string $url
 *
 * @return mixed Youtube video ID or FALSE if not found
 */
//function getYoutubeIdFromUrl( $url ) {
//	$parts = parse_url( $url );
//	if ( isset( $parts['query'] ) ) {
//		parse_str( $parts['query'], $qs );
//		if ( isset( $qs['v'] ) ) {
//			return $qs['v'];
//		} else if ( isset( $qs['vi'] ) ) {
//			return $qs['vi'];
//		}
//	}
//	if ( isset( $parts['path'] ) ) {
//		$path = explode( '/', trim( $parts['path'], '/' ) );
//
//		return $path[ count( $path ) - 1 ];
//	}
//
//	return false;
//}