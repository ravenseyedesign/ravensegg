<?php
/**
 * Admin customizations
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */

namespace RavensEgg2024;

/**
 * Disables the auto generated email sent to the admin after a successful core update.
 *
 * @param $send
 * @param $type
 * @param $core_update
 * @param $result
 *
 * @return bool
 */
function bypass_auto_update_email( $send, $type, $core_update, $result ) {
	// The conditional below does not send an email for any type. Delete the "|| $type == 'type'" for the type you wish to receieve
	// if ( ! empty( $type ) && ( $type == 'success' || $type == 'manual' || $type == 'critical' || $type == 'fail' ) ) {
	if ( ! empty( $type ) && $type == 'success' ) {
		return false;
	}

	// return the send function
	return true;
}

add_filter( 'auto_core_update_send_email', 'RavensEgg2024\bypass_auto_update_email', 10, 4 );


/**
 * Modifies the admin bar left label.
 */
function custom_adminbar_titles() {
	if ( is_admin() ) {
		$title = "Home Page";
	} else {
		$title = "Admin Area";
	}
	global $wp_admin_bar;
	$wp_admin_bar->add_menu( array(
			'id'    => 'site-name',
			'title' => $title,
		)
	);
}

add_action( 'wp_before_admin_bar_render', 'RavensEgg2024\custom_adminbar_titles' );

/**
 * Disables default dashboard widgets.
 */
function disable_dashboard_widgets() {
	remove_meta_box( 'dashboard_right_now', 'dashboard', 'core' );        // Right Now Widget
	remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'core' );    // Incoming Links Widget
	remove_meta_box( 'dashboard_plugins', 'dashboard', 'core' );            // Plugins Widget
	remove_meta_box( 'dashboard_quick_press', 'dashboard', 'core' );        // Quick Press Widget
	remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'core' );    // Recent Drafts Widget
	remove_meta_box( 'dashboard_activity', 'dashboard', 'core' );            // Activity Widget
	remove_meta_box( 'dashboard_primary', 'dashboard', 'core' );            // WordPress News Widget

	// removes plugin dashboard boxes
	remove_meta_box( 'rg_forms_dashboard', 'dashboard', 'normal' );        // Gravity Forms Plugin Widget
}

add_action( 'admin_menu', 'RavensEgg2024\disable_dashboard_widgets' );

/**
 * Removes top admin menu items.
 */
function customize_admin_bar() {
	global $wp_admin_bar;
//	$wp_admin_bar->remove_menu( 'search' );
	$wp_admin_bar->remove_menu( 'wp-logo' );
	$wp_admin_bar->remove_menu( 'about' );
	$wp_admin_bar->remove_menu( 'wporg' );
//	$wp_admin_bar->remove_menu( 'documentation' );
	$wp_admin_bar->remove_menu( 'support-forums' );
	$wp_admin_bar->remove_menu( 'feedback' );
//	$wp_admin_bar->remove_menu( 'view-site' );
//	$wp_admin_bar->remove_menu( 'new-content' );
//	$wp_admin_bar->remove_menu( 'new-media' );
//	$wp_admin_bar->remove_menu( 'new-user' );
	$wp_admin_bar->remove_menu( 'themes' );
//	$wp_admin_bar->remove_menu( 'customize' );
//	$wp_admin_bar->remove_menu( 'widgets' );
//	$wp_admin_bar->remove_menu('comments');
//	if ( ! is_admin() ) {
//		$wp_admin_bar->remove_menu( 'my-account' ); // removes my account bar from top right
//	}
}

add_action( 'wp_before_admin_bar_render', 'RavensEgg2024\customize_admin_bar' );

/**
 * Replaces howdy in the admin bar.
 *
 * @param $translated
 * @param $text
 * @param $domain
 *
 * @return mixed
 */
function replace_howdy( $translated, $text, $domain ) {
	if ( false !== strpos( $translated, "Howdy" ) ) {
		return str_replace( "Howdy", "Hello", $translated );
	}

	return $translated;
}

add_filter( 'gettext', 'RavensEgg2024\replace_howdy', 10, 3 );


/**
 * Customizes admin footer.
 */
function edit_admin_footer_text() {

	$theme_data       = wp_get_theme();
	$theme_author_uri = $theme_data->get( 'AuthorURI' );

	printf( 'Handcrafted with WordPress by <a href="%s">Raven&rsquo;s Eye Design</a>. For support, please send an email to <a
                href="mailto:help@ravenseyedesign.com">help@ravenseyedesign.com</a>', $theme_author_uri );

}

add_filter( 'admin_footer_text', 'RavensEgg2024\edit_admin_footer_text' );


//add_filter( 'admin_footer_text', 'RavensEgg2024\admin_footer' );

/**
 * Removes screen options from the dashboard.
 *
 * @return bool
 */
//function remove_screen_options() {
//	global $pagenow;
//
//	if ( 'index.php' == $pagenow ) {
//		return false;
//	}
//
//	return true;
//}
//
//add_filter( 'screen_options_show_screen', 'RavensEgg2024\remove_screen_options' );