<?php
/**
 * Login screen customization
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */

namespace RavensEgg2024;

/**
 * Customizes the login screen.
 */
function custom_login_init() {
	// actions
	add_action( 'login_enqueue_scripts', 'RavensEgg2024\add_login_css' );

	// filters
	add_filter( 'login_headerurl', 'RavensEgg2024\change_login_url' );
	add_filter( 'login_headertext', 'RavensEgg2024\change_login_title' );
}

add_action( 'login_init', 'RavensEgg2024\custom_login_init' );

/**
 * Adds custom login styles.
 */
function add_login_css() {
	wp_enqueue_style( 'ravensegg2024_admin_login', get_template_directory_uri() . get_asset_path( 'login.css' ),
		false );
}

/**
 * Changes the logo link from wordpress.org to the site home.
 *
 * @return mixed|string|void
 */
function change_login_url() {
	return home_url( '/' );
}

/**
 *  Changes the alt text on the logo to site name.
 *
 * @return mixed|void
 */
function change_login_title() {
	return get_option( 'blogname' );
}

/**
 * Adds site ID to login screen
 */
function add_blog_id_to_login_page( $classes ) {
	$blog_id   = get_current_blog_id();
	$classes[] = "site-id-{$blog_id}";

	return $classes;
}

// add_filter( 'login_body_class', 'RavensEgg2024\add_blog_id_to_login_page' );

