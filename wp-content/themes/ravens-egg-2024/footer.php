<?php
/**
 * Footer
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */
?>

<!-- Subfooter -->
<?php if ( is_active_sidebar( 'subfooter' ) ) { ?>

    <div id="subfooter">

        <div class="signup">
            <div class="content">
				<?php dynamic_sidebar( 'subfooter' ); ?>
            </div><!-- .content -->
        </div><!-- .signup -->

    </div><!-- #subfooter -->

<?php } ?>

<!-- Footer -->
<footer id="colophon">
    <div class="content">

        <div id="footer-menu" class="column">

			<?php if ( get_theme_mod( 'display_footer_email' ) == 1 ): ?>

                <p class="email">
                    <svg width="24" height="24">
                        <use xlink:href="#icon-email"></use>
                    </svg>
                    <a href="mailto:<?php bloginfo( 'admin_email' ); ?>"><?php bloginfo( 'admin_email' );
						?></a></p>

			<?php elseif ( ! empty( get_theme_mod( 'footer_left' ) ) ): ?>

				<?php echo '<p class="footer-custom-text">' . get_theme_mod( 'footer_left', '&nbsp;' ) . '</p>';
				?>

			<?php endif; ?>

			<?php wp_nav_menu( array(
				'theme_location' => 'footer',
				'fallback_cb'    => 'false',
				'class'          => 'footer-menu',
			) ); ?>

        </div>

         <div id="logo-footer" class="column">
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="to home page"><?php _e( 'To home page',
                    'ravens-egg-2024' ); ?></a>
        </div>


        <div id="footer-copyright" class="column">
            <div id="footer-socials">
                <?php get_template_part( 'template-parts/aside/social-links' ); ?>
            </div>
            <p id="copyright">Copyright &copy;
                <?php echo date( 'Y' ); ?>
                by <?php bloginfo( 'name' ); ?>.</p>
        </div>

    </div><!-- .content -->

	<?php if ( is_front_page() ) { ?>
        <p class="credit"><a href="<?php RavensEgg2024\print_theme_author_link(); ?>"><?php _e( 'Custom website design by
 <strong>Raven&rsquo;s Eye Design</strong>', 'ravens-egg-2024' ); ?></a></p>
	<?php } ?>

</footer><!-- #colophon -->

<noscript><p
            class="js-warning"><?php _e( 'Javascript appears to be disabled in your browser. Please turn on Javascript in order to fully enjoy this website.', 'ravens-egg-2024' ) ?></p>
</noscript>

<!--[if lte IE 9]>
<div id="browser-status"><p>
    <?php _e( 'It looks like you\'re using an obsolete web browser. This makes it difficult to fully enjoy this website, or to use all of its features. For the best experience on the web, please update your browser.', 'ravens-egg-2024' ); ?></p>
</div>
<![endif]-->

<!-- This website was handcrafted with love and care by Raven's Eye Design, LLC, a creative design firm based in
Tucson, Arizona: <?php RavensEgg2024\print_theme_author_link(); ?> -->

</div><!-- #wrapper -->

<?php wp_footer(); ?>

</body>
</html>