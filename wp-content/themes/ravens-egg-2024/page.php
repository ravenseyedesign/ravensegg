<?php
/**
 * Page template
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */

get_header(); ?>

<?php get_template_part( 'template-parts/header/start' ); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'template-parts/page/content-page' ); ?>

<?php endwhile; ?>

<?php else : ?>

	<?php get_template_part( 'template-parts/post/content-none' ); ?>

<?php endif; ?>

<?php

get_template_part( 'template-parts/page/page-navigation' );

if ( function_exists( 'wp_link_pages' ) ) {
	wp_link_pages( 'before=<p class="pagelink">Page: ' );
}

?>

<?php get_template_part( 'template-parts/footer/end' ); ?>