<?php
/**
 * Sidebar
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */
?>

<!-- Sidebar -->
<div id="secondary">

	<?php dynamic_sidebar( 'Sidebar 1' ); ?>

	<?php dynamic_sidebar( 'Sidebar 2' ); ?>

	<?php // if ( ! is_post_type_archive( 'testimonial' ) && ! is_singular( 'testimonial' ) ) {
	// get_template_part( 'template-parts/aside/testimonials' );
	// } ?>

	<?php //get_template_part( 'template-parts/aside/social-links' ); ?>

</div><!-- #secondary -->