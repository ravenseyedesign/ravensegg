const { merge } = require( 'webpack-merge' );
const common = require( './webpack.common.js' );
const MiniCssExtractPlugin = require( 'mini-css-extract-plugin' );
const CssMinimizerPlugin = require( 'css-minimizer-webpack-plugin' );
const TerserPlugin = require( 'terser-webpack-plugin' );
const RemovePlugin = require( 'remove-files-webpack-plugin' );
const { CleanWebpackPlugin } = require( 'clean-webpack-plugin' );

module.exports = merge( common, {
  mode: 'production',
  module: {
    rules: [
      {
        test: /\.(sc|sa|c)ss$/,
        exclude: /node_modules/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          {
            loader: 'css-loader',
          },
          {
            loader: 'postcss-loader',
          },
          {
            loader: 'sass-loader',
          }
        ]
      },
      {
        test: /\.(png|svg|jpg|gif|webp)$/,
        type: 'asset/resource',
        generator: {
          filename: 'images/[name].[contenthash][ext]'
        }
      },
      {
        test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
        type: 'asset/resource',
        generator: {
          filename: 'fonts/[name].[contenthash][ext]'
        }
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [{
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
          }
        }]
      },
    ]
  },
  optimization: {
    minimize: true,
    minimizer: [
      new CssMinimizerPlugin( {
        minimizerOptions: {
          preset: [
            'default',
            {
              discardComments: { removeAll: true },
            },
          ],
        },
      } ),
      new TerserPlugin( {
        parallel: true,
        terserOptions: {
          ecma: 5,
        },
      } ),
    ],
  },

  plugins: [
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin( {
      filename: 'css/[name].[contenthash].css',
      chunkFilename: 'css/[name].[chunkhash].css'
    } ),
    new RemovePlugin( {
      after: {
        root: '../dist',
        test: [
          {
            folder: './js',
            method: ( absoluteItemPath ) => {
              return new RegExp( /\.js\.LICENSE\.txt$/, 'm' ).test( absoluteItemPath );
            },
          },
          {
            folder: './js',
            method: ( absoluteItemPath ) => {
              return new RegExp( /styles\.bundle\.\S*$/, 'm' ).test( absoluteItemPath );
            },
          },
        ],
      }
    } ),
  ]
} );