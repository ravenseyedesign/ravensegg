const { merge } = require( 'webpack-merge' );
const common = require( './webpack.common.js' );
const MiniCssExtractPlugin = require( 'mini-css-extract-plugin' );
const RemovePlugin = require( 'remove-files-webpack-plugin' );
const { CleanWebpackPlugin } = require( 'clean-webpack-plugin' );

module.exports = merge( common, {
  mode: 'development',
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.(sc|sa|c)ss$/,
        exclude: /node_modules/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          {
            loader: 'css-loader',
            options: {
              sourceMap: true
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: true
            }
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true
            }
          }
        ]
      },
      {
        test: /\.(png|svg|jpg|gif|webp)$/,
        type: 'asset/resource',
        generator: {
          filename: 'images/[name].[contenthash][ext]'
        }
      },
      {
        test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
        type: 'asset/resource',
        generator: {
          filename: 'fonts/[name].[contenthash][ext]'
        }
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [{
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
          }
        }]
      },
    ]
  },
  watch: true,
  watchOptions: {
    ignored: /node_modules/,
    aggregateTimeout: 300,
    poll: 1000
  },

  plugins: [
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin( {
      filename: 'css/[name].[contenthash].css',
      chunkFilename: 'css/[name].[chunkhash].css'
    } ),
    new RemovePlugin( {
      after: {
        root: '../dist',
        test: [
          {
            folder: './js',
            method: ( absoluteItemPath ) => {
              return new RegExp( /\.js\.LICENSE\.txt$/, 'm' ).test( absoluteItemPath );
            },
          },
          {
            folder: './js',
            method: ( absoluteItemPath ) => {
              return new RegExp( /styles\.bundle\.\S*$/, 'm' ).test( absoluteItemPath );
            },
          },
        ],
      }
    } ),
  ]
} );