const webpack = require( 'webpack' );
const path = require( 'path' );
const WebpackAssetsManifest = require( 'webpack-assets-manifest' );

module.exports = {
  entry: {
    index: './js/index.js',
    fonts: './js/fonts.js',
    styles: './js/styles.js',
    megamenu: './js/megamenu.js',
    // megamenu: './js/slider.js',
  },
  externals: {
    jquery: 'jQuery'
  },
  output: {
    path: path.resolve( __dirname, '../dist' ),
    filename: 'js/[name].bundle.[contenthash].js',
    publicPath: '/wp-content/themes/ravens-egg-2024/dist/'
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        editorStylesCSS: {
          test: /editor.scss/,
          name: 'editor',
          chunks: 'all',
          enforce: true
        },
        loginStylesCSS: {
          test: /login.scss/,
          name: 'login',
          chunks: 'all',
          enforce: true
        },
        printStylesCSS: {
          test: /print.scss/,
          name: 'print',
          chunks: 'all',
          enforce: true
        },
        screenStylesCSS: {
          test: /screen.scss/,
          name: 'screen',
          chunks: 'all',
          enforce: true
        },
      }
    },
  },
  plugins: [
    new WebpackAssetsManifest( {
      output: 'data/manifest.json',
    } ),
  ]
};