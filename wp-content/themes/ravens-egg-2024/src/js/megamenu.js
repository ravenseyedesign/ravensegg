/**
 * Theme Name: Raven's Egg 2024
 * Author: Chad Bush
 * Description: Initialize Mega-menu
 */

import hoverintent from 'hoverintent'; // https://github.com/tristen/hoverintent

document.addEventListener( 'DOMContentLoaded', function () {

  /**
   * Custom navigation menu stuff - uses primary menu.
   * For mega-menu, add 'mega-menu-item' class to top-level menu li.
   * For mega-menu headings, add 'mega-head' class to li elements that start new columns.
   * Adjust styles in 972up-menu.scss to set width of columns.
   * If inline sub-headings are needed, add 'mega-subhead' class to li elements.
   * All of the above are null links ('#').
   * Breakpoint for hover JS is hard-coded, be mindful if it changes from 972px wide.
   */

  /**
   * When a menu item is clicked:
   *  - Expand that menu
   *  - Collapse any other open menus at the same level
   *  - Unless it was already open, in which case close it (accordion)
   *
   *  @param {Event} event - DOM node that was clicked on
   */

    // Applies to mobile and desktop width screens.
  const toggleOpenMenu = ( event ) => {
      /**
       * DOM node on which we attached the event listener.
       *
       * This may not be the DOM node which received the actual click. It could
       * be any DOM node inside of the one to which we attached the listener.
       * Events "bubble" and chain up and back down the DOM tree when processing.
       *
       * Compare to `event.target`.
       *
       * @type Element
       */
      const currentTarget = event.currentTarget;

      /**
       * Does this node have sublists under it?
       * @type boolean
       */
      const hasChildren = currentTarget.querySelectorAll( 'ul' ) !== null;
      if ( !hasChildren ) {
        // If we don't have menus underneath us we're not a top-level menu,
        // so abort and let the next DOM node in the event chain do the
        // opening and closing.
        return;
      }

      // If the click originated on another DOM node (event.target) than the one
      // we attached this listener to _and_ it's not a DOM node directly underneath
      // the one we attached to then we should ignore it and abort.
      // We only want to toggle the menus when the top-most menu itself is clicked
      // or when one of its immediate descendants is clicked. (secondary menus?)
      if ( event.target !== currentTarget && event.target.parentNode !== currentTarget ) {
        return;
      }

      const wasAlreadyOpen = currentTarget.classList.contains( 'open' );

      const parentNode = /** @type Element */ currentTarget.parentNode;

      const siblingMenus = parentNode.parentNode.classList.contains( 'mega-column' )
        ? parentNode.parentNode.parentNode.querySelectorAll( 'li' )
        : parentNode.childNodes;

      siblingMenus.forEach(
        node => {
          if ( node.tagName === 'LI' ) {
            node.classList.remove( 'open' );
          }
        }
      );

      // Open the intended DOM node if it wasn't already open.
      if ( !wasAlreadyOpen ) {
        currentTarget.classList.add( 'open' );
      }
    };

  ( document.querySelectorAll( '.primary-menu li' ) || [] )
    .forEach( menuItem => menuItem.addEventListener( 'click', toggleOpenMenu ) );

  /**
   * When hovering over a menu on desktop width screens:
   *  - Expand that menu
   *  - Collapse any other open menus at the same level
   *
   *  @param {Event} event - DOM node that was clicked on
   */

// Function that opens menu on hover.
  const openHoveredMenu = ( menuItem ) => ( event ) => {
    if ( window.innerWidth < 972 ) {
      return;
    }

    menuItem.parentNode.childNodes.forEach(
      node => {
        if ( node.tagName === 'LI' ) {
          node.classList.remove( 'open' );
        }
      }
    );

    menuItem.classList.add( 'open' );
  };

// Function that closes menu on hover away.
  const closeHoveredMenu = ( primaryMenu ) => ( event ) => {
    if ( window.innerWidth < 972 ) {
      return;
    }

    ( primaryMenu.querySelectorAll( 'li' ) || [] )
      .forEach( menuItem => menuItem.classList.remove( 'open' ) );
  };

// Runs open on mouseover via hoverintent.
  ( document.querySelectorAll( '.primary-menu > li' ) || [] )
    .forEach( menuItem =>
      hoverintent(
        menuItem,
        openHoveredMenu( menuItem ),
        () => {}
      )
    );

// Runs close on mouseout away via hoverintent.
  hoverintent(
    document.querySelector( '.primary-menu' ),
    () => {},
    closeHoveredMenu( document.querySelector( '.primary-menu' ) )
  ).options( { timeout: 300 } );

  // Removes links from menu items used only for layout.
  const nonSemanticLinks = document.querySelectorAll( 'li.mega-column > a' );

  nonSemanticLinks.forEach( nonSemanticLink => {
    nonSemanticLink.remove();
  } );

} );