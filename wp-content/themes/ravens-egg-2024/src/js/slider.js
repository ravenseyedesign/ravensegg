/**
 * Theme Name: Raven's Egg 2024
 * Author: Chad Bush
 * Description: Initialize Glide Slider
 */

import Glide from '@glidejs/glide'; // https://glidejs.com/docs/setup/

const sliderExists = document.querySelector( '.glide' );
if ( sliderExists !== null ) {

  new Glide( '.glide', {
    type: 'slideshow',
    autoplay: 4000,
    animationDuration: 900
  } ).mount();

}