/**
 * Theme Name: Raven's Egg 2024
 * Author: Chad Bush
 * Description: Asynchronous font loading init
 *
 */

import WebFont from 'webfontloader';

// Insert into .load the same type of font server that's in config from below:

// With Google Web Fonts
// WebFont.load({
//  google: {
//    families: ['Source Sans Pro:400,400i,600,600i', 'Merriweather:700,700i&display=swap']
//  }
// });

// With Custom Fonts - Also must include _fonts.scss

WebFont.load({
    custom: {
        families: ['Source Sans Pro:n4,i4,n6,i6']
    }
});


// With Typekit

// WebFont.load({
//   typekit: {
//     id: 'xxxxxx'
//   }
// });


// With Fonts.com

// WebFont.load({
//   monotype: {
//     projectId: 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx',
//     version: 12345, // (optional, flushes the CDN cache)
//     loadAllFonts: true //(optional, loads all project fonts)
//   }
// });

