// import $ from 'jquery';

// Vanilla JS libs
import ScrollOut from 'scroll-out'; // https://github.com/scroll-out/scroll-out
import offside from 'offside-js'; // https://github.com/toomuchdesign/offside
import GLightbox from 'glightbox'; // https://github.com/biati-digital/glightbox
// import noframe from 'reframe.js'; // https://github.com/dollarshaveclub/reframe.js

// jQuery document loaded wrapper
// $(document).ready(function () { });

// Vanilla JS document loaded wrapper
document.addEventListener('DOMContentLoaded', function () {

    // Only run if a main menu exists
    const mainMenu = document.getElementById('menu-main-menu');
    if (typeof (mainMenu) != 'undefined' && mainMenu != null) {

        const slidingMenu = offside('.primary-menu', {

            // Global offside options: affect all offside instances
            slidingElementsSelector: '#branding, #primary',  // String: Sliding elements selectors ('#foo, #bar')
            disableCss3dTransforms: false,  // Disable CSS 3d Transforms support (for testing purposes)
            debug: true,  // Boolean: If true, print errors in console

            // Offside instance options: affect only this offside instance
            buttonsSelector: '#open-menu, #close-menu',  // String: Offside toggle buttons selectors ('#foo, #bar')
            slidingSide: 'left',  // String: Offside element pushed on left or right
            // init: function () {},
            // beforeOpen: function () { },
            // afterOpen: function () { },
            // beforeClose: function () { },
            // afterClose: function () { },

        });

        // Removes click behavior on main nav menu null links.
        document.addEventListener('click', function (event) {
            if (event.target.matches('.primary-menu a[href=\'#\']')) {
                event.preventDefault();
            }
        }, false);

    }

    /**
     * ScrollOut init
     */

    // Adds data attribute to scroll-to-top link to show/hide depending on scroll position.
    ScrollOut({
        targets: '#scroll-to-top',
        offset: 400
    });

    // Adds data attribute to nav menu for sticky styles.
    ScrollOut({
        targets: '#access',
        offset: 200
    });

    /**
     * GLightbox init
     */

    // Adds lightbox class to GetWid carousel block.
    // function carouselImageLinks() {
    //     var anchors = document.querySelectorAll(
    //         '.wp-block-getwid-images-slider__item > a[href$=\'.jpg\']');
    //     Array.prototype.forEach.call(anchors, function (a) {
    //         var img = a.querySelector('img');
    //         if (img !== null) {
    //             a.classList.add('lightbox');
    //         }
    //     });
    // }

    // carouselImageLinks();

    // Adds figcaption content into data-description attr of a link within figure for lightbox.
    function addLightboxCaptions() {
        const imageBlock = document.getElementsByClassName('wp-block-image');

        for (let i = 0, max = imageBlock.length; i < max; i++) {

            const link = imageBlock[i].querySelector('a');
            const caption = imageBlock[i].querySelector('figcaption');

            if (link !== null && caption !== null) {
                link.setAttribute('data-description', caption.textContent);
            }
        }
    }

    addLightboxCaptions();

    // Initializes lightbox. Add options from here as needed: https://github.com/biati-digital/glightbox
    const lightbox = GLightbox({
        selector: '.lightbox'
    });

    // Legacy responsive video support - JS method in case videos of varying aspect ratios
    // const iframeWithYouTube = document.querySelectorAll( 'iframe[src*=\'youtube\']' );
    // const blockEmbedIframe = document.querySelectorAll( '.wp-block-embed__wrapper iframe' );
    // if ( blockEmbedIframe.length < 1 ) {
    //   noframe( iframeWithYouTube );
    // }

    // Legacy responsive video support, CSS only.
    // If iframe has youtube in src and is not block embed, adds wrapper for CSS treatment.
    const oldVideos = document.querySelectorAll('iframe[src*=\'youtube\']');
    const newVideo = document.querySelectorAll('.wp-block-embed__wrapper iframe');
    if (newVideo.length < 1) {
        for (let i = 0, max = oldVideos.length; i < max; i++) {
            const oldVideo = oldVideos[i];
            let wrapper = document.createElement('div');
            oldVideo.parentNode.insertBefore(wrapper, oldVideo);
            wrapper.appendChild(oldVideo);
            wrapper = wrapper.classList.add('video-wrapper');
        }
    }

    // Get scrollbar width for vw workaround
    const scrollbarWidth = window.innerWidth - document.body.clientWidth;
    if (scrollbarWidth > 0) {
        document.documentElement.style.setProperty(
            '--scrollbarWidth',
            scrollbarWidth + 'px'
        );
    }

});
