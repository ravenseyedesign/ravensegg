/******/ (function() { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./node_modules/hoverintent/index.js":
/*!*******************************************!*\
  !*** ./node_modules/hoverintent/index.js ***!
  \*******************************************/
/***/ (function(module) {



module.exports = function(el, onOver, onOut) {
  var x, y, pX, pY;
  var mouseOver = false;
  var focused = false;
  var h = {},
    state = 0,
    timer = 0;

  var options = {
    sensitivity: 7,
    interval: 100,
    timeout: 0,
    handleFocus: false
  };

  function delay(el, e) {
    if (timer) timer = clearTimeout(timer);
    state = 0;
    return focused ? undefined : onOut.call(el, e);
  }

  function tracker(e) {
    x = e.clientX;
    y = e.clientY;
  }

  function compare(el, e) {
    if (timer) timer = clearTimeout(timer);
    if ((Math.abs(pX - x) + Math.abs(pY - y)) < options.sensitivity) {
      state = 1;
      return focused ? undefined : onOver.call(el, e);
    } else {
      pX = x;
      pY = y;
      timer = setTimeout(function() {
        compare(el, e);
      }, options.interval);
    }
  }

  // Public methods
  h.options = function(opt) {
    var focusOptionChanged = opt.handleFocus !== options.handleFocus;
    options = Object.assign({}, options, opt);
    if (focusOptionChanged) {
      options.handleFocus ? addFocus() : removeFocus();
    }
    return h;
  };

  function dispatchOver(e) {
    mouseOver = true;
    if (timer) timer = clearTimeout(timer);
    el.removeEventListener('mousemove', tracker, false);

    if (state !== 1) {
      pX = e.clientX;
      pY = e.clientY;

      el.addEventListener('mousemove', tracker, false);

      timer = setTimeout(function() {
        compare(el, e);
      }, options.interval);
    }

    return this;
  }

  function dispatchOut(e) {
    mouseOver = false;
    if (timer) timer = clearTimeout(timer);
    el.removeEventListener('mousemove', tracker, false);

    if (state === 1) {
      timer = setTimeout(function() {
        delay(el, e);
      }, options.timeout);
    }

    return this;
  }

  function dispatchFocus(e) {
    if (!mouseOver) {
      focused = true;
      onOver.call(el, e);
    }
  }

  function dispatchBlur(e) {
    if (!mouseOver && focused) {
      focused = false;
      onOut.call(el, e);
    }
  }

  function addFocus() {
    el.addEventListener('focus', dispatchFocus, false);
    el.addEventListener('blur', dispatchBlur, false);
  }

  function removeFocus() {
    el.removeEventListener('focus', dispatchFocus, false);
    el.removeEventListener('blur', dispatchBlur, false);
  }

  h.remove = function() {
    if (!el) return;
    el.removeEventListener('mouseover', dispatchOver, false);
    el.removeEventListener('mouseout', dispatchOut, false);
    removeFocus();
  };

  if (el) {
    el.addEventListener('mouseover', dispatchOver, false);
    el.addEventListener('mouseout', dispatchOut, false);
  }

  return h;
};


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	!function() {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = function(module) {
/******/ 			var getter = module && module.__esModule ?
/******/ 				function() { return module['default']; } :
/******/ 				function() { return module; };
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	!function() {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = function(exports, definition) {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	!function() {
/******/ 		__webpack_require__.o = function(obj, prop) { return Object.prototype.hasOwnProperty.call(obj, prop); }
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	!function() {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = function(exports) {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	}();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
!function() {
/*!************************!*\
  !*** ./js/megamenu.js ***!
  \************************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var hoverintent__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! hoverintent */ "./node_modules/hoverintent/index.js");
/* harmony import */ var hoverintent__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(hoverintent__WEBPACK_IMPORTED_MODULE_0__);
/**
 * Theme Name: Raven's Egg 2024
 * Author: Chad Bush
 * Description: Initialize Mega-menu
 */
 // https://github.com/tristen/hoverintent

document.addEventListener('DOMContentLoaded', function () {
  /**
   * Custom navigation menu stuff - uses primary menu.
   * For mega-menu, add 'mega-menu-item' class to top-level menu li.
   * For mega-menu headings, add 'mega-head' class to li elements that start new columns.
   * Adjust styles in 972up-menu.scss to set width of columns.
   * If inline sub-headings are needed, add 'mega-subhead' class to li elements.
   * All of the above are null links ('#').
   * Breakpoint for hover JS is hard-coded, be mindful if it changes from 972px wide.
   */

  /**
   * When a menu item is clicked:
   *  - Expand that menu
   *  - Collapse any other open menus at the same level
   *  - Unless it was already open, in which case close it (accordion)
   *
   *  @param {Event} event - DOM node that was clicked on
   */
  // Applies to mobile and desktop width screens.
  var toggleOpenMenu = function toggleOpenMenu(event) {
    /**
     * DOM node on which we attached the event listener.
     *
     * This may not be the DOM node which received the actual click. It could
     * be any DOM node inside of the one to which we attached the listener.
     * Events "bubble" and chain up and back down the DOM tree when processing.
     *
     * Compare to `event.target`.
     *
     * @type Element
     */
    var currentTarget = event.currentTarget;
    /**
     * Does this node have sublists under it?
     * @type boolean
     */

    var hasChildren = currentTarget.querySelectorAll('ul') !== null;

    if (!hasChildren) {
      // If we don't have menus underneath us we're not a top-level menu,
      // so abort and let the next DOM node in the event chain do the
      // opening and closing.
      return;
    } // If the click originated on another DOM node (event.target) than the one
    // we attached this listener to _and_ it's not a DOM node directly underneath
    // the one we attached to then we should ignore it and abort.
    // We only want to toggle the menus when the top-most menu itself is clicked
    // or when one of its immediate descendants is clicked. (secondary menus?)


    if (event.target !== currentTarget && event.target.parentNode !== currentTarget) {
      return;
    }

    var wasAlreadyOpen = currentTarget.classList.contains('open');
    var parentNode =
    /** @type Element */
    currentTarget.parentNode;
    var siblingMenus = parentNode.parentNode.classList.contains('mega-column') ? parentNode.parentNode.parentNode.querySelectorAll('li') : parentNode.childNodes;
    siblingMenus.forEach(function (node) {
      if (node.tagName === 'LI') {
        node.classList.remove('open');
      }
    }); // Open the intended DOM node if it wasn't already open.

    if (!wasAlreadyOpen) {
      currentTarget.classList.add('open');
    }
  };

  (document.querySelectorAll('.primary-menu li') || []).forEach(function (menuItem) {
    return menuItem.addEventListener('click', toggleOpenMenu);
  });
  /**
   * When hovering over a menu on desktop width screens:
   *  - Expand that menu
   *  - Collapse any other open menus at the same level
   *
   *  @param {Event} event - DOM node that was clicked on
   */
  // Function that opens menu on hover.

  var openHoveredMenu = function openHoveredMenu(menuItem) {
    return function (event) {
      if (window.innerWidth < 972) {
        return;
      }

      menuItem.parentNode.childNodes.forEach(function (node) {
        if (node.tagName === 'LI') {
          node.classList.remove('open');
        }
      });
      menuItem.classList.add('open');
    };
  }; // Function that closes menu on hover away.


  var closeHoveredMenu = function closeHoveredMenu(primaryMenu) {
    return function (event) {
      if (window.innerWidth < 972) {
        return;
      }

      (primaryMenu.querySelectorAll('li') || []).forEach(function (menuItem) {
        return menuItem.classList.remove('open');
      });
    };
  }; // Runs open on mouseover via hoverintent.


  (document.querySelectorAll('.primary-menu > li') || []).forEach(function (menuItem) {
    return hoverintent__WEBPACK_IMPORTED_MODULE_0___default()(menuItem, openHoveredMenu(menuItem), function () {});
  }); // Runs close on mouseout away via hoverintent.

  hoverintent__WEBPACK_IMPORTED_MODULE_0___default()(document.querySelector('.primary-menu'), function () {}, closeHoveredMenu(document.querySelector('.primary-menu'))).options({
    timeout: 300
  }); // Removes links from menu items used only for layout.

  var nonSemanticLinks = document.querySelectorAll('li.mega-column > a');
  nonSemanticLinks.forEach(function (nonSemanticLink) {
    nonSemanticLink.remove();
  });
});
}();
/******/ })()
;
//# sourceMappingURL=megamenu.bundle.cab8644fda75a9d5604b.js.map