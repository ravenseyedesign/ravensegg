<?php
/**
 * Comments section
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */

/*
 * If the current post is protected by a password and the visitor has not yet
 * entered the password we will return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<aside id="comments">
    <div class="comments-area">

		<?php if ( have_comments() ) : ?>

            <h3 class="comments-title">
				<?php
				printf( _n( 'One thought on &ldquo;%2$s&rdquo;', '%1$s thoughts on &ldquo;%2$s&rdquo;', get_comments_number(), 'ravens-egg-2024' ),
					number_format_i18n( get_comments_number() ), get_the_title() );
				?>
            </h3>

			<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
                <nav id="comment-nav-above" class="navigation comment-navigation" role="navigation">
                    <p class="screen-reader-text"><?php _e( 'Comment navigation', 'ravens-egg-2024' ); ?></p>

                    <div
                            class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'ravens-egg-2024' ) ); ?></div>
                    <div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'ravens-egg-2024' ) ); ?></div>
                </nav><!-- #comment-nav-above -->
			<?php endif; // Check for comment navigation. ?>

            <ol class="comment-list">
				<?php
				wp_list_comments( array(
					'style'       => 'ol',
					'short_ping'  => true,
					'avatar_size' => 80,
					'callback'    => 'RavensEgg2024\customize_comment',
				) );
				?>
            </ol><!-- .comment-list -->

			<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
                <nav id="comment-nav-below" class="navigation comment-navigation" role="navigation">
                    <p class="screen-reader-text"><?php _e( 'Comment navigation', 'ravens-egg-2024' ); ?></p>

                    <div
                            class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'ravens-egg-2024' ) ); ?></div>
                    <div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'ravens-egg-2024' ) ); ?></div>
                </nav><!-- #comment-nav-below -->
			<?php endif; // Check for comment navigation. ?>

			<?php if ( ! comments_open() ) : ?>
                <p class="no-comments"><?php _e( 'Comments are closed.', 'ravens-egg-2024' ); ?></p>
			<?php endif; ?>

		<?php endif; // have_comments() ?>

		<?php
		$args = array(
			'id_form'           => 'commentform',
			'id_submit'         => 'submit',
			'title_reply'       => __( 'Leave a comment.', 'ravens-egg-2024' ),
			'title_reply_to'    => __( 'Leave a Reply to %s', 'ravens-egg-2024' ),
			'cancel_reply_link' => __( 'Cancel Comment', 'ravens-egg-2024' ),
			'label_submit'      => __( 'Post Comment', 'ravens-egg-2024' ),
			'comment_field'     => '<p class="comment-form-comment"><label for="comment">' . _x( 'Comment', 'noun', 'ravens-egg-2024' ) .
			                       '</label><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true">' .
			                       '</textarea></p>',
		);
		?>

		<?php comment_form( $args ); ?>

    </div>
</aside><!-- #comments -->

