<?php
/**
 * Index of posts
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */

get_header(); ?>

<?php get_template_part( 'template-parts/header/start-wide' ); ?>

<?php // Sets default post category as page title
$cats = wp_get_post_categories( $post->ID );
foreach ( $cats as $category ) {
	$title = get_cat_name( get_option( 'default_category' ) );
} ?>

    <h1 id="page-title"><?php echo $title; ?></h1>

    <div class="cards blog">

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'template-parts/post/content-archive-cards' ); ?>

<?php endwhile; ?>

    </div><!-- .cards.blog -->

	<?php if ( function_exists( 'wp_pagenavi' ) ) {
		echo '<div class="pagenavi-container">';
		wp_pagenavi();
		echo '</div>';
	} ?>

<?php else : ?>

	<?php get_template_part( 'template-parts/post/content-none' ); ?>

<?php endif; ?>

<?php get_template_part( 'template-parts/footer/end-wide' ); ?>