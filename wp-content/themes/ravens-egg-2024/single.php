<?php
/**
 * Single post
 *
 * @package    WordPress
 * @subpackage Ravens_Egg_2024
 * @since      1.0
 * @version    1.0
 */
?>

<?php // Redirects single cpt for non-admins to home page. Admins get custom template.

//if ( get_post_type() === 'cpt' || get_post_type() === 'intro' ) { ?>
    <!---->
    <!--	--><?php
//
//	if ( ! current_user_can( 'administrator' ) ) {
//
//		$location = get_site_url();
//		wp_safe_redirect( $location );
//		exit;
//
//	} else {
//
//		get_header();
//		echo '<div class="container" id="primary"><main id="main">';
//		get_template_part( 'template-parts/cpt/content-redirected' );
//		get_template_part( 'template-parts/footer/end' );
//
//	}
//
//	?>
    <!---->
<?php //} else { ?>

<?php get_header(); ?>

<?php get_template_part( 'template-parts/header/start' ); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php // Displays first category as section header.
//	$category      = get_the_category();
//	$firstCategory = $category[0]->cat_name;
//	printf( '<h2 id="section-title">%s</h2>', $firstCategory );
	?>

	<?php get_template_part( 'template-parts/post/content-single' ); ?>

	<?php if ( is_singular( 'post' ) ) {

		if ( comments_open() || get_comments_number() ) {
			comments_template();
		}
	}
	?>

<?php endwhile; ?>

<?php get_template_part( 'template-parts/post/single-post-navigation' ); ?>

<?php get_template_part( 'template-parts/footer/end' ); ?>

<?php //}