=== Raven's Eye Design Block Spacing ===
Contributors:      Chad Bush
Tags:              block
Tested up to:      5.8.0
Stable tag:        1.2
License:           GPL-2.0-or-later
License URI:       https://www.gnu.org/licenses/gpl-2.0.html

Adds custom controls for top and bottom margins and padding to group and cover core blocks.

== Description ==

I like to use group and cover core blocks as major sections of a page. When a full-width group with a background color
butts up against a full-width cover block vertically or another group with a background color, there should be no margin
 between them. But when one of these blocks appears next to other types of blocks, I want to set the default space
 consistently using custom theme styles.

This plugin adds controls to the core group and cover blocks to select default top and bottom margins, no top margin, no
 bottom margin or no top or bottom margin. It adds controls to the group block to do the same thing with padding.

In order for this to work, styles need to be set in the theme that add default top and bottom margins to these blocks.

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/ravenseyedesign-block-spacing` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress.
3. Add styles like these to your theme:

	// Custom spacing via block filters
	.wp-block-group,
	.wp-block-cover {
		margin-top: 1.5rem;
		margin-bottom: 1.5rem;
	}

== Changelog ==

= 1.0 =
* Release.

= 1.1 =
* Add more padding options.

= 1.2 =
* Add options for bottom of page with negative bottom margins.

= 1.3 =
* Refactor since Lodash was removed from WordPress core, to include it in the build stage instead of pulling it in as a core dependency.
* Add code to place it into the new Inspector panel context, now under Dimensions in the Style panel.
* Update build tools to current versions.

= 1.4 =
* Add controls to remove left and right padding on blocks.

= 1.5 =
* Add style override for new core '&.alignfull.is-layout-constrained' styles.

= 1.6 =
* Fixed bug with style from v1.5.

== How to Build ==

Custom block development starter written with ESNext standard and JSX support – build step required.

This is the long description. No limit, and you can use Markdown (as well as in the following sections).

For backwards compatibility, if this section is missing, the full length of the short description will be used, and
Markdown parsed.

== Build Steps ==

1. Add build scripts to package.json
2. Add register_block_type line for each block to ravenseyedesign-blocks.php
3. npm run build:specific-block

Inspired by https://github.com/rmorse/multiple-blocks-plugin
