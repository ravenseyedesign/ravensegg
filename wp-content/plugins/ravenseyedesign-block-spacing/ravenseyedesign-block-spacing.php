<?php
/**
 * Plugin Name: Raven's Eye Design Block Spacing
 * Plugin URI: https://ravenseyedesign.com
 * Description: Adds simple margin and padding controls to group and cover blocks. Works best with custom theme styles that set default spacing.
 * Requires at least: 6.0
 * Requires PHP:      8.0
 * Version:           1.7
 * Author:            Chad Bush
 * License:           GPL-2.0-or-later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       ravenseyedesign-block-spacing
 *
 * @package           create-block
 */

/**
 * Registers the block using the metadata loaded from the `block.json` file.
 * Behind the scenes, it registers also all assets so they can be enqueued
 * through the block editor in the corresponding context.
 *
 * @see https://developer.wordpress.org/block-editor/how-to-guides/block-tutorial/writing-your-first-block-type/
 */
function create_block_ravenseyedesign_block_spacing_block_init() {
	register_block_type( plugin_dir_path( __FILE__ ) . 'blocks/spacing-group/' );

    // Additional blocks would be registered here
    register_block_type( plugin_dir_path( __FILE__ ) . 'blocks/spacing-cover/' );
}
add_action( 'init', 'create_block_ravenseyedesign_block_spacing_block_init' );
