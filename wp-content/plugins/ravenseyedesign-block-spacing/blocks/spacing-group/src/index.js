/* Inspired by https://css-tricks.com/a-crash-course-in-wordpress-block-filters/ */

/* eslint-disable react/jsx-props-no-spreading, react/prop-types */
import classnames from 'classnames';
import {assign, merge} from 'lodash';
import './style.scss';
import './editor.scss';

const {__} = wp.i18n;
const {addFilter} = wp.hooks;
const {createHigherOrderComponent} = wp.compose;
const {Fragment} = wp.element;
const {useBlockProps, InspectorControls} = wp.blockEditor;
const {PanelBody, SelectControl} = wp.components;

/**
 * Add Margins and Padding attributes to Group block
 *
 * @param  {Object} settings Original block settings
 * @param  {string} name     Block name
 * @return {Object}          Filtered block settings
 */
function addAttributes(settings, name) {
	if (name === 'core/group') {
		return assign({}, settings, {
			attributes: merge(settings.attributes, {
				margins: {
					type: 'string',
					default: '',
				},
				padding: {
					type: 'string',
					default: '',
				},
			}),
		});
	}
	return settings;
}

addFilter(
	'blocks.registerBlockType',
	'ravenseyedesign-block-spacing/src/spacing-group/add-attributes',
	addAttributes,
);

/**
 * Add Top/Bottom Space control to Group block
 */
const addInspectorControl = createHigherOrderComponent((BlockEdit) => {
	return (props) => {
		const {
			attributes: {margins, padding},
			setAttributes,
			name,
		} = props;

		if (name !== 'core/group') {
			return <BlockEdit {...props} />;
		}

		return (
			<Fragment>
				<BlockEdit {...props} />
				<InspectorControls group="dimensions">
					<div className="full-width-control-wrapper">
						<PanelBody title={__('Spacing', 'ravenseyedesign-block-spacing')} initialOpen={true}>
							<SelectControl
								label={__('Top/Bottom Margins', 'ravenseyedesign-block-spacing')}
								value={margins}
								options={[
									{
										label: __('Default', 'ravenseyedesign-block-spacing'),
										value: '',
									},
									{
										label: __('No Margin Top', 'ravenseyedesign-block-spacing'),
										value: 'no-top',
									},
									{
										label: __('No Margin Bottom', 'ravenseyedesign-block-spacing'),
										value: 'no-bottom',
									},
									{
										label: __('No Margin Top or Bottom', 'ravenseyedesign-block-spacing'),
										value: 'no-top-or-bottom',
									},
									{
										label: __('No Margin Top or Bottom of Page', 'ravenseyedesign-block-spacing'),
										value: 'no-top-or-bottom-page',
									},
									{
										label: __('No Margin Bottom of Page', 'ravenseyedesign-block-spacing'),
										value: 'no-bottom-page',
									},
								]}
								onChange={(value) => {
									setAttributes({margins: value});
								}}
							/>
							<SelectControl
								label={__('Top/Bottom Padding', 'ravenseyedesign-block-spacing')}
								value={padding}
								options={[
									{
										label: __('Default', 'ravenseyedesign-block-spacing'),
										value: '',
									},
									{
										label: __('Big Padding Top', 'ravenseyedesign-block-spacing'),
										value: 'big-top',
									},
									{
										label: __('Big Padding Bottom', 'ravenseyedesign-block-spacing'),
										value: 'big-bottom',
									},
									{
										label: __('Big Padding Top and Bottom', 'ravenseyedesign-block-spacing'),
										value: 'big-top-and-bottom',
									},
									{
										label: __('Medium Padding Top', 'ravenseyedesign-block-spacing'),
										value: 'medium-top',
									},
									{
										label: __('Medium Padding Bottom', 'ravenseyedesign-block-spacing'),
										value: 'medium-bottom',
									},
									{
										label: __('Medium Padding Top and Bottom', 'ravenseyedesign-block-spacing'),
										value: 'medium-top-and-bottom',
									},
									{
										label: __('No Padding Top', 'ravenseyedesign-block-spacing'),
										value: 'no-top',
									},
									{
										label: __('No Padding Bottom', 'ravenseyedesign-block-spacing'),
										value: 'no-bottom',
									},
									{
										label: __('No Padding Top and Bottom', 'ravenseyedesign-block-spacing'),
										value: 'no-top-and-bottom',
									},
								]}
								onChange={(value) => {
									setAttributes({padding: value});
								}}
							/>
							<SelectControl
								label={__('No Side Padding Options', 'ravenseyedesign-block-spacing')}
								value={padding}
								options={[
									{
										label: __('Default', 'ravenseyedesign-block-spacing'),
										value: '',
									},
									{
										label: __('No Sides - Default T & B', 'ravenseyedesign-block-spacing'),
										value: 'no-sides',
									},
									{
										label: __('No Sides - Big T', 'ravenseyedesign-block-spacing'),
										value: 'big-top-no-sides',
									},
									{
										label: __('No Sides - Big B', 'ravenseyedesign-block-spacing'),
										value: 'big-bottom-no-sides',
									},
									{
										label: __('No Sides - Big T & B', 'ravenseyedesign-block-spacing'),
										value: 'big-top-and-bottom-no-sides',
									},
									{
										label: __('No Sides - Medium T', 'ravenseyedesign-block-spacing'),
										value: 'medium-top-no-sides',
									},
									{
										label: __('No Sides - Medium B', 'ravenseyedesign-block-spacing'),
										value: 'medium-bottom-no-sides',
									},
									{
										label: __('No Sides - Medium T & B', 'ravenseyedesign-block-spacing'),
										value: 'medium-top-and-bottom-no-sides',
									},
									{
										label: __('No Sides - No T', 'ravenseyedesign-block-spacing'),
										value: 'no-top-no-sides',
									},
									{
										label: __('No Sides - No B', 'ravenseyedesign-block-spacing'),
										value: 'no-bottom-no-sides',
									},
									{
										label: __('No Sides - No T & B', 'ravenseyedesign-block-spacing'),
										value: 'no-top-and-bottom-no-sides',
									},

								]}
								onChange={(value) => {
									setAttributes({padding: value});
								}}
							/>
						</PanelBody>
					</div>
				</InspectorControls>
			</Fragment>
		);
	};
}, 'withInspectorControl');

addFilter(
	'editor.BlockEdit',
	'ravenseyedesign-block-spacing/src/spacing-group/add-inspector-controls',
	addInspectorControl,
);

/**
 * Add margins class to the block in the editor
 */
const addMarginsClassEditor = createHigherOrderComponent((BlockListBlock) => {
	return (props) => {
		const {
			attributes: {margins},
			className,
			name,
		} = props;

		if (name !== 'core/group') {
			return <BlockListBlock {...props} />;
		}

		return (
			<BlockListBlock
				{...props}
				className={classnames(className, margins ? `has-margins-${margins}` : '')}
			/>
		);
	};
}, 'withClientIdClassName');

addFilter(
	'editor.BlockListBlock',
	'ravenseyedesign-block-spacing/src/spacing-group/add-editor-class',
	addMarginsClassEditor,
);

/**
 * Add padding class to the block in the editor
 */
const addPaddingClassEditor = createHigherOrderComponent((BlockListBlock) => {
	return (props) => {
		const {
			attributes: {padding},
			className,
			name,
		} = props;

		if (name !== 'core/group') {
			return <BlockListBlock {...props} />;
		}

		return (
			<BlockListBlock
				{...props}
				className={classnames(className, padding ? `has-padding-${padding}` : '')}
			/>
		);
	};
}, 'withClientIdClassName');

addFilter(
	'editor.BlockListBlock',
	'ravenseyedesign-block-spacing/src/spacing-group/add-editor-class',
	addPaddingClassEditor,
);

/**
 * Add margins class to the block on the front end
 *
 * @param  {Object} props      Additional props applied to save element.
 * @param  {Object} block      Block type.
 * @param  {Object} attributes Current block attributes.
 * @return {Object}            Filtered props applied to save element.
 */
function addMarginsClassFrontEnd(props, block, attributes) {
	if (block.name !== 'core/group') {
		return props;
	}

	const {className} = props;
	const {margins} = attributes;

	return assign({}, props, {
		className: classnames(className, margins ? `has-margins-${margins}` : ''),
	});
}

addFilter(
	'blocks.getSaveContent.extraProps',
	'ravenseyedesign-block-spacing/src/spacing-group/add-front-end-class',
	addMarginsClassFrontEnd,
);

/**
 * Add padding class to the block on the front end
 *
 * @param  {Object} props      Additional props applied to save element.
 * @param  {Object} block      Block type.
 * @param  {Object} attributes Current block attributes.
 * @return {Object}            Filtered props applied to save element.
 */
function addPaddingClassFrontEnd(props, block, attributes) {
	if (block.name !== 'core/group') {
		return props;
	}

	const {className} = props;
	const {padding} = attributes;

	return assign({}, props, {
		className: classnames(className, padding ? `has-padding-${padding}` : ''),
	});
}

addFilter(
	'blocks.getSaveContent.extraProps',
	'ravenseyedesign-block-spacing/src/spacing-group/add-front-end-class',
	addPaddingClassFrontEnd,
);
