/* Inspired by https://css-tricks.com/a-crash-course-in-wordpress-block-filters/ */

/* eslint-disable react/jsx-props-no-spreading, react/prop-types */
import classnames from 'classnames';
import {assign, merge} from 'lodash';
import './style.scss';
import './editor.scss';

const {__} = wp.i18n;
const {addFilter} = wp.hooks;
const {createHigherOrderComponent} = wp.compose;
const {Fragment} = wp.element;
const {useBlockProps, InspectorControls} = wp.blockEditor;
const {PanelBody, SelectControl} = wp.components;

/**
 * Add Margins attribute to Cover block
 *
 * @param  {Object} settings Original block settings
 * @param  {string} name     Block name
 * @return {Object}          Filtered block settings
 */
function addAttributes(settings, name) {
	if (name === 'core/cover') {
		return assign({}, settings, {
			attributes: merge(settings.attributes, {
				margins: {
					type: 'string',
					default: '',
				},
			}),
		});
	}
	return settings;
}

addFilter(
	'blocks.registerBlockType',
	'ravenseyedesign-block-spacing/src/spacing-cover/add-attributes',
	addAttributes,
);

/**
 * Add Top/Bottom Space control to Cover block
 */
const addInspectorControl = createHigherOrderComponent((BlockEdit) => {
	return (props) => {
		const {
			attributes: {margins},
			setAttributes,
			name,
		} = props;

		if (name !== 'core/cover') {
			return <BlockEdit {...props} />;
		}

		return (
			<Fragment>
				<BlockEdit {...props} />
				<InspectorControls group="dimensions">
					<div className="full-width-control-wrapper">
						<PanelBody title={__('Top/Bottom Space', 'ravenseyedesign-block-spacing')} initialOpen={false}>
							<SelectControl
								label={__('Margins', 'ravenseyedesign-block-spacing')}
								value={margins}
								options={[
									{
										label: __('Default', 'ravenseyedesign-block-spacing'),
										value: '',
									},
									{
										label: __('No Margin Top', 'ravenseyedesign-block-spacing'),
										value: 'no-top',
									},
									{
										label: __('No Margin Bottom', 'ravenseyedesign-block-spacing'),
										value: 'no-bottom',
									},
									{
										label: __('No Margin Top or Bottom', 'ravenseyedesign-block-spacing'),
										value: 'no-top-or-bottom',
									},
									{
										label: __('No Margin Top or Bottom of Page', 'ravenseyedesign-block-spacing'),
										value: 'no-top-or-bottom-page',
									},
									{
										label: __('No Margin Bottom of Page', 'ravenseyedesign-block-spacing'),
										value: 'no-bottom-page',
									},
								]}
								onChange={(value) => {
									setAttributes({margins: value});
								}}
							/>
						</PanelBody>
					</div>
				</InspectorControls>
			</Fragment>
		);
	};
}, 'withInspectorControl');

addFilter(
	'editor.BlockEdit',
	'ravenseyedesign-block-spacing/src/spacing-cover/add-inspector-controls',
	addInspectorControl,
);

/**
 * Add margins class to the block in the editor
 */
const addMarginsClassEditor = createHigherOrderComponent((BlockListBlock) => {
	return (props) => {
		const {
			attributes: {margins},
			className,
			name,
		} = props;

		if (name !== 'core/cover') {
			return <BlockListBlock {...props} />;
		}

		return (
			<BlockListBlock
				{...props}
				className={classnames(className, margins ? `has-margins-${margins}` : '')}
			/>
		);
	};
}, 'withClientIdClassName');

addFilter(
	'editor.BlockListBlock',
	'ravenseyedesign-block-spacing/src/spacing-cover/add-editor-class',
	addMarginsClassEditor,
);

/**
 * Add margins class to the block on the front end
 *
 * @param  {Object} props      Additional props applied to save element.
 * @param  {Object} block      Block type.
 * @param  {Object} attributes Current block attributes.
 * @return {Object}            Filtered props applied to save element.
 */
function addMarginsClassFrontEnd(props, block, attributes) {
	if (block.name !== 'core/cover') {
		return props;
	}

	const {className} = props;
	const {margins} = attributes;

	return assign({}, props, {
		className: classnames(className, margins ? `has-margins-${margins}` : ''),
	});
}

addFilter(
	'blocks.getSaveContent.extraProps',
	'ravenseyedesign-block-spacing/src/spacing-cover/add-front-end-class',
	addMarginsClassFrontEnd,
);
