/**
 * Block dependencies
 */
import classnames from 'classnames';
import icons from './icons';
import './style.scss';
import './editor.scss';

/**
 * Internal block libraries
 */
const {
	__,
	sprintf
} = wp.i18n;
const { registerBlockType } = wp.blocks;
const { format } = wp.date;

const {
	Fragment,
	Component
} = wp.element;

const {
	RichText,
	URLInput,
} = wp.blockEditor;

const {
	Button,
	Tooltip,
	TextControl,
	Dashicon,
	SelectControl,
	DateTimePicker,
} = wp.components;

/**
 * Register block
 */
export default registerBlockType(
	'ravenseyeblocks/work-intro',
	{
		title: __( 'Work Intro', 'ravenseyeblocks' ),
		description: __( 'Intro with basic project data for top of case study.', 'ravenseyeblocks' ),
		category: 'common',
		icon: {
			src: icons.default,
		},
		keywords: [
			__( 'work', 'ravenseyeblocks' ),
			__( 'intro', 'ravenseyeblocks' ),
			__( 'section', 'ravenseyeblocks' ),
		],
		attributes: {
			projectDesc: {
				type: 'array',
				source: 'children',
				selector: '.project-description',
			},
			liveUrl: {
				type: 'string',
				source: 'attribute',
				attribute: 'href',
				selector: 'a',
			},
			launchDate: {
				type: 'string',
			}
		},

		edit: ( props => {
			const { attributes, setAttributes, isSelected } = props;

			// Project description
			const onChangeProjectDesc = value => {
				setAttributes( { projectDesc: value } );
			};
			// Live site link
			const { attributes: { liveUrl } } = props;
			const urlNoProtocol = liveUrl && liveUrl.replace( /^https?:\/\//i, '' );

			return (
				<section className='basics'>
					<h3>
						<RichText
							placeholder={__( 'Project description...', 'ravenseyeblocks' )}
							value={attributes.projectDesc}
							onChange={onChangeProjectDesc}
						/>
					</h3>

					<div>
						{isSelected ? (
							<Fragment>
								<p><em>{__( 'Link to project', 'ravenseyeblocks' )}</em></p>
								<form
									className='block-library-button__inline-link'
									onSubmit={( event ) => event.preventDefault()}>
									<Dashicon icon="admin-links"/>
									<URLInput
										value={liveUrl}
										onChange={( value ) => setAttributes( { liveUrl: value } )}
									/>
									<Button icon='editor-break' label={__( 'Apply' )} type='submit'/>
								</form>
							</Fragment>

						) : (

							<p>
								<a href={liveUrl}>
									{liveUrl ? sprintf( __( 'Visit %s', 'ravenseyeblocks' ), urlNoProtocol ) : __( 'Edit link', 'ravenseyeblocks' )}
								</a>
							</p>

						)}

					</div>

					<em>{__( 'Launched ', 'ravenseyeblocks' )}</em>

					{isSelected ? (
						<DateTimePicker
							currentDate={attributes.launchDate}
							onChange={( launchDate ) => setAttributes( { launchDate } )}
						/>
					) : (
						attributes.launchDate ? format( 'F, Y', attributes.launchDate ) : __( 'Select date', 'ravenseyeblocks' )
					)}
				</section>
			);
		} ),
		save: props => {
			const { attributes: { liveUrl = '', postTitle, launchDate: launchDateString } } = props;
			const urlNoProtocol = liveUrl && liveUrl.replace( /^https?:\/\//i, '' );
			const isEmpty = liveUrl.length <= 0;
			const launchDate = launchDateString && new Date( launchDateString );
			return (
				<section className='basics'>

					<h3 className='project-description'>{props.attributes.projectDesc}</h3>

					<p className='live-url'>
						{isEmpty ? (
							__( 'Live link is unavailable.', 'ravenseyeblocks' )
						) : (

							<a className='button arrow' href={liveUrl} rel='noopener noreferrer'>
								{sprintf( __( 'Visit %s', 'ravenseyeblocks' ), urlNoProtocol )}
							</a>
						)}
					</p>

					{launchDate &&
					<p className='launch-date'>
						{__( 'Launched ', 'ravenseyeblocks' )}
						<span className='month'>{format( 'F', launchDate )}</span>
						<span className='year'>{launchDate.getFullYear()}</span>
					</p>}

				</section> );
		}
	},
);
