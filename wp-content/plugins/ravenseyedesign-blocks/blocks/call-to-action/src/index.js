/**
 * Block dependencies
 */
import classnames from 'classnames';
import icon from './icon';
import './style.scss';
import './editor.scss';

/**
 * Internal block libraries
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { InnerBlocks } = wp.blockEditor;

const ALLOWED_BLOCKS = ['core/button'];
const TEMPLATE = [
	['core/button', {
		placeholder: 'Add a call to action...',
		className: 'call-to-action',
	}],
];

/**
 * Register block
 */
export default registerBlockType(
	'ravenseyeblocks/call-to-action',
	{
		title: __( 'Call to Action', 'ravenseyeblocks' ),
		description: __( 'Call to action button with text and link.', 'ravenseyeblocks' ),
		category: 'common',
		icon: {
			src: icon,
		},
		keywords: [
			__( 'Call to Action', 'ravenseyeblocks' ),
			__( 'Link', 'ravenseyeblocks' ),
			__( 'Button', 'ravenseyeblocks' ),
		],

		edit: () =>
			<InnerBlocks allowedBlocks={ALLOWED_BLOCKS}
									 template={TEMPLATE}
									 templateLock={'insert'}
									 templateInsertUpdatesSelection={false}
			/>,
		save: () =>
			<InnerBlocks.Content/>
	},
);
