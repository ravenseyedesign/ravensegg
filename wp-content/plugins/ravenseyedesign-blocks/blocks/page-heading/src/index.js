/**
 * Block dependencies
 */
import classnames from 'classnames';
import icon from './icon';
import './style.scss';
import './editor.scss';

/**
 * Internal block libraries
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { InnerBlocks } = wp.blockEditor;

const ALLOWED_BLOCKS = ['core/heading'];
const TEMPLATE = [
	['core/heading', {
		placeholder: 'Enter page heading text...',
		level: 2,
		className: 'page-heading'
	}],
];

/**
 * Register block
 */
export default registerBlockType(
	'ravenseyeblocks/page-heading',
	{
		title: __( 'Page Heading', 'ravenseyeblocks' ),
		description: __( 'Page heading with preset class.', 'ravenseyeblocks' ),
		category: 'common',
		icon: {
			src: icon,
		},
		keywords: [
			__( 'heading', 'ravenseyeblocks' ),
			__( 'overlay', 'ravenseyeblocks' ),
			__( 'page', 'ravenseyeblocks' ),
		],

		edit: () =>
			<InnerBlocks allowedBlocks={ALLOWED_BLOCKS}
									 template={TEMPLATE}
									 templateLock={'insert'}
									 templateInsertUpdatesSelection={ false }
			/>,
		save: () =>
			<InnerBlocks.Content/>
	},
);
