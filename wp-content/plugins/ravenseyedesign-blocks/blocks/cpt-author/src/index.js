/**
 * Block dependencies
 */
import classnames from 'classnames';
import icon from './icon';
import './style.scss';
import './editor.scss';

/**
 * Internal block libraries
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { InnerBlocks } = wp.blockEditor;

const ALLOWED_BLOCKS = ['core/paragraph'];
const TEMPLATE = [
	['core/paragraph', {
		placeholder: 'Add author...',
		className: 'cpt-author',
	}],
];

/**
 * Register block
 */
export default registerBlockType(
	'ravenseyeblocks/cpt-author',
	{
		title: __( 'Posttype Author', 'ravenseyeblocks' ),
		description: __( 'Author for Posttype (insert CPT here).', 'ravenseyeblocks' ),
		category: 'common',
		icon: {
			src: icon,
		},
		keywords: [
			__( 'Author', 'ravenseyeblocks' ),
			__( 'Posttype', 'ravenseyeblocks' ),
			__( 'Author List', 'ravenseyeblocks' ),
		],

		edit: () =>
			<InnerBlocks allowedBlocks={ALLOWED_BLOCKS}
									 template={TEMPLATE}
									 templateLock={'insert'}
									 templateInsertUpdatesSelection={false}
			/>,
		save: () =>
			<InnerBlocks.Content/>
	},
);
