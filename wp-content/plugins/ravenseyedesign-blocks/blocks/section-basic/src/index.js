/**
 * Block dependencies
 */
import classnames from 'classnames';
import icon from './icon';
import './style.scss';
import './editor.scss';

/**
 * Internal block libraries
 */
const { __ } = wp.i18n;
const { Fragment } = wp.element;
const { registerBlockType } = wp.blocks;
const {
  RichText,
  URLInput,
  Editable,
} = wp.blockEditor;

const {
  Button,
  TextControl,
  Tooltip,
  Dashicon,
} = wp.components;

/**
 * Register block
 */
export default registerBlockType(
  'ravenseyeblocks/section-basic',
  {
    title: __( 'Section Basic', 'ravenseyeblocks' ),
    description: __( 'A basic text block without a background.', 'ravenseyeblocks' ),
    category: 'common',
    icon: {
      src: icon,
    },
    keywords: [
      __( 'basic', 'ravenseyeblocks' ),
      __( 'text-field', 'ravenseyeblocks' ),
      __( 'post', 'ravenseyeblocks' ),
    ],
    attributes: {
      heading: {
        type: 'string',
      },
      content: {
        type: 'array',
        source: 'children',
        selector: '.inner',
      },
      ctaUrl: {
        type: 'string',
        source: 'attribute',
        attribute: 'href',
        selector: 'a',
      },
      ctaLabel: {
        type: 'string',
        source: 'text',
        selector: 'a',
      },
    },
    edit: props => {
      const { attributes, setAttributes, isSelected } = props;
      const { attributes: { ctaLabel, ctaUrl } } = props;
      const onChangeHeading = value => { setAttributes( { heading: value } ); };
      const onChangeContent = value => { setAttributes( { content: value } ); };

      return (
        <section className='basic'>
          <h2><RichText
            tagname='h2'
            placeholder={__( 'Heading...', 'ravenseyeblocks' )}
            value={attributes.heading}
            onChange={onChangeHeading}
          /></h2>
          <RichText
            multiline='p'
            placeholder={__( 'Body Text...', 'ravenseyeblocks' )}
            value={attributes.content}
            onChange={onChangeContent}
          />

          <div>
            {isSelected ? (
              <Fragment>
                <TextControl
                  id='cta-link'
                  label={__( 'Call to action text', 'ravenseyeblocks' )}
                  value={ctaLabel}
                  onChange={ctaLabel => setAttributes( { ctaLabel } )}
                />
                <em className='controls'>{__( 'Call to action link ', 'ravenseyeblocks' )}</em>
                <form
                  className='block-library-button__inline-link'
                  onSubmit={( event ) => event.preventDefault()}>
                  <Dashicon icon='admin-links'/>
                  <URLInput
                    value={ctaUrl}
                    onChange={( value ) => setAttributes( { ctaUrl: value } )}
                  />
                  <Button icon='editor-break' label={__( 'Apply' )} type='submit'/>
                </form>
              </Fragment>

            ) : (

              <p>
                <a href={ctaUrl}>
                  {ctaLabel || __( 'Add/edit link', 'ravenseyeblocks' )}
                </a>
              </p>
            )}

          </div>

        </section>
      );
    },
    save: props => {
      const { attributes: { ctaLabel, ctaUrl } } = props;

      return (
        <section className='basic'>
          <h2>{props.attributes.heading}</h2>
          <div className='inner'>
            {props.attributes.content}
          </div>

          {ctaLabel && <p className='cta'>
            <a href={ctaUrl}>
              {ctaLabel}
            </a>
          </p>}

        </section> );
    },
  },
);
