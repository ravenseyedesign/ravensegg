/**
 * Block dependencies
 */
import icons from './icons';
import './style.scss';
import './editor.scss';

/**
 * Internal block libraries
 */
const { __ } = wp.i18n;
const {Fragment} = wp.element;
const { registerBlockType } = wp.blocks;
const {
  RichText,
  URLInput,
  Editable,
  MediaUpload,
} = wp.blockEditor;
const {
  Button,
  TextControl,
  Tooltip,
  Dashicon,
} = wp.components;

/**
 * Register block
 */
export default registerBlockType(
  'ravenseyeblocks/section-image-overlay',
  {
    title: __( 'Section Image Overlay', 'ravenseyeblocks' ),
    description: __( 'A section with a full-width image overlaid by a translucent text block.', 'ravenseyeblocks' ),
    category: 'common',
    icon: {
      src: icons.default,
    },
    keywords: [
      __( 'image', 'ravenseyeblocks' ),
      __( 'overlay', 'ravenseyeblocks' ),
      __( 'section', 'ravenseyeblocks' ),
    ],
    attributes: {
      heading: {
        type: 'string',
      },
      content: {
        type: 'array',
        source: 'children',
        selector: '.text-block',
      },
      ctaUrl: {
        type: 'string',
        source: 'attribute',
        attribute: 'href',
        selector: 'a',
      },
      ctaLabel: {
        type: 'string',
        source: 'text',
        selector: 'a',
      },
      imgURL: {
        type: 'string',
        source: 'attribute',
        attribute: 'src',
        selector: 'img',
      },
      imgID: {
        type: 'number',
      },
      imgAlt: {
        type: 'string',
        source: 'attribute',
        attribute: 'alt',
        selector: 'img',
      }
    },
    edit: props => {
      const { attributes, setAttributes, isSelected } = props;
      const { attributes: { imgID, imgURL, imgAlt } } = props;
      const { attributes: { ctaLabel, ctaUrl } } = props;
      const onChangeHeading = value => { setAttributes( { heading: value } ); };
      const onChangeContent = value => { setAttributes( { content: value } ); };
      const onSelectImage = img => {
        setAttributes( {
          imgID: img.id,
          imgURL: img.url,
          imgAlt: img.alt,
        } );
      };
      const onRemoveImage = () => {
        setAttributes( {
          imgID: null,
          imgURL: null,
          imgAlt: null,
        } );
      };

      return (
        <section className={'image-overlay'}>

          <h2><RichText
            tagname='h2'
            placeholder={__( 'Heading...', 'ravenseyeblocks' )}
            value={attributes.heading}
            onChange={onChangeHeading}
          /></h2>
          <em className="controls">{__( 'Background image', 'ravenseyeblocks' )}</em>
          {!imgID ? (
            <MediaUpload
              onSelect={onSelectImage}
              type='image'
              value={imgID}
              render={( { open } ) => (
                <Button
                  className={'button button-large'}
                  onClick={open}
                >
                  {icons.upload}
                  {__( ' Upload Image', 'ravenseyeblocks' )}
                </Button>
              )}
            >
            </MediaUpload>
          ) : (
            <p className='image-wrapper'>
              <img
                src={imgURL}
                alt={imgAlt}
              />
              {isSelected ? (
                <Button
                  className='remove-image'
                  onClick={onRemoveImage}
                >
                  {icons.remove}
                </Button>
              ) : null}
            </p>
          )}
          <div className={'text-block'}>
            <RichText
              multiline='p'
              placeholder={__( 'Body Text...', 'ravenseyeblocks' )}
              value={attributes.content}
              onChange={onChangeContent}
            />
          </div>

          <div>
            {isSelected ? (
              <Fragment>
                <TextControl
                  id='cta-link'
                  label={__( 'Call to action text', 'ravenseyeblocks' )}
                  value={ctaLabel}
                  onChange={ctaLabel => setAttributes( { ctaLabel } )}
                />
                <em className='controls'>{__( 'Call to action link ', 'ravenseyeblocks' )}</em>
                <form
                  className='block-library-button__inline-link'
                  onSubmit={( event ) => event.preventDefault()}>
                  <Dashicon icon='admin-links'/>
                  <URLInput
                    value={ctaUrl}
                    onChange={( value ) => setAttributes( { ctaUrl: value } )}
                  />
                  <Button icon='editor-break' label={__( 'Apply' )} type='submit'/>
                </form>
              </Fragment>

            ) : (

              <p>
                <a href={ctaUrl}>
                  {ctaLabel || __( 'Add/edit link', 'ravenseyeblocks' )}
                </a>
              </p>
            )}

          </div>


        </section>
      );
    },
    save: props => {
      const { imgURL, imgAlt } = props.attributes;
      const { attributes: { ctaLabel, ctaUrl } } = props;

      return (
        <section className={'image-overlay'}>
          <img
            src={imgURL}
            alt={imgAlt}
          />
          <div className={'overlay'}>
            <h2>{props.attributes.heading}</h2>
            <div className={'text-block'}>
              {props.attributes.content}
            </div>

            {ctaLabel && <p className='cta'>
              <a href={ctaUrl}>
                {ctaLabel}
              </a>
            </p>}

          </div>
        </section> );
    },
  },
);
