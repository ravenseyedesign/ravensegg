/**
 * Block dependencies
 */
import icon from './icon';
import './style.scss';

/**
 * Internal block libraries
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { RichText, PlainText } = wp.blockEditor;

/**
 * Register block
 */
export default registerBlockType(
  'ravenseyeblocks/section-instagram',
  {
    title: __( 'Section Instagram', 'ravenseyeblocks' ),
    description: __( 'A block for Instagram feed.', 'ravenseyeblocks' ),
    category: 'common',
    icon: {
      src: icon,
    },
    keywords: [
      __( 'Basic', 'ravenseyeblocks' ),
      __( 'Shortcode', 'ravenseyeblocks' ),
      __( 'Post', 'ravenseyeblocks' ),
    ],
    attributes: {
      heading: {
        type: 'array',
        source: 'children',
        selector: 'h2',
      },
      text: {
        type: 'string',
      },
    },
    edit: props => {
      const {
        attributes,
        setAttributes
      } = props;
      const onChangeHeading = value => { setAttributes( { heading: value } ); };
      const onChangeText = value => { setAttributes( { text: value } ); };
      return (
        <section className='instagram'>
          <h2><RichText
            tagname='h2'
            placeholder={__( 'Heading...', 'ravenseyeblocks' )}
            value={attributes.heading}
            onChange={onChangeHeading}
          /></h2>
          <p><PlainText
            placeholder={__( 'Instagram plugin shortcode...', 'ravenseyeblocks' )}
            value={attributes.text}
            onChange={onChangeText}
          /></p>
        </section>
      );
    },
    save: props => {
      return (
        <section className='instagram'>
          <h2>{props.attributes.heading}</h2>
          <p>{props.attributes.text}</p>
        </section> );
    },
  },
);
