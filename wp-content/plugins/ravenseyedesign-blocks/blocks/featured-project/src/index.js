/**
 * Block dependencies
 */
import classnames from 'classnames';
import icon from './icon';
import './style.scss';
import './editor.scss';

/**
 * Internal block libraries
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { InnerBlocks } = wp.blockEditor;

/**
 * Register block
 */
export default registerBlockType(
	'ravenseyeblocks/featured-project',
	{
		title: __( 'Featured Project', 'ravenseyeblocks' ),
		description: __( 'Featured Project section for home page', 'ravenseyeblocks' ),
		category: 'common',
		icon: {
			src: icon,
		},
		keywords: [
			__( 'section', 'ravenseyeblocks' ),
			__( 'info', 'ravenseyeblocks' ),
			__( 'project', 'ravenseyeblocks' ),
		],
		edit: () => <section className='featured-project'>
			<h3 className='subsection-title'>Featured Project</h3>
			<InnerBlocks allowedBlocks={['core/image', 'ravenseyeblocks/basic-info']}
									 template={[
										 ['core/image', {}, []],
										 ['ravenseyeblocks/basic-info', {}, []],
									 ]}
									 templateLock='insert'/>
		</section>,
		save: () => <section className='featured-project'>
			<h3 className='subsection-title'>Featured Project</h3>

			<InnerBlocks.Content/>

		</section>
	},
);
