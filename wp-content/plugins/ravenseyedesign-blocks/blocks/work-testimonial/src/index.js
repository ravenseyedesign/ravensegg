/**
 * Block dependencies
 */
import classnames from 'classnames';
import icon from './icon';
import './style.scss';
import './editor.scss';

/**
 * Internal block libraries
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { InnerBlocks } = wp.blockEditor;

/**
 * Register block
 */
export default registerBlockType(
	'ravenseyeblocks/work-testimonial',
	{
		title: __( 'Work Testimonial', 'ravenseyeblocks' ),
		description: __( 'A full-width section with a testimonial in the middle.', 'ravenseyeblocks' ),
		category: 'common',
		icon: {
			src: icon,
		},
		keywords: [
			__( 'section', 'ravenseyeblocks' ),
			__( 'blockquote', 'ravenseyeblocks' ),
			__( 'project', 'ravenseyeblocks' ),
		],
		edit: () => <section className='testimonial'>
			<InnerBlocks allowedBlocks={['core/quote']}
									 template={[['core/quote', {}, []]]}
									 templateLock='all'/></section>,
		save: () => <section className='work testimonial'>
			<div className='inner'><InnerBlocks.Content/></div>
		</section>
	},
);
