/**
 * Block dependencies
 */
import classnames from 'classnames';
import icon from './icon';
import './style.scss';
import './editor.scss';

/**
 * Internal block libraries
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { InnerBlocks } = wp.blockEditor;

/**
 * Register block
 */
export default registerBlockType(
	'ravenseyeblocks/basic-info',
	{
		title: __( 'Basic Info', 'ravenseyeblocks' ),
		description: __( 'Basic info inserted into Featured Project block', 'ravenseyeblocks' ),
		category: 'common',
		icon: {
			src: icon,
		},
		keywords: [
			__( 'section', 'ravenseyeblocks' ),
			__( 'info', 'ravenseyeblocks' ),
			__( 'project', 'ravenseyeblocks' ),
		],
		parent: ['ravenseyeblocks/featured-project'],
		edit: () => <div className='basics'>
			<div className='inner'>
				<InnerBlocks allowedBlocks={['core/heading', 'core/button']}
										 template={[
											 ['core/heading', {
												 level: 3,
												 placeholder: 'Project title',
												 className: 'work-title'
											 }, []],
											 ['core/heading', {
												 level: 4,
												 placeholder: 'Project description',
												 className: 'project-description'
											 }, []],
											 ['core/button', {
												 text: 'View project details',
												 className: 'live-url'
											 }, []],
										 ]}
										 templateLock='insert'/>
			</div>
		</div>,
		save: () => <div className='basics'>
			<div className='inner'>
				<InnerBlocks.Content/>
			</div>
		</div>
	},
);
