/**
 * Block dependencies
 */
import icon from './icon';
import './style.scss';

/**
 * Internal block libraries
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { PlainText } = wp.blockEditor;

/**
 * Register block
 */
export default registerBlockType(
  'ravenseyeblocks/section-signup',
  {
    title: __( 'Section Signup', 'ravenseyeblocks' ),
    description: __( 'A block for the news signup form.', 'ravenseyeblocks' ),
    category: 'common',
    icon: {
      src: icon,
    },
    keywords: [
      __( 'Basic', 'ravenseyeblocks' ),
      __( 'Shortcode', 'ravenseyeblocks' ),
      __( 'Post', 'ravenseyeblocks' ),
    ],
    attributes: {
      text: {
        type: 'string',
        source: 'text',
      },
    },
    edit: props => {
      const { attributes: { text }, setAttributes } = props;
      const onChangeText = text => { setAttributes( { text } ); };
      return (
        <section className={'signup'}>
          <PlainText
            placeholder={__( 'Form plugin shortcode...', 'ravenseyeblocks' )}
            value={text}
            onChange={onChangeText}
          />
        </section>
      );
    },
    save: props => {
      const { attributes: { text } } = props;
      return (
        <section className={'signup'}>
          <div className={'content'}>
            {text}
          </div>
        </section> );
    },
  },
);
