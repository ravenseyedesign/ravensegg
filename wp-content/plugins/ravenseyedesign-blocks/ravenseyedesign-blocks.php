<?php
/**
 * Plugin Name:       Raven's Eye Design Custom Blocks
 * Plugin URI:        https://ravenseyedesign.com
 * Description:       Custom blocks developed by Raven's Eye Design.
 * Requires at least: 5.9
 * Requires PHP:      7.4
 * Version:           1.5
 * Author:            Chad Bush
 * License:           GPL-2.0-or-later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       ravenseyedesign-blocks
 *
 * @package           create-block
 */

/**
 * Registers the block using the metadata loaded from the `block.json` file.
 * Behind the scenes, it registers also all assets so they can be enqueued
 * through the block editor in the corresponding context.
 *
 * @see https://developer.wordpress.org/block-editor/how-to-guides/block-tutorial/writing-your-first-block-type/
 */
function create_block_ravenseyedesign_blocks_block_init() {
	register_block_type( plugin_dir_path( __FILE__ ) . 'blocks/basic-info/' );
	register_block_type( plugin_dir_path( __FILE__ ) . 'blocks/featured-project/' );
	register_block_type( plugin_dir_path( __FILE__ ) . 'blocks/work-intro/' );
	register_block_type( plugin_dir_path( __FILE__ ) . 'blocks/work-testimonial/' );

    // Additional blocks would be registered here
    // register_block_type( plugin_dir_path( __FILE__ ) . 'blocks/work-intro/' );
}
add_action( 'init', 'create_block_ravenseyedesign_blocks_block_init' );
